﻿module.exports = {
    sections: {

        //HRA XEOS locators
        HRAPage: {
            selector: 'body',
            elements: {
                inputUsername: { selector: '#login_id'},
                inputPassword: { selector: '#password'},
                buttonLogin: {locateStrategy: 'xpath', selector: '//input[@value="Log In"]'},


                linkDashboard: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Dashboard")])[2]'},
                linkClaims: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Claims")])[2]'},
                linkHRAReports: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"HRA Reports")])[2]'},

                imageCreateClaim: {locateStrategy: 'xpath', selector: '//h5[contains(text(),"Create a Claim")]'},
                imagePersonalInfo: {locateStrategy: 'xpath', selector: '//h5[contains(text(),"Personal Information")]'},

                pageTitle: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Welcome")]'},
                titleCreateClaim: {locateStrategy: 'xpath', selector: '//*[contains(text(),"Claim Submission")]'},
                pageClaims:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div[1]/h1'},
                titlePersonalInfo: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Personal Info")]'},
                titleClaims: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"Claims")]'},
                titleHRAReports: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"HRA Reports")]'},

                //buttonCreateOneTimeClaim: {locateStrategy: 'xpath', selector: '//input[@value="Create One-Time Claim")][@class="button radius evo-button"'},
                buttonCreateOneTimeClaim: {locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/div/div[2]/form/input[2]'},
                buttonCreateAutomaticClaim: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Create Automatic Claim")]'},
                buttonSaveChanges: {locateStrategy: 'xpath', selector: '//input[@value="Save Changes"]'},
                buttonGenerateDenialLetters: {locateStrategy: 'xpath', selector: '//input[@value="Generate Denial Letters"]'},
                buttonPrint: { selector: '#print'},
                buttonDownload: { selector: '#download'},

                claimNumber_OneTime: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"-")])[2]'},
                ClaimNumber_Automatic:{locateStrategy: 'xpath', selector:'(//*[contains(text(),"-")])'},
                searchFilter:{locateStrategy: 'xpath', selector: '//section[@id="panel1"]//div[contains(@id,"DataTables_Table")]//input[@placeholder="Filter"]'},
                ClaimStatusReceived:{locateStrategy: 'xpath', selector: '(//*[contains(text(),"Claim Received")])'},
                searchedClaimId:{locateStrategy: 'xpath', selector: '//*[@id="DataTables_Table_0"]/tbody/tr'},
                searchedClaimId_number:{locateStrategy: 'xpath', selector: '//*[@id="DataTables_Table_0"]/tbody/tr/td[1]/a'},

                iconEditInfo :{selector: '#editaddress'},
                inputStreet: {locateStrategy: 'xpath', selector: '//input[@name="address"]'},
                inputCity: {locateStrategy: 'xpath', selector: '//input[@name="city"]'},
                inputZip: {locateStrategy: 'xpath', selector: '//input[@name="zip"]'},
                inputPhone: {locateStrategy: 'xpath', selector: '//input[@name="phone"]'},
                inputCellPhone: {locateStrategy: 'xpath', selector: '//input[@name="cellphone"]'},

                messageSuccess: {locateStrategy: 'xpath', selector: '(//button[@class="close-button"])[1]/..'},

                Page_AutomaticClaimSubmission:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/div/div[1]/h1'},
                Page_OneTimeClaimSubmission: {locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/div/div[1]/h1'},
                ClaimantDropdownAutomatic:{locateStrategy: 'xpath', selector:'//select[@name="pat_relation_emp"]'},
                ClaimantDropdownAutomatic_Value:{locateStrategy: 'xpath', selector:'//select//*[contains(text(),"uKsFJ BADHw / DOB 01/02/1944")]'},
                ClaimantDropOneTime:{locateStrategy: 'xpath', selector:'//select[@name="pat_relation_emp"]'},
                ClaimantDropOneTime_Value:{locateStrategy: 'xpath', selector:'//select//*[contains(text(),"uKsFJ BADHw / DOB 01/02/1944")]'},
                buttonSubmitClaimOneTime:{locateStrategy: 'xpath', selector: '//*[@value="Submit Claim"]'},
                SubmitClaimAutomaticbutton:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/form/div[11]/fieldset/input'},
                messageSubmitClaim:{locateStrategy: 'xpath', selector: '//a[contains(text(),Your claim has been successfully submitted. …")]'},
                servicedateTextBox:{locateStrategy: 'xpath', selector: '//input[@name="pat_date_ser"]'},
                startDateTextBox:{locateStrategy: 'xpath', selector: '//input[@name="auto_date_start"]'},
                endDateTextBox:{locateStrategy: 'xpath', selector: '//input[@name="auto_date_end"]'},
                monthlyPremiumTextBox:{locateStrategy: 'xpath', selector: '//input[@name="pat_claim_amt"]'},

                todayDate:{locateStrategy: 'xpath', selector:'//*[@id="ui-datepicker-div"]/table/tbody/tr[4]/td[6]'},
                claimAmount:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/form/div[4]/fieldset/label/input'},
                descriptionExpense: {locateStrategy: 'xpath', selector: '//input[@name="pat_reason_4v"]'},
                providerName: {locateStrategy: 'xpath', selector: '//input[@name="pat_prov_name"]'},
                uploadChooseFilebutton:{selector:'#attached_doc'},
                addToListbutton_OneTime:{locateStrategy: 'xpath', selector: '//*[@name="upload_document"]'},
                addToListbutton_Automatic:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/form/div[9]/fieldset/div[3]/input'},
                iHerebyCheckbox_OneTime: {locateStrategy: 'xpath', selector: '//*[@name="auth_submit"][@type="checkbox"]'},
                iHerebyCheckbox_Automatic:{locateStrategy: 'xpath', selector:'//input[@name="auth_submit"]'},
                agreedCheckbox: {locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/form/div[8]/fieldset/p/input'},
                agreedCheckbox_Automatic:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/form/div[10]/fieldset/p/input'},
                successfullSubmitMessage:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div[1]'},
                uploadedDoc:{locateStrategy: 'xpath', selector: '//*[@id="mainContent"]/div/div/form/div[9]/fieldset/p[1]'},

                //claim verification values
                verificationPopUp: {locateStrategy: 'xpath', selector: '//div[@class="reveal evo-reveal"][contains(@style,"display")]/form'},
                documentButton:{locateStrategy: 'xpath', selector:'//div[@class="reveal evo-reveal"][contains(@style,"display")]/..//input[@class="button small evo-button"][contains(@value,".docx")]'},

                //CA User Locators
                ClaimPage:{locateStrategy: 'xpath', selector: '//*[@id="evo-site-top-bar"]/div[2]/div/ul/li[2]/a'},
                claimNumberSearchTextBox:{locateStrategy: 'xpath', selector: '//*[@name="claim_num"]'},
                searchButton:{locateStrategy: 'xpath', selector: '//*[@name="param_search"]'},
                searchedClaimRow:{locateStrategy: 'xpath', selector: '//*[@id="main_div_x"]/table'},
                detailsExpandButton:{locateStrategy: 'xpath', selector: '//*[@class="openclaimreviewtable"]'},
                expandedContainer:{locateStrategy: 'xpath', selector: '//*[@class="hradiv_container"]'},
                uploadedDocButton:{locateStrategy: 'xpath', selector:'//input[@class="button tiny round evo-button"][contains(@value,".docx")][@name="doc_button_clicked"]'},
                approvedCheckbox:{locateStrategy: 'xpath', selector:'//*[@value="Approved"]'},
                deniedCheckbox:{locateStrategy: 'xpath', selector:'//*[@value="Denied"]'},
                updateClaimStatusButton:{locateStrategy: 'xpath', selector:'//*[@value="Update Claims Status"]'},
                claimStatusMessage:{locateStrategy: 'xpath', selector:'//*[@id="mainContent"]/div/form/div/fieldset/label/text[17]'},
                deniedClaimStatusMessage:{locateStrategy: 'xpath', selector:'//*[@id="status_field"]/text'},
                userMenu:{locateStrategy: 'xpath', selector:'//*[@class="evo-icon-user"]'},
                logoutButton:{locateStrategy: 'xpath', selector:'//*[@id="clicklogout2"]'},
                afterApprovalStatus:{locateStrategy: 'xpath', selector:'//*[@id="DataTables_Table_0"]/tbody/tr/td[6]'},


            }
        },

    }
};




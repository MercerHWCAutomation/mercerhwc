var action = require('../../../Keywords.js');
//var Objects = require(__dirname + '/../../objects/HRA_XEOS_Locators.js');
var data = require('../../test_data/HRA_TestData.js');
var robot = require('robot-js');
var ClaimId,ClaimIdAutomatic,claimres,OneTimeclaimres;

module.exports = function () {
    this.Given(/^Once User is logged in using "([^"]*)" to HRA XEOS application$/, function (browser,userType) {

        var URL;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' +execEnv);
        URL = data.url_QA;
        var userDB = data.usersQA[userType];
        var locators= browser.page.HRA_XEOS_Locators();
        page = locators.section;
        HRAPage = page.HRAPage;

       // action.initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);


            HRAPage.waitForElementVisible('@inputUsername', data.longWait);
            HRAPage.setValue('@inputUsername', userDB.username);

            HRAPage.waitForElementVisible('@inputPassword', data.longWait);
            HRAPage.setValue('@inputPassword', userDB.password);
            HRAPage.click('@buttonLogin', function () {
                browser.timeoutsImplicitWait(5000);
                HRAPage.waitForElementVisible('@pageTitle', data.longWait);
          //  });
        });
    });

    this.Given(/^Edits some user Information and verifies it$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.setText('HRAPage|inputStreet', "abc", function () {
                browser.timeoutsImplicitWait(data.shortWait);
                action.setText('HRAPage|inputCity', "xyz", function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                    action.setText('HRAPage|inputZip', "65432", function () {
                        browser.timeoutsImplicitWait(data.shortWait);
                        action.setText('HRAPage|inputPhone', "0123456789", function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            action.setText('HRAPage|inputCellPhone', "9876543210", function () {
                                browser.timeoutsImplicitWait(data.shortWait);
                                action.performClick('HRAPage|buttonSaveChanges', function () {
                                    browser.timeoutsImplicitWait(data.shortWait);
                                    browser.pause(data.longWait);
                                    action.isDisplayed('HRAPage|messageSuccess', function () {
                                        browser.timeoutsImplicitWait(data.shortWait);
                                    });
                                });
                            });
                        });
                    });
                });
            });

        });
    });

    this.Given(/^User saves and verifies the file$/, function (browser) {
        action.initializePageObjects(browser, function () {
            var Keyboard = robot.Keyboard;
            var k = Keyboard();
            k.click(robot.KEY_S);
            browser.pause(data.shortWait);
            k.click(robot.KEY_ENTER);
        });
    });

    this.Given(/^Fill all the mandatory fields and select "Agreed and Acknowledged" checkboxes/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick("HRAPage|ClaimantDropOneTime", function () {
                browser.pause(data.shortWait);
                action.performClick("HRAPage|ClaimantDropOneTime_Value", function () {
                    action.setText('HRAPage|claimAmount', "100")

                            action.performClick("HRAPage|servicedateTextBox", function () {
                                action.performClick("HRAPage|todayDate", function () {
                                    action.setText('HRAPage|descriptionExpense', "expense description")
                                        browser.timeoutsImplicitWait(data.shortWait);
                                        action.setText('HRAPage|providerName', "provider name")
                                           // browser.timeoutsImplicitWait(data.shortWait);
                                            console.log('setvalue check');

                                    browser.setValue("input#attached_doc", require('path').resolve(__dirname + '/HRA_XEOS_Smoke_TestDoc.docx'));
                                            action.performClick("HRAPage|addToListbutton_OneTime", function () {
                                                action.performClick("HRAPage|iHerebyCheckbox_OneTime", function () {
                                                    action.performClick("HRAPage|agreedCheckbox", function () {

                                                        browser.timeoutsImplicitWait(data.shortWait);
                                                    });



                                });
                            });

                        });
                    });

                });

            });

        });

    });

    this.Given(/^Fill mandatory fields and select "Agreed and Acknowledged" checkboxes for Automatic Claim Page/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick("HRAPage|ClaimantDropdownAutomatic", function () {
                browser.pause(data.shortWait);
                action.performClick("HRAPage|ClaimantDropdownAutomatic_Value", function () {
                });
            });

            action.setText('HRAPage|monthlyPremiumTextBox', "100", function () {
            });
            action.performClick("HRAPage|startDateTextBox", function () {
            });
            action.performClick("HRAPage|todayDate", function () {
            });
            action.performClick("HRAPage|endDateTextBox", function () {
            });
            action.performClick("HRAPage|todayDate", function () {
            });

            browser.setValue("input#attached_doc", require('path').resolve(__dirname + '/HRA_XEOS_Smoke_TestDoc.docx'));

            action.performClick("HRAPage|addToListbutton_Automatic", function () {

                action.performClick("HRAPage|agreedCheckbox_Automatic", function () {
                });
                action.performClick("HRAPage|iHerebyCheckbox_Automatic", function () {
                })

            });
        });
    });

    this.When(/^User Clicks on "([^"]*)" Link or Button$/, function(browser,locator) {
        action.initializePageObjects(browser, function () {
            action.performClick(locator, function () {
               // browser.pause(data.shortpause);
            });
        });
    });

    this.Then(/^"([^"]*)" page or message should be displayed$/, function(browser,locator){
        action.initializePageObjects(browser, function () {
            action.isDisplayed(locator, function () {
               // browser.pause(data.shortpause)
            });
        });
    });
    this.When(/^Enter "([^"]*)" field with "([^"]*)" value$/, function (browser,locator, value) {
         action.initializePageObjects(browser, function () {
             action.setText(locator, value)
         });
    });
    this.Given(/^Once User is logged in using "([^"]*)" to HRA XEOS application to the internal website$/, function (browser,userType) {

        var URL;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' +execEnv);
        URL = data.url_QA_Internal;
        var userDB = data.usersQA[userType];
        var Hpage= browser.page.HRA_XEOS_Locators();

        HRAPage = Hpage.section.HRAPage;
       // action.initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);


            HRAPage.waitForElementVisible('@inputUsername', data.longWait);
            HRAPage.setValue('@inputUsername', userDB.username);

            HRAPage.waitForElementVisible('@inputPassword', data.longWait);
            HRAPage.setValue('@inputPassword', userDB.password);
            HRAPage.click('@buttonLogin', function () {
                browser.timeoutsImplicitWait(5000);
                HRAPage.waitForElementVisible('@titleClaims', data.longWait);
            });
      //  });
    });

      this.Given(/^User searches "Claim ID " in filter textbox/, function (browser) {

     //action.setText('HRAPage|searchFilter', ClaimId, function () {
     //    browser.timeoutsImplicitWait(data.shortWait);
     //});
     HRAPage.clearValue('@searchFilter',function () {
     var field= OneTimeclaimres.split('-');
     var claimidafterhyphenOneTime = field[1];
     // OneTimeclaimres= claimidafterhyphenOneTime;
     HRAPage.sendKeys("@searchFilter",claimidafterhyphenOneTime);
     })
     });

    /*  this.Given(/^User searches "Claim ID " filter textbox for Automatic Claim page/, function (browser) {
     /*action.setText('HRAPage|searchFilter', claimres,function(){

     });consol
     HRAPage.clearValue('@searchFilter',function () {
     var fields= claimres.split('-');
     var claimidafterhyphen = fields[1];
     HRAPage.sendKeys("@searchFilter",claimidafterhyphen);
     });
     //browser.timeoutsImplicitWait(data.shortWait);
     }); */

    this.Given(/^User enters the claim number in the Claim Number field/, function (browser) {
        action.initializePageObjects(browser, function () {
            HRAPage.clearValue('@claimNumberSearchTextBox', function () {
                HRAPage.sendKeys("@claimNumberSearchTextBox", OneTimeclaimres);
            });
        });
    });

    this.Given(/^User enters the claim number in the Claim field and click on Search for Denial by CA/, function (browser) {
        action.initializePageObjects(browser, function () {
            HRAPage.clearValue('@claimNumberSearchTextBox', function () {
                HRAPage.sendKeys("@claimNumberSearchTextBox", claimres);
            });
        });
    });

    this.Given(/^User stores the Claim number/, function (browser) {
        // var ClaimId;
        action.initializePageObjects(browser, function () {
        HRAPage.getText("@claimNumber_OneTime", function (ClaimId) {

            console.log(ClaimId.value);
            OneTimeclaimres = ClaimId.value;

        });
        });
    });
    this.Given(/^Automatic Claim number should be stored/, function (browser) {
        action.initializePageObjects(browser, function () {
            HRAPage.getText("@ClaimNumber_Automatic", function (ClaimIdAutomatic) {
                console.log(ClaimIdAutomatic);
                // var1 = ClaimIdAutomatic.value;
                claimres = ClaimIdAutomatic.value;

            });
        });
    });
    this.Given(/^Corresponding One Time Claim id should be displayed/, function (browser) {

    });
    this.Given(/^Verify the status of the claim as Recieved/, function (browser) {
        //ClaimStatusReceived


    });

    // this.Given(/^The claim details should be the same as entered$/, function (browser) {
    //     verificationPopUp
    //     action.verifyText(,)
//
    // });
};

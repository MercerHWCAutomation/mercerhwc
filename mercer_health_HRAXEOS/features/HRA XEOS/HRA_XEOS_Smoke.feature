@HRAXEOSSmokeSuite
  #==================Participant User ===============
Feature: Sanity Validations in HRA XEOS Application

  @SC01_LoginasParticipantUser @ts
  Scenario: Launch the application and navigate to Client page
    Given   Once User is logged in using "PartcipantUser" to HRA XEOS application
  @SC02_OneTimeCreateClaim @ts
  Scenario: Verification of One Time Claim Functionality
    Given   User Clicks on "HRAPage|imageCreateClaim" Link or Button
    And     "HRAPage|titleCreateClaim" page or message should be displayed
    When User Clicks on "HRAPage|buttonCreateOneTimeClaim" Link or Button
    Then "HRAPage|Page_OneTimeClaimSubmission" page or message should be displayed
    And "HRAPage|claimNumber_OneTime" page or message should be displayed
    And User stores the Claim number
    When  Fill all the mandatory fields and select "Agreed and Acknowledged" checkboxes
    #And User Clicks on "HRAPage|buttonSubmitClaim" Link or Button
    # Then "HRAPage|messageSubmitClaim" page or message should be displayed
    And User Clicks on "HRAPage|buttonSubmitClaimOneTime" Link or Button
    Then "HRAPage|successfullSubmitMessage" page or message should be displayed
   # When User searches "Claim ID " in filter textbox
    Then "HRAPage|searchedClaimId" page or message should be displayed
    And "HRAPage|ClaimStatusReceived" page or message should be displayed
    When User Clicks on "HRAPage|searchedClaimId_number" Link or Button
    Then "HRAPage|verificationPopUp" page or message should be displayed
    When User Clicks on "HRAPage|documentButton" Link or Button
       #And The document should open and should be saved
  @SC03_OneTimeCreateClaim @ts
  @SC03_AutomaticClaim
  Scenario: Verification of Automatic Claim Functionality
    Given   Once User is logged in using "PartcipantUser" to HRA XEOS application
    #Given   User clicks on "HRAPage|linkDashboard" Link or Button
    Given   User Clicks on "HRAPage|linkClaims" Link or Button
    And     "HRAPage|pageClaims" page or message should be displayed
    When User Clicks on "HRAPage|buttonCreateAutomaticClaim" Link or Button
    Then "HRAPage|Page_AutomaticClaimSubmission" page or message should be displayed
    And "HRAPage|ClaimNumber_Automatic" page or message should be displayed
    And Automatic Claim number should be stored
    When Fill mandatory fields and select "Agreed and Acknowledged" checkboxes for Automatic Claim Page
    And User Clicks on "HRAPage|SubmitClaimAutomaticbutton" Link or Button
    Then "HRAPage|successfullSubmitMessage" page or message should be displayed
   # When User searches "Claim ID " filter textbox for Automatic Claim page
    Then "HRAPage|searchedClaimId" page or message should be displayed
    And Verify the status of the claim as Recieved
       #And The claim status should be submitted
    When User Clicks on "HRAPage|searchedClaimId_number" Link or Button
    Then "HRAPage|verificationPopUp" page or message should be displayed
    When User Clicks on "HRAPage|documentButton" Link or Button
    #And The document should open and should be saved
  @SC04_OneTimeCreateClaim @ts
  @SC04_EditPersonalInfo
  Scenario: Verification  of Edit Personal Information Page
    Given   Once User is logged in using "PartcipantUser" to HRA XEOS application
    When    User Clicks on "HRAPage|imagePersonalInfo" Link or Button
    And     "HRAPage|titlePersonalInfo" page or message should be displayed
    When    User Clicks on "HRAPage|iconEditInfo" Link or Button
    And     Enter "HRAPage|inputStreet" field with "abcdefghij" value
    And     Enter "HRAPage|inputCity" field with "zyxwvutsrqp" value
    And     Enter "HRAPage|inputZip" field with "65432" value
    And     Enter "HRAPage|inputPhone" field with "0123456789" value
    And     Enter "HRAPage|inputCellPhone" field with "9876543210" value
    And     User Clicks on "HRAPage|buttonSaveChanges" Link or Button
    Then    "HRAPage|messageSuccess" page or message should be displayed


 # @HRA_XEOS_SmokeSuite_InternalUser
#========================Internal User============
##  @SC05_OneTimeCreateClaim @ts
##  @SC01_LoginasInternalUser
##  Scenario: Launch the application and navigate to Client page
##    Given   Once User is logged in using "InternalUser" to HRA XEOS application to the internal website
##  @SC06_OneTimeCreateClaim @ts
##  @SC03_ClaimApprovalasCA
##  Scenario: Verification  of Approve claim as CA
##    Then "HRAPage|ClaimPage" page or message should be displayed
##    When User enters the claim number in the Claim Number field
##    When User Clicks on "HRAPage|searchButton" Link or Button
##    Then "HRAPage|searchedClaimRow" page or message should be displayed
##    When User Clicks on "HRAPage|detailsExpandButton" Link or Button
##    And "HRAPage|uploadedDocButton" page or message should be displayed
##    When User Clicks on "HRAPage|uploadedDocButton" Link or Button
## # Then The documents should be viewable and downloadable
##    And User Clicks on "HRAPage|approvedCheckbox" Link or Button
##    And User Clicks on "HRAPage|updateClaimStatusButton" Link or Button
##    Then "HRAPage|claimStatusMessage" page or message should be displayed
##  @SC07_OneTimeCreateClaim @ts
##  @SC04_ClaimDenielasCA
##  Scenario: Denial of Claim by CA
##
##    Given Once User is logged in using "InternalUser" to HRA XEOS application to the internal website
##    When User enters the claim number in the Claim field and click on Search for Denial by CA
##    And User Clicks on "HRAPage|searchButton" Link or Button
##    Then "HRAPage|searchedClaimRow" page or message should be displayed
##    #And Verify Approved for Review radio buttons are selected
##    When User Clicks on "HRAPage|detailsExpandButton" Link or Button
##    And "HRAPage|uploadedDocButton" page or message should be displayed
##    When User Clicks on "HRAPage|uploadedDocButton" Link or Button
##    #Then The documents should be viewable and downloadable
##    And User Clicks on "HRAPage|deniedCheckbox" Link or Button
##    And User Clicks on "HRAPage|updateClaimStatusButton" Link or Button
##    Then "HRAPage|deniedClaimStatusMessage" page or message should be displayed
##    Given   Once User is logged in using "PartcipantUser" to HRA XEOS application
##    Given User Clicks on "HRAPage|linkClaims" Link or Button
##
##    # entering claim id of one time which is approved
##    When User searches "Claim ID " in filter textbox
##    Then "HRAPage|afterApprovalStatus" page or message should be displayed
##
##  @SC08_OneTimeCreateClaim @ts
##  @SC05_ReportsIntUser
##  Scenario: Verification of HRA Reports page
##    Given   Once User is logged in using "InternalUser" to HRA XEOS application to the internal website
##    Given   User Clicks on "HRAPage|linkHRAReports" Link or Button
##    And     "HRAPage|titleHRAReports" page or message should be displayed
##    When    User Clicks on "HRAPage|buttonGenerateDenialLetters" Link or Button
## #  Then    "HRAPage|buttonPrint" page or message should be displayed
## #  And     User Clicks on "HRAPage|buttonDownload" Link or Button
##    And     User saves and verifies the file

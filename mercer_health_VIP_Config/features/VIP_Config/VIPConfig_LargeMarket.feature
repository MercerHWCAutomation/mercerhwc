Feature: Sanity Validations in VIP Config Application for Large Markets

  @VIPConfig @LargeMarket @Smoke @Login
  Scenario: Navigate to client page
    Given   User logs in to VIPConfig application

  @VIPConfig @LargeMarket @Smoke @CarrierTab
  Scenario: Verfication of Carrier ID List Tab
    Given   User Clicks on "VIPPage|linkQAACME" Link or Button
    And     "VIPPage|titleQAACME" page or message should be displayed
    And     User verifies the Carrier View Tab

  @VIPConfig @LargeMarket @Smoke @PremiumTab
  Scenario: Verfication of Premium Report Tab
    Given   User Clicks on "VIPPage|tabPremReport" Link or Button
    And     "VIPPage|titleTabPremReport" page or message should be displayed
    And     "VIPPage|tabCustomFieldMapping" page or message should be displayed
    And     "VIPPage|tabReportSettings" page or message should be displayed
    And     "VIPPage|tabCarrierReportConfigurations" page or message should be displayed
    And     User verifes the Custom Field Mapping tab

  @VIPConfig @LargeMarket @Smoke @PremiumTab
  Scenario: Verfication of Reports Tab
    Given   User Clicks on "VIPPage|tabReportSettings" Link or Button
    And     "VIPPage|labelReportSettings" page or message should be displayed
    And     "VIPPage|fieldMarkSSN" page or message should be displayed
    And     "VIPPage|fieldPremReportLive" page or message should be displayed
    And     "VIPPage|fieldClientSummaryKeyField" page or message should be displayed

  @VIPConfig @LargeMarket @Smoke @PremiumTab
  Scenario: Verfication of Carrier Report Configurations Tab
    Given   User Clicks on "VIPPage|tabCarrierReportConfigurations" Link or Button
    And     "VIPPage|labelCarrierReportConfigurations" page or message should be displayed
    And     "VIPPage|columnnCarrierDisplayName" page or message should be displayed
    And     "VIPPage|columnnCarrierBenefitIds" page or message should be displayed
    And     "VIPPage|columnnActionsCarrierReportsTab" page or message should be displayed



var action = require('./Keywords.js');
var Objects = require(__dirname + '/../../objects/VIPConfig_Locators.js');
var data = require('../../test_data/VIPConfig_TestData.js');

module.exports = function () {

    this.Given(/^User verifies the Carrier View Tab$/, function (browser) {

        action.isDisplayed('VIPPage|titleTabCarrierList',function () {
            action.isDisplayed('VIPPage|viewActiveInactive',function () {
                action.isDisplayed('VIPPage|columnnCarrierID',function () {
                    action.isDisplayed('VIPPage|columnnCarrierDesc',function () {
                        action.isDisplayed('VIPPage|columnnEffectiveDate',function () {
                            action.isDisplayed('VIPPage|columnnLastUpdated', function () {
                                action.isDisplayed('VIPPage|columnnSuppressCarriers',function () {
                                    action.isDisplayed('VIPPage|columnnSuppressPremiums',function () {
                                        browser.timeoutsImplicitWait(data.shortWait);
                                        action.performClick('VIPPage|viewInactive',function () {
                                            browser.pause(data.shortWait);

                                            action.isDisplayed('VIPPage|inputBoxFilter',function () {
                                                browser.timeoutsImplicitWait(data.shortWait);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    this.Given(/^User verifes the Custom Field Mapping tab$/, function (browser) {
        action.isDisplayed('VIPPage|columnnNumber', function() {
            action.isDisplayed('VIPPage|columnnFieldDisplayName', function() {
                action.isDisplayed('VIPPage|columnnTable', function() {
                    action.isDisplayed('VIPPage|columnnFieldNames', function() {
                        action.isDisplayed('VIPPage|columnnActions', function() {
                            browser.timeoutsImplicitWait(data.shortWait);
                        });
                    });
                });
            });
        });
    });

};
Feature: Admin Toolkit Smoke Test cases

  @Smoke @AdminToolkitContentEPAddRule @1
  Scenario: Accessing the Admin Toolkit Content Ep Add Rule
    Given   User opens the browser and launch the AdminTool kit URl
    When    User clicks on Client Link and Web configuration Page is displayed
    And     User enter Content EP Values and Corresponding EP details should be displayed
    And     User clicks on EP description link and should navigate to Ep Rules page
    And     User Adds a new Rule for the Ep and Congratulation Msg Appears
    Then    User able to delete the Created Rule
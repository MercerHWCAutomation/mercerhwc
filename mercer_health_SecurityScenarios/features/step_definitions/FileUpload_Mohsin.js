var data = require('../../TestResources/SampleData.js');
var Objects = require(__dirname + '/../../repository/SamplePages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs= require("fs");
var Login, Dashboard, Docupload;

function initializePageObjects(browser, callback) {
    var SamplePageData = browser.page.SamplePages();
    Login = SamplePageData.section.LoginPage;
    Dashboard= SamplePageData.section.DashboardPage;
    Docupload= SamplePageData.section.DocuploadPage;
    callback();
}
module.exports = function() {

    this.Given(/^User goes to dependent verification file upload$/, function () {
        var URL;
        var execEnv = data["TestingEnvironment"];
        execEnv=== "QAI";
        console.log('Test Environment: ' + execEnv);
        browser = this;
        URL = data.urlMBCQA3;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);
            Login.setValue('@inputUsername', data.UserMBCQA3.MBCUser.username);
            Login.setValue('@inputPassword', data.UserMBCQA3.MBCUser.password);
            Login.click('@SubmitButton');
            browser.pause(5000);
            Dashboard.click('@docUploadButton');
            browser.pause(5000);
            Dashboard.click('@seeDetailAndUploadButton');

        });
    });

    this.When(/^User uploads an unexpected file$/, function () {
        browser = this;

        initializePageObjects(browser, function () {
            browser.pause(5000);
            this.demoTest = function (browser) {
                browser.moveToElement('@selectFileButton', 10, 10);
            };
            browser.pause(5000);
            Docupload.click('@selectFileButton')
            browser.setValue('input[type="file"]',require('path').resolve('C:\\Users\\Rachana-GN\\Desktop\\TestDocument.docx'))
            Docupload.click('@Uploadbutton')
            browser.pause(5000);
        });
    });

    this.Then(/^Verify error message for unexpected file$/, function () {
        browser = this;
        browser.useXpath().assert.containsText("(//div[@type='text']/..)[1]/p",
            "Upload unsuccessful. This file type cannot be processed. Allowed file types are PNG, PDF, GIF, TIF or TIFF, BMP, JPG, or JPEG");
        this.pause(5000);
    });

    this.Then(/^User logs out from MBC Application$/, function () {
        browser = this;
        Docupload.click('@LogoutButton');
        this.pause(5000);
    });
}










//Then User shouldnt see the content of the file in the browse
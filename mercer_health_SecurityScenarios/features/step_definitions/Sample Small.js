var data = require('../../TestResources/MBCData Small.js');
var Objects = require(__dirname + '/../../repository/MBCPage Small.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');

var DashboardPage;

function initializePageObjects(browser, callback) {
    var MBCPageS1 = browser.page.MBCPageS();
    DashboardPage = MBCPageS1.section.DashboardPage;
    callback();
}
module.exports = function() {

    this.Given(/^User launches the MBC Application in Browser for Small Market$/, function () {
        var URL;
        var execEnv = data["TestingEnvironment"];
        execEnv=== "SmallMarket";
        console.log('Test Environment: ' + execEnv);
        browser = this;
        URL = data.URLSmall;
        initializePageObjects(browser, function () {
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(3000);
            DashboardPage.waitForElementVisible('@Username', data.wait);
        });
    });

    this.When(/^User enters the username in Username textbox for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Username', data.wait)
            .setValue('@Username', data.Username);
        browser.pause(data.wait);

    });


    this.When(/^User enters the SQL Query as password in the Password textbox for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Password', data.wait)
            .setValue('@Password', data.SQLPassword);
        browser.pause(data.wait);
    });

    this.When(/^User enters the password in the Password textbox for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Password', data.wait)
            .setValue('@Password', data.Password);
        browser.pause(data.wait);
    });

    this.When(/^User clicks on submit for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@SubmitButton', data.Loginwait)
            .click('@SubmitButton')
    });

    this.Then(/^Verify error message for wrong Username and Password for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@SubmitButton', data.wait)
        browser.useXpath().assert.containsText("//div[@id='dialog']/i", "");
        this.pause(5000)
    });

    this.When(/^User clicks on Menu for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@DashboardTab', data.longwait)
        browser.pause(data.Loginwait)
        DashboardPage.waitForElementVisible('@MenuButton', data.Loginwait)
        DashboardPage.click('@MenuButton')
    });

    this.When(/^User clicks on Complete Life Event for Small Market$/, function () {
        browser = this;
        browser.pause(data.longwait)
        DashboardPage.waitForElementVisible('@CompleteLifeEventButton', data.longwait)
            .click('@CompleteLifeEventButton')
        this.pause(5000)
    });

    this.When(/^User clicks on Get Started Tab for Small Market$/, function () {
        browser = this;
        browser.pause(10000);
        browser.pause(data.longwait)
        DashboardPage.waitForElementVisible('@GetStartedTab', data.longwait)
            .click('@GetStartedTab')
        browser.pause(data.longwait)
        this.pause(5000);
    });

    this.When(/^User navigates to Who's covered page for Small Market$/, function () {
        browser = this;
        browser.pause(10000);
        DashboardPage.waitForElementVisible('@WhosCoveredTab', data.wait)
            .click('@WhosCoveredTab')
        this.pause(5000);
    });

    this.Then(/^User clicks on Add Dependent Button for Small Market$/, function () {
        browser = this;
        browser.pause(10000);
        DashboardPage.waitForElementVisible('@AddDependentButton', data.wait)
            .click('@AddDependentButton')
        this.pause(5000);
    });

    this.Then(/^User clicks on Continue Button for Small Market$/, function () {
        browser = this;
        browser.pause(10000);
        DashboardPage.waitForElementVisible('@ContinueButton', data.wait)
            .click('@ContinueButton')
        this.pause(5000);
    });

    this.Then(/^User enters the SQL Query in SSN textfield for Small Market$/, function () {
        browser = this;
        browser.pause(10000);
        DashboardPage.waitForElementVisible('@SSN', data.wait)
            .setValue('@SSN', data.SSN).click('@Random')
        this.pause(5000);
    });

    this.Then(/^Verify error message when entered wrong SSN for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Random', data.wait)
        browser.useXpath().assert.containsText("//div[.='Please enter a valid SSN.']", "Please enter a valid SS");
        this.pause(5000);
    });

    this.Then(/^User logs out of MBC Application for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@LogoutButton', data.wait)
            .click('@LogoutButton')
        this.pause(5000);
    });

    this.Then(/^User navigates to My Information page for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@ContinueButtoninWCP', data.wait)
            .click('@ContinueButtoninWCP');
        this.pause(5000);
    });

    this.Then(/^User clicks on edit button MyInfoPage for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@EditButtoninWCP', data.wait)
            .click('@EditButtoninWCP');
        this.pause(5000);
    });

    this.Then(/^User enters SQL query in Mobile phone text field for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Mobiletextfield', data.wait)
            .setValue('@Mobiletextfield', data.SSN).click('@Random1')
        this.pause(5000);
    });

    this.Then(/^Verify error message for entering wrong Mobile number for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Random1', data.wait)
        browser.useXpath().assert.containsText("//div[.='Please enter a valid phone number.']", "");
        this.pause(5000);
    });

    this.When(/^User clicks on Profile link for Small Market$/, function () {
        browser = this;
        browser.pause(5000);
        DashboardPage.waitForElementVisible('@Profilepage', data.wait)
            .click('@Profilepage')
        this.pause(5000);
    });
    this.When(/^User clicks on edit button in Profile page for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@EditbuttoninProfilepage', data.wait)
            .click('@EditbuttoninProfilepage')
        this.pause(5000);
    });

    this.Then(/^User enters the data in the editable field for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Addressfield', data.wait)
            .setValue('@Addressfield', data.Alertdata).click('@Random3')
        this.pause(5000);
    });

    this.Then(/^Verifies the alert popup for Small Market$/, function () {
        browser = this;
        DashboardPage.waitForElementVisible('@Random3', data.wait)
        browser.useXpath().assert.containsText("(//div[@class='error-msg ng-scope']//div)[2]", "");

        this.pause(5000);
    });

    this.Given(/^User creates an html file for Small Market$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv === "SmallMarket") {
            //fs.createFileFromStorageFile()
            fs.writeFile("C:\\Users\\rachana-gn\\Documents/Script.html",
                "<html>" +
                "<head>" +
                "<title>test</title>" +
                "</head>" +
                "<body>" +
                "<p>This website is vulnerable to Clickjacking</p>" +
                "<iframe height=500 width=1000 scrolling=yes frameborder=1 marginwidth=10 src='https://auth-qai.mercerbenefitscentral.com/mbcqa3/login.tpz'>" +
                "</body>" +
                "</html>"
                , function (err) {
                    if (err) {
                        returnconsole.log(err);
                    }

                })
        }
    });

    this.When(/^User tries to open the file for Small Market$/,function(){
        var URL;
        browser=this;
        var execEnv=data["TestingEnvironment"];
        if(execEnv ==="SmallMarket"){
            console.log("The file was saved!");
            var fileURL;
            fileURL='C:\\Users\\rachana-gn\\Documents/Script.html';
            initializePageObjects(browser,function(){
                browser.timeoutsImplicitWait(50000)
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(fileURL);
                browser.timeoutsImplicitWait(90000);
                browser.timeoutsImplicitWait(90000);
                browser.assert.elementNotPresent('Password');

            });
        }
    });

    this.Given(/^User launches the MBC login page in Browser for Small Market$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv === "SmallMarket") {
            URL = data.URLSmall;
            console.log(URL)
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
               browser.deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(20000);
                //browser.pause(data.longWait);
                //browser.pause(data.longWait);
                DashboardPage.waitForElementVisible('@Username', data.wait);
                browser.pause(20000);
            });
        }
    });

    this.Given(/^User changes https to http for Small Market$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv=== "SmallMarket") {
            URL = data.httpUrl;
            //browser.timeoutsImplicitWait(30000);
            browser.pause(5000)
            initializePageObjects(browser, function () {
                browser.maximizeWindow();
                browser.timeoutsImplicitWait(5000)
                    .url(URL);
                browser.pause(5000)
                console.log(URL);
                browser.timeoutsImplicitWait(5000);
                browser.pause(data.wait)
            });
        }
    });

    this.Given(/^User should be redirected to https login page for Small Market$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv=== "SmallMarket") {
            URL = data.ErrorURL;
            browser.pause(5000);
            browser.useXpath().assert.containsText("//button[@id='reload-button']", "");
            this.pause(5000);
            browser.pause(5000)
        }
    });

    this.Given(/^User enters username and password and clicks on Submit for Small Market$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv=== "SmallMarket") {
            DashboardPage.waitForElementVisible('@Username', data.longwait)
                .setValue('@Username', data.Username)
                .setValue('@Password', data.Password)
                .click('@SubmitButton');
            browser.pause(data.longwait);
            DashboardPage.waitForElementVisible('@DashboardTab', data.longwait)
            browser.pause(data.longwait)

        }
    });

    this.When(/^User logsout of MBC and tries to navigate back for Small Market$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv=== "SmallMarket") {

            //browser.timeoutsImplicitWait(30000)
            DashboardPage.waitForElementVisible('@LogoutButton', data.longwait)
                .click('@LogoutButton');
           browser.pause(data.longwait);
           //browser.timeoutsImplicitWait(30000)
           //DashboardPage.waitForElementVisible('@FNPassword', data.Loginwait)
            browser.pause(data.longwait)
            browser.back();
            //browser.back();
            //browser.back();

           browser.pause(data.longwait);

        }
    });

    this.Then(/^User lands back on Login page for Small Market$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv=== "SmallMarket") {

            browser.timeoutsImplicitWait(30000)
            DashboardPage.waitForElementVisible('@FNPassword', data.Loginwait)
            browser.pause(30000)
            browser.pause(data.longwait)
        }
    });

    this.When(/^User enters invalid username and password and clicks Submit for Small Market$/, function () {
        var URL;
        var a;
        //var t;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv === "SmallMarket") {
            a = 0;

            if (a < 8) {

                DashboardPage.waitForElementVisible('@Password', data.wait);

                if (a === 0) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMIUsername)
                        .setValue('@Password', data.SMIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait);
                    DashboardPage.waitForElementVisible('@CredsError', data.wait);
                    browser.pause(data.wait);
                    a = a + 1;
                }

                if (a === 1) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMIIUsername)
                        .setValue('@Password', data.SMIIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait);
                    DashboardPage.waitForElementVisible('@CredsError', data.wait);
                    browser.pause(data.wait);
                    a = a + 1;
                }

                if (a === 2) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMIIIUsername)
                        .setValue('@Password', data.SMIIIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait);
                    DashboardPage.waitForElementVisible('@CredsError', data.wait);
                    browser.pause(data.wait);
                    browser.useXpath().getText('//div[@id=\'dialog\']', function (response) {
                        var ExpMsg= "Either username or password is incorrect. Please try again.";
                        console.log("Excepted: " + ExpMsg);
                        console.log("Actual: " + response.value);
                        browser.assert.equal(response.value, ExpMsg);
                        if (response.value != ExpMsg) {
                            console.log("Text Content mismatch: ");
                        }
                    });
                    a = a + 1;
                }
                if (a === 3) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMIVUsername)
                        .setValue('@Password', data.SMIVPasswordPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait)
                    DashboardPage.waitForElementVisible('@CredsError', data.wait)
                    browser.pause(data.wait);
                    a = a + 1;
                }
                if (a === 4) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMVUsername)
                        .setValue('@Password', data.SMVPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait)
                    DashboardPage.waitForElementVisible('@CredsError', data.wait)
                    browser.pause(data.wait);
                    a = a + 1;
                }
                if (a === 5) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMVIUsername)
                        .setValue('@Password', data.SMVIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait)
                    DashboardPage.waitForElementVisible('@CredsError', data.wait)
                    browser.pause(data.wait);
                    a = a + 1;
                }
                if (a === 6) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMVIUsername)
                        .setValue('@Password', data.SMVIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait)
                    DashboardPage.waitForElementVisible('@CredsError', data.wait)
                    browser.pause(data.wait);
                    a = a + 1;
                }
                if (a === 7) {
                    DashboardPage.waitForElementVisible('@Password', data.wait).setValue('@Username', data.SMVIIUsername)
                        .setValue('@Password', data.SMVIIPassword)
                        .click('@SubmitButton');
                    browser.pause(data.wait);
                    DashboardPage.waitForElementVisible('@LockError', data.wait);
                    browser.useXpath().getText('//div[@id=\'dialog\']', function (response) {
                        var ExpMsg1= "Your account is temporarily locked for security reasons, please try again after 10 minutes.";
                        console.log("Excepted: " + ExpMsg1);
                        console.log("Actual: " + response.value);
                        browser.assert.equal(response.value, ExpMsg1);
                        if (response.value != ExpMsg1) {
                            console.log("Text Content mismatch: ");
                        }
                    });
                    browser.pause(data.wait);

                }
            }

        }
    });

}
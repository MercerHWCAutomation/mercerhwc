/**
 * Created by Triveni Gherde on 10/11/2017.
 */


var data = require('../../TestResources/Consultant_Security_GLobalData');
var Objects = require(__dirname + '/../../repository/ConsultantSecurityPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');

var LoginPageCP,DashboardPageBP,UploadFile;

function initializePageObjects(browser, callback) {
    var BPPage = browser.page.ConsultantSecurityPages();
    LoginPageCP = BPPage.section.LoginPageCP;
    DashboardPageBP = BPPage.section.DashboardPageBP;
    UploadFile = BPPage.section.UploadFile;
    callback();
}

module.exports = function() {
//1
    this.Given(/^User opens the browser and launch the consultant Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL = data.ConsultantURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPageCP.waitForElementVisible('@inputUsernameCP', data.longWait);
            });
        }
    });
    this.When(/^Enter non-consultant username and password and click Login on consultant Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            LoginPageCP.waitForElementVisible('@inputPasswordCP', data.longWait)
                .setValue('@inputUsernameCP',data.ConsultantlUsername1)
                .setValue('@inputPasswordCP',data.ConsultantPassword1)
                .click('@ContinueButtonLoginCP');
        }
    });
    this.When(/^User must not be able to Login into Consultant Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageCP.waitForElementVisible('@Error_Incorrect_credentials', data.longWait)
                .waitForElementVisible('@inputUsernameCP',data.LoginWait);
        }
    });

    //2
    this.When(/^Try to access the Consultant page without Login$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            URL = data.ConsultantURL1;
            browser.maximizeWindow()
                .deleteCookies()
                .url(URL);
            browser.timeoutsImplicitWait(30000);
        }
    });
    this.Then(/^Page should not open and ppt should get redirected to CP-login page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            browser.waitForElementNotPresent('@DashboardTitleBP', data.longWait);
            LoginPageCP.waitForElementVisible('@inputUsernameCP', data.longWait);
        }
    });

    //@3
    this.When(/^Enter invalid username or password and click Login on Consultant Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            LoginPageCP.waitForElementVisible('@inputPasswordCP', data.longWait)
                .setValue('@inputUsernameCP',data.ConsultantUsername2)
                .setValue('@inputPasswordCP',data.InvalidPassword1)
                .click('@ContinueButtonLoginCP')
                .waitForElementVisible('@Error_Incorrect_credentials', data.longWait);
        }
    });
    this.Then(/^Check the error messages on Login page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageCP.waitForElementVisible('@Error_Incorrect_credentials', data.longWait)
                .waitForElementVisible('@inputUsernameCP',data.LoginWait)
                .waitForElementNotPresent('@Invalid_Error1',data.LoginWait)
                .waitForElementNotPresent('@Invalid_Error2',data.LoginWait)
                .waitForElementNotPresent('@Invalid_Error3',data.LoginWait);

        }
    });

    //@5
    this.When(/^Change https to http and try to open the URL$/, function () {

        var URL2;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL2 = data.ConsultantURL2;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL2);
                browser.timeoutsImplicitWait(40000);
            });
        }
    });
    this.Then(/^Consultant site must not open with http site$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {

            LoginPageCP.waitForElementPresent('@CantReachError', data.LoginWait);
        }
    });

    //@6
    this.When(/^Open the Iframe-HTML file in any browser$/, function () {

        var URL4;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL4 = data.ConsultantIframeFile;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL4);
                browser.pause(10000);
            });
        }
    });
    this.When(/^Check consultant-site is vulnerable or not$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            var VulnerableText="//p[contains(text(),'This website is vulnerable to Clickjacking')]";
            var ConsultantSite="//p[contains(text(),'Register your account now')]";
            var Xpathgg="//label[@for='Username']";
            browser.useXpath().waitForElementNotPresent(VulnerableText, data.longWait);
            browser.pause(5000)
                .frame('ITest')
            browser.useXpath().waitForElementNotPresent(ConsultantSite, data.longWait)
            browser.useXpath().waitForElementNotPresent(Xpathgg, data.longWait);

        }
    });

    //@4
    this.When(/^Enter valid username and password and click Login on Consultant Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            console.log(data.ConsultantlUsername3)
            console.log(data.ConsultantPassword3)
            LoginPageCP.waitForElementVisible('@inputPasswordCP', data.longWait)
                .clearValue('@inputUsernameCP')
                .setValue('@inputUsernameCP',data.ConsultantlUsername3)
                .clearValue('@inputPasswordCP')
                .setValue('@inputPasswordCP',data.ConsultantPassword3)
                .click('@ContinueButtonLoginCP');
            browser.pause(5000);
        }
    });
    this.When(/^Consultant Portal Dashboard should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            DashboardPageBP.waitForElementVisible('@DashboardTitleBP', data.longWait)
                .waitForElementVisible('@ClientsLinkTab',data.LoginWait)
                .waitForElementVisible('@ResourcesTab',data.LoginWait);
        }
    });
    this.When(/^Go to Upload File page in Consultant and upload unexpected file$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {

            URL = data.UploadURL;
            browser.maximizeWindow()
                .url(URL);
            UploadFile.waitForElementPresent('@EmployeeDataTitle', data.LoginWait)
                    .waitForElementPresent('@ChooseFile1', data.LoginWait)
                browser.pause(5000);
                browser.setValue('input[type="file"]', require('path').resolve('C:/Users/raghun_app/Desktop/Consultant portal data for auto run/Doc8.docx'));
                browser.pause(5000);
                UploadFile.waitForElementVisible('@UploadFilebutton', data.LoginWait);
                browser.pause(2000);
                UploadFile.click('@UploadFilebutton');
                browser.pause(5000);

        }
    });
    this.Then(/^Application does not allow the upload of unexpected file types and should Throws error$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD" ) {
            UploadFile.waitForElementPresent('@ErrorIcon1',data.LoginWait)
                .waitForElementPresent('@ErrorMessage1',data.LoginWait);
            browser.pause(10000);

        }
    });
    this.Then(/^User able to logout Successfully from Consultant Portal/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {

            var xpath45t="//div[@class='user-pic small']";
            var xpath101t="//a[contains(text(),'Log Out')]";
            browser.useXpath().moveToElement(xpath45t,0,1).pause(3000);
            browser.useXpath().click(xpath101t);
            browser.pause(5000);
        }
        LoginPageCP.waitForElementPresent('@inputUsernameCP',data.LoginWait);
    });

//@7
    this.Then(/^Consultant User's Account should get locked$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageCP.waitForElementVisible('@inputPasswordCP', data.longWait)
                .setValue('@inputUsernameCP',data.ConsultantUsername2)
                .setValue('@inputPasswordCP',data.InvalidPassword1)
                .click('@ContinueButtonLoginCP')
                .waitForElementVisible('@AccountLockError', data.longWait)
                .waitForElementVisible('@inputUsernameCP',data.LoginWait);
        }
    });

//@10
    this.When(/^User must not use the CP-application for the specified time limit$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            //browser.waitForElementPresent('@Timeout',data.sessionTimeout).acceptAlert();
            browser.pause(1400000);
        }
    });
    this.Then(/^The CP-Application has automatically logged you out$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageCP.waitForElementPresent('@inputUsernameCP',data.LoginWait);

        }
    });

//@8
this.When(/^Check Username and password field$/, function () {

    var URL;
    browser = this;
    var execEnv = data["TestingEnvironment"];
    if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
        LoginPageCP.waitForElementVisible('@UsernameType', data.longWait)
            .waitForElementVisible('@PasswordTypeBefore', data.longWait);
         //   .waitForElementVisible('@UsernameValueInDOMBefore', data.longWait);
    }
});
this.Then(/^Enter Credentials and check that the credentials are encrypted$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageCP.waitForElementVisible('@inputUsernameCP', data.longWait)
                .setValue('@inputUsernameCP',"abcdad")
                .setValue('@inputPasswordCP',"abcds")
                .click('@ContinueButtonLoginCP')
                .waitForElementVisible('@UsernameValueInDOMAfter', data.longWait)
                .waitForElementVisible('@PasswordTypeAfter', data.longWait)
               //.waitForElementVisible('@PasswordClassAfter', data.longWait)
                .waitForElementNotPresent('@PasswordClassBefore', data.longWait)
                .waitForElementNotPresent('@UsernameValueInDOMBefore', data.longWait);


        }
    });

//@9
    this.When(/^Server are down and Try to open the consultant Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            URL = data.ConsultantURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
            });
        }
    });
    this.Then(/^Verify that the program codes must not be displayed$/, function () {

        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "CSO" || execEnv.toUpperCase() == "PROD") {
            LoginPageCP.waitForElementNotPresent('@inputPasswordCP', data.longWait)
                .waitForElementNotPresent('@inputPasswordCP', data.longWait)
                .waitForElementNotPresent('@ContinueButtonLoginCP', data.longWait);
            DashboardPageBP.waitForElementNotPresent('@DashboardTitleBP', data.longWait)
                .waitForElementNotPresent('@ClientsLinkTab',data.LoginWait)
                .waitForElementNotPresent('@ResourcesTab',data.LoginWait);


        }
    });

}


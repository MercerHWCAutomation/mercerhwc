#Created by Triveni Gherde on 10/11/2017.
Feature: HR-Admin Portal Security Scenarios

  @Security @HRAdminPortalLoginAuthorizationTesting@1
  Scenario: Verify that an  ppt without HR status cannot access the HR Admin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter non-HR username and password and click Login on HRAdmin Portal Login Page
    Then    User must not be able to Login


  @Security @BypassingAuthenticationSchemaTesting @3
  Scenario: Verify that a user is not able to access the file/page of the application before login
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Try to access the HR admin page without Login
    Then    Page should not open and ppt should get redirected to login page

  @Security @ErrorCodeTesting @4
  Scenario: verify the error message after making invalid attempt on HR Admin login page.
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter invalid username or password and click Login on HRAdmin Portal Login Page
    Then    User must not be able to Login
    Then    Check the error messages

  @Security @UploadOfUnexpectedFileTypes @5
  Scenario: verify that the application does not allow the upload of unexpected file types
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page new
    And     HRAdmin Portal Dashboard should be displayed
    And     Go to Upload File page and upload unexpected file
    Then    Application does not allow the upload of unexpected file types and Throws error
    Then    User able to logout Successfully from HR Admin Portal


  @Security @HTTPStrictTransportSecurity @6
  Scenario: To verify security of HR-Admin application
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Change https to http and try opening the URL
    Then    URL must not open with http site

  @Smoke @IFrame @7
  Scenario: To verfiy whether HR Admin Portal opens in an iframe
    Given  Open the HTML file in any browser
    When   Check site is vulnerable or not


  @Security @HRAdminPortalLockOutTesting @2
  Scenario: Verify that user gets locked out and after making number of invalid attempts to login
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter invalid username or password and click Login on HRAdmin Portal Login Page1
    And     Enter invalid username or password and click Login on HRAdmin Portal Login Page1
    And     Enter invalid username or password and click Login on HRAdmin Portal Login Page1
    And     Enter invalid username or password and click Login on HRAdmin Portal Login Page1
    And     Enter invalid username or password and click Login on HRAdmin Portal Login Page1
    And     Enter invalid username or password and click Login on HRAdmin Portal Login Page1
    Then    User's Account should get locked

  @Smoke @SessionTimeout @10
  Scenario: To verify session timeout in HR-Admin application
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User must not use the application for the specified time limit
    Then    The Application has automatically logged you out

  @Smoke @HRAdmin-CredentialsEncrypted  @8
  Scenario: To verify credentials should be encrypted in HRAdmin application
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Check the Username and password field
    Then    Enter the Credentials and check that the credentials are encrypted

#Run the below script when servers are down
  @Smoke @HP-SecurityWhenServerIsDown  @9
  Scenario: To verify that when the servers are down,the program codes must not be displayed
    Given   Server are down and Try to open the HRAdmin Portal URl
    When    Verify that the program codes must not be displayed when servers are down





#########
  #########@Smoke @TestingforBrowsercacheweakness  @7
  #########Scenario: To verify that the HR-Admin application does not remember sensitive data
    #########Given   User opens the browser and launch the HRAdmin Portal URl
    #########When    Click on the username field,Browser must not prompt the attacker with recently used usernames
    #########Then    User able to logout Successfully from HR Admin Portal





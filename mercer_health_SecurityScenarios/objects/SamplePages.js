/**
 * Created by Mohsin on 12/12/2017.
 * Updated by Rachana on 03/01/2018
 */



module.exports = {
    sections: {
        LoginPage: {
            selector: 'body',
            elements: {
                inputUsername: {locateStrategy: 'xpath', selector: "//input[@id='usernameId']"},
                inputPassword: {locateStrategy: 'xpath', selector: "//input[@id='passwordId']"},
                SubmitButton: {locateStrategy: 'xpath', selector: "//input[@type='submit'][@value='Submit']"}
            }
        },
        DashboardPage: {
            selector: 'body',
            elements: {
                docUploadButton: {locateStrategy: 'xpath', selector: "//button[@id='RequiredDocumentsBtn']"},
                seeDetailAndUploadButton: {
                    locateStrategy: 'xpath',
                    selector: "//a[@class='btn btn-primary close-docs-mega-nav ng-scope']"
                }
            }
        },
        DocuploadPage: {
            selector: 'body',
            elements: {
                selectFileButton: {
                    locateStrategy: 'xpath', selector: "//button[.='Select File']"},
                Uploadbutton: {
                    locateStrategy: 'xpath', selector: "//button[@class='btn btn-primary ng-binding']"},
                LogoutButton:{
                    locateStrategy: 'xpath', selector: "//a[.='Logout']"}


            }
        }
    }
}


module.exports = {
    sections: {
        DashboardPage: {
            selector: 'body',
            elements: {
                Username: {locateStrategy: 'xpath',selector: "//input[@name='Username']"},
                Password: {locateStrategy: 'xpath',selector: "//input[@id='Password']"},
                SubmitButton: {locateStrategy: 'xpath',selector: "//button[@type='submit']"},
                MenuButton: {locateStrategy: 'xpath',selector: "//span[.='Menu']"},
                CompleteLifeEventButton: {locateStrategy: 'xpath',selector: "//span[.='Complete Life Event']"},
                GetStartedTab: {locateStrategy: 'xpath',selector: "(//ul[@class='nav nav-pills tab-pills hide-arrows']/li)[1]"},
                WhosCoveredTab: {locateStrategy: 'xpath',selector: "//button[.='NEXT']"},
                AddDependentButton: {locateStrategy: 'xpath',selector: "//span[.='Add a Dependent']"},
                ContinueButton: {locateStrategy: 'xpath',selector: "(//button[.='Continue'])[2]"},
                SSN: {locateStrategy: 'xpath',selector: "//input[@name='SSN']"},
                Random: {locateStrategy: 'xpath',selector: "(//button[@name='Gender'])[2]"},
                ContinueButtoninWCP: {locateStrategy: 'xpath',selector: "//span[.='Continue']"},
                EditButtoninWCP: {locateStrategy: 'xpath',selector: "//span[.='Edit']"},
                Mobiletextfield: {locateStrategy: 'xpath',selector: "(//input[@name='phone'])[2]"},
                Random1: {locateStrategy: 'xpath',selector: "(//input[@name='phone'])[3]"},
                Random2: {locateStrategy: 'xpath',selector: "(//input[@name='phone'])[3]"},
                EstimateNow: {locateStrategy: 'xpath',selector: "(//span[.='ESTIMATE NOW'])[1]"},
                Profilepage: {locateStrategy: 'xpath',selector: "//a[@title='Profile']"},
                EditbuttoninProfilepage: {locateStrategy: 'xpath',selector: "//span[.='Edit']"},
                Addressfield: {locateStrategy: 'xpath',selector: "(//input[@name='AddressField'])[2]"},
                Random3: {locateStrategy: 'xpath',selector: "(//input[@name='AddressField'])[1]"},
                LogoutButton: {locateStrategy: 'xpath',selector: "//a[text()='Logout']"},
                DashboardTab: {locateStrategy: 'xpath',selector: "//span[contains(text(),'Dashboard')]"},
                LockError: {locateStrategy: 'xpath', selector: "//div[@id='dialog']/i"},
                CredsError: {locateStrategy: 'xpath', selector: "//div[@id='dialog']/i"},
                ErrorUrl: {locateStrategy: 'xpath', selector: "//div[@id='main-message']/h1"},
                FNPassword: {locateStrategy: 'xpath', selector: "//input[@type='password']"}
            }
        }
    }};
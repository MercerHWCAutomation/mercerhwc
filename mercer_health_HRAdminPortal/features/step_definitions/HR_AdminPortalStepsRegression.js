/**
 * Created by subhajit-chakraborty on 6/9/2017.
 */

var data = require('../../TestResources/HR_AdminPortalGlobalData.js');
var Objects = require(__dirname + '/../../repository/HRAdminPortalPages.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');

var LoginPageHRAP,DashboardPageHRAP,DocumentLibraryPageHRAP,BenefitsPageHRAP,EducationPageHRAP,EmployeePageHRAP,ReportingPageHRAP,TestingPageHRAP,AddEmployeePage;
var count=0;

function initializePageObjects(browser, callback) {
    var BPPage = browser.page.HRAdminPortalPages();
    LoginPageHRAP = BPPage.section.LoginPageHRAP;
    DashboardPageHRAP = BPPage.section.DashboardPageHRAP;
    DocumentLibraryPageHRAP = BPPage.section.DocumentLibraryPageHRAP;
    EducationPageHRAP = BPPage.section.EducationPageHRAP;
    EmployeePageHRAP = BPPage.section.EmployeePageHRAP;
    ReportingPageHRAP = BPPage.section.ReportingPageHRAP;
    TestingPageHRAP = BPPage.section.TestingPageHRAP;
    AddEmployeePage = BPPage.section.AddEmployeePage;
    BenefitsPageHRAP = BPPage.section.BenefitsPageHRAP;
    callback();
}

module.exports = function() {

    this.Given(/^User opens the browser and launch the HRAdmin Portal URl$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF") {
            URL = data.HRAdminURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
                LoginPageHRAP.waitForElementVisible('@inputUsernameHRAP', data.longWait);

                //extra
              //  var a1='#Username';
              //  LoginPageHRAP.style.backgroundColor('FFFFFF')
              // // browser.useCss().moveToElement(a1,0,0).setProperty('backgroundColor','FFFFFF',1)
              //  browser.pause(10000);
            });
        }
    });

    this.When(/^Enter valid username and password and click Login on HRAdmin Portal Login Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            LoginPageHRAP.waitForElementVisible('@inputPasswordHRAP', data.longWait)
                .setValue('@inputUsernameHRAP',data.HRAdminPortalUsername)
                .setValue('@inputPasswordHRAP',data.HRAdminPortalPassword)
                .click('@ContinueButtonLoginHRAP');
        }
    });

    this.When(/^HRAdmin Portal Dashboard should be displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@MyDashboardHeader', data.longWait)
                .waitForElementVisible('@ViewAllEmployeeButton', data.longWait)
                .waitForElementVisible('@EmployeeSnapshotTile', data.longWait)
                .waitForElementVisible('@ActivityStreamTile', data.longWait)
                .waitForElementVisible('@MessageCenterTile', data.longWait)
                .waitForElementVisible('@PayrollTile', data.longWait)
                .waitForElementVisible('@ReportingTile', data.longWait)
                .waitForElementVisible('@DashboardMenuFlyoverTab', data.longWait)
                .waitForElementVisible('@EmployeesMenuFlyover', data.longWait)
                .waitForElementVisible('@BenefitsMenuFlyover', data.longWait)
                .waitForElementVisible('@ReportingMenuFlyover', data.longWait)
                .waitForElementVisible('@PayrollMenuFlyover', data.longWait)
                .waitForElementVisible('@ResourcesMenuFlyover', data.longWait)
                .waitForElementVisible('@TestingMenuFlyover', data.longWait);
        }
    });

    this.Then(/^Dashboard Page should be in default view for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DayToDayTab', data.longWait)
                .waitForElementVisible('@ViewAllEmployeeButton', data.longWait)
                .waitForElementVisible('@EmployeeSnapshotTile', data.longWait)
                .waitForElementVisible('@ActivityStreamTile', data.longWait)
                .waitForElementVisible('@MessageCenterTile', data.longWait)
                .waitForElementVisible('@PayrollTile', data.longWait)
                .waitForElementVisible('@ReportingTile', data.longWait)
                .waitForElementVisible('@DashboardMenuFlyoverTab', data.longWait)
                .waitForElementVisible('@EmployeesMenuFlyover', data.longWait)
                .waitForElementVisible('@BenefitsMenuFlyover', data.longWait)
                .waitForElementVisible('@ReportingMenuFlyover', data.longWait)
                .waitForElementVisible('@PayrollMenuFlyover', data.longWait)
                .waitForElementVisible('@ResourcesMenuFlyover', data.longWait)
                .waitForElementVisible('@TestingMenuFlyover', data.longWait);
        }
    });

    this.When(/^User able to open Document Library Page HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DocumentLibraryQuicklinks', data.longWait)
                .click('@DocumentLibraryQuicklinks');
            DocumentLibraryPageHRAP.waitForElementVisible('@DocumentLibraryTitle',data.longWait)

        }
    });

    this.When(/^User able to view Tile and List view of document library for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DocumentLibraryPageHRAP.waitForElementVisible('@TileViewTab',data.longWait)
                .waitForElementVisible('@ListViewTab',data.longWait)
                .waitForElementVisible('@SolutionOverviewTab',data.longWait)
                .click('@SolutionOverviewTab')
                .waitForElementVisible('@DataManagementTab',data.longWait)
                .click('@DataManagementTab')
                .waitForElementVisible('@PayrollAndReportsTab',data.longWait)
                .click('@PayrollAndReportsTab')
                .waitForElementVisible('@SpendingAccountsTab',data.longWait)
                .click('@SpendingAccountsTab')
                .waitForElementVisible('@ProcessSupportTab',data.longWait)
                .click('@ProcessSupportTab')
                .waitForElementVisible('@COBRATab',data.longWait)
                .click('@COBRATab')


        }
    });

    this.Then(/^User able to perform download and sorting action on the files for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DocumentLibraryPageHRAP.waitForElementVisible('@AscendingTab',data.longWait)
                .waitForElementVisible('@DescendingTab',data.longWait)
                .click('@AscendingTab')
                .click('@DescendingTab')


        }
    });

    this.When(/^User able to open Education Page for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@EducationQuicklinks',data.longWait)
                .click('@EducationQuicklinks');

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });
        }
    });

    this.Then(/^Video should work and can move to next Video Page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF") {

            EducationPageHRAP.waitForElementVisible('@CanvasButtonHRAP', data.longWait)
                .click('@CanvasButtonHRAP')
                .waitForElementVisible('@LearnMoreButton', data.longWait)
                .click('@LearnMoreButton')
                .waitForElementVisible('@VideoDashboardTab', data.longWait)
        }
    });

    this.Then(/^User able to open the Employee Page screen$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF") {

            DashboardPageHRAP.waitForElementVisible('@ViewAllEmployeeButton',data.longWait)
                .click('@ViewAllEmployeeButton')
                .waitForElementVisible('@AddNewEmployeeButton',data.longWait)
        }
    });

    this.Then(/^User able to navigate to proxy access for MBC Portal for Promoted Clients$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF") {

            EmployeePageHRAP.waitForElementVisible('@EmployeeTitleHeader',data.longWait)
                .waitForElementVisible('@FirstLName',data.longWait)
                .click('@FirstLName')
                .waitForElementVisible('@ProxyAccessLinkFirst',data.LoginWait)
                .click('@ProxyAccessLinkFirst')

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });

            EmployeePageHRAP.waitForElementVisible('@EndImpersonationButton',data.LoginWait)
                .click('@EndImpersonationButton')

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[0];
                browser.switchWindow(handle);
                browser.pause(data.LoginWait)
                //browser.maximizeWindow();
            });
        }
    });

    this.Then(/^User able to logout Successfully form HR Admin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            var xpath="//div[@class='user-pic small']";
            var xpath1="//a[contains(text(),'Log Out')]";
            browser.useXpath().moveToElement(xpath,0,1).pause(3000);
            browser.click(xpath1);
            browser.pause(5000);
        }
        LoginPageHRAP.waitForElementVisible('@inputUsernameHRAP',data.LoginWait)
    });

    this.When(/^User clicks on reporting tab and reporting Page is opened$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@ReportingMenuFlyover',data.LoginWait)
                .click('@ReportingMenuFlyover');
            ReportingPageHRAP.waitForElementVisible('@ReportingTitleHeader',data.LoginWait)
        }
    });

    this.Then(/^User able to create and download the report created by user$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            ReportingPageHRAP.waitForElementVisible('@SelectReportTypeDropdown',data.LoginWait)
                .click('@SelectReportTypeDropdown')
                .waitForElementVisible('@AlllifeEventDropdown',data.LoginWait)
                .click('@AlllifeEventDropdown')
                .waitForElementVisible('@CreateReportButton',data.LoginWait)
                .click('@CreateReportButton')
                .waitForElementVisible('@IncludeEventDropdown',data.LoginWait)
                .click('@IncludeEventDropdown')
                .waitForElementVisible('@AllEventOptionIEDropdown',data.LoginWait)
                .click('@AllEventOptionIEDropdown')
                .waitForElementVisible('@EffectiveDateStartValue',data.LoginWait)
                .waitForElementVisible('@EffectiveDateEndValue',data.LoginWait)
                .setValue('@EffectiveDateStartValue',data.ReportStartDate)
                .setValue('@EffectiveDateEndValue',data.ReportEndDate)
                .waitForElementVisible('@GenerateReportButton',data.LoginWait)
                .click('@GenerateReportButton')
                .waitForElementVisible('@ReportSuccessMsg',data.LoginWait)
                .waitForElementVisible('@ReportDownloadButton',data.LoginWait)

        }
    });

    this.Then(/^New report is added to the row of all reports$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            ReportingPageHRAP.waitForElementVisible('@CloseButtonReportOption',data.LoginWait)
                .click('@CloseButtonReportOption')
                .waitForElementVisible('@LatestCreatedReportValue',data.LoginWait)
                .click('@LatestCreatedReportValue')
                .waitForElementNotVisible('@ErrorMsgReportDownload',data.LoginWait)


        }
    });


    //Regression Steps:

    this.Given(/^User click on the Testing tab and the the edit tab of Profile page$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@TestingTab', data.longWait)
                .click('@TestingTab');
            TestingPageHRAP.waitForElementVisible('@CloseButtonTestingTab',data.longWait)
                .click('@CloseButtonTestingTab')
                .waitForElementVisible('@FirstClientTesting',data.longWait)
                .click('@FirstClientTesting')
                .waitForElementVisible('@EditProfileButtonTestingTab',data.longWait)
                .click('@EditProfileButtonTestingTab')
                .waitForElementVisible('@EffectiveDateTextbox',data.longWait);
        }
    });

    this.When(/^User make some changes and click on the save button$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            TestingPageHRAP.waitForElementVisible('@EffectiveDateTextbox',data.longWait)
                .setValue('@EffectiveDateTextbox',data.EffectiveDateTesting)
                .waitForElementVisible('@SaveButtonEmpDetails',data.longWait)
                .click('@SaveButtonEmpDetails');
        }
    });

    this.Then(/^User lands on the review page and verify the Congratulation Message$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            TestingPageHRAP.waitForElementVisible('@CongratulationMsgEmpTesting',data.longWait)
                .waitForElementVisible('@ChangesReqCongMsg',data.longWait)
        }
    });

    this.Then(/^User open day to day tab and required page should open$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DayToDayTab',data.longWait)
                .click('@DayToDayTab')
                .waitForElementVisible('@TotalEmployeesTab',data.longWait)
        }
    });

    this.Then(/^User clicks on Open Enrollment tab and required field is displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@OpenEnrollmentTab',data.longWait)
                .click('@OpenEnrollmentTab')
            this.pause(5000);
               // .waitForElementVisible('@TotalEmployeesTab',data.longWait)
        }
    });

    this.Then(/^Only one Day to Day tab should be displayed in default view$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DayToDayTab',data.longWait)
                .click('@DayToDayTab')
                .waitForElementVisible('@TotalEmployeesTab',data.longWait)
        }
    });

    this.Then(/^User able to navigate to proxy access for MBC Portal for Promoted Clients to start event$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAF") {

            EmployeePageHRAP.waitForElementVisible('@EmployeeTitleHeader',data.longWait)
                .waitForElementVisible('@FirstLName',data.longWait)
                .click('@FirstLName')
                .waitForElementVisible('@ProxyAccessLinkFirst',data.LoginWait)
                .click('@ProxyAccessLinkFirst')

            browser.pause(data.LoginWait)
            browser.windowHandles(function(result) {
                var handle = result.value[1];
                browser.switchWindow(handle);
                browser.maximizeWindow();
            });

            //EmployeePageHRAP.waitForElementVisible('@',data.longWait)




















        }
    });

    this.When(/^User opens the Open Enrollment Tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@OpenEnrollmentTab',data.longWait)
                .click('@OpenEnrollmentTab')
               this.pause(10000);
        }
    });

    this.Then(/^User review the daily statistic table and all items should be properly displayed$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DateTab',data.longWait)
                .waitForElementVisible('@EnrollmentTab',data.longWait)
                .waitForElementVisible('@ChangesTab',data.longWait)
                .waitForElementVisible('@TotalActivityTab',data.longWait)
                .waitForElementVisible('@EmployeeActivityTab',data.longWait)
                .waitForElementVisible('@HRActivityTab',data.longWait)
                .waitForElementVisible('@CallCenterActivityTab',data.longWait)
                .waitForElementVisible('@DownloadActivityReportTab',data.longWait);

            //var count = browser.elements("xpath","//tbody/tr", function(result){
            //    var els = result.value;
            //    var i = 0;
            //    els.forEach(function(el){
            //        browser.elementIdText(el.ELEMENT, function(text) {
            //            i++;
            //        });
            //    });
            //    return i;
            //});
            //console.log(count);

            var getWebElementsCount = function (callback) {
                browser.elements('xpath','//tbody/tr', function (webElementsArray) {
                    count = webElementsArray.value.length;
                    callback(count);
                });
            }
            console.log(count.value);

            //click on the download button:
            DashboardPageHRAP.waitForElementVisible('@DownloadActivityReportTab',data.longWait)
                .click('@DownloadActivityReportTab')
                .waitForElementVisible('@ActivityStatusSelectBox',data.longWait)
                .click('@ActivityStatusSelectBox')
                .waitForElementVisible('@AllActivitiesOptions',data.longWait)
                .click('@AllActivitiesOptions');
            ReportingPageHRAP.waitForElementVisible('@GenerateReportButton',data.longWait)
                .click('@GenerateReportButton');
            browser.pause(data.longWait);
            //ReportingPageHRAP.waitForElementVisible('@ReportDownloadButton',data.longWait)
            //    .click('@ReportDownloadButton');








        }
    });

    this.Then(/^Two graphs should be presented and Daily Activity should be selected by default$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DailyActivityHeader',data.longWait)
                .waitForElementVisible('@DailyActivityTab',data.longWait)
                .waitForElementVisible('@CummulativeActivityTab',data.longWait)
                .waitForElementVisible('@DailyActivityDefaultSelect',data.longWait)
                .waitForElementVisible('@EmployeeEnrolledSelect',data.longWait)
                .waitForElementVisible('@ElectionChangesSelect',data.longWait)
                .waitForElementVisible('@DailyActivityXAxis',data.longWait)
                .waitForElementVisible('@DailyActivityYAxis',data.longWait)
                .click('@CummulativeActivityTab')
                .waitForElementVisible('@DailyActivityXAxis',data.longWait)
                .waitForElementVisible('@DailyActivityYAxis',data.longWait);
        }
    });

    this.Then(/^Download Activity Report Button should be present$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DownloadActivityReportTab',data.longWait);
        }
    });

    this.When(/^User clicks on the View Employee Tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@EmployeesMenuFlyover',data.longWait)
                .click('@EmployeesMenuFlyover')
                .waitForElementVisible('@ViewEmployeesTab',data.longWait)
                .click('@ViewEmployeesTab')
        }
    });

    this.When(/^User creates a new employee with all details and saves the Employee$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            AddEmployeePage.waitForElementVisible('@EmployeeTitleHeader',data.longWait)
                .waitForElementVisible('@AddNewEmployeeButton',data.longWait)
                .click('@AddNewEmployeeButton')
                .waitForElementVisible('@DemographicsInfoTitle',data.longWait);

            //entering all the details:

            AddEmployeePage.waitForElementVisible('@FirstNameTextbox',data.longWait)
                .setValue('@FirstNameTextbox',data.FirstName)
                .waitForElementVisible('@MiddleNameTextbox',data.longWait)
                .setValue('@MiddleNameTextbox',data.MiddleName)
                .waitForElementVisible('@LastNameTextbox',data.longWait)
                .setValue('@LastNameTextbox',data.LastName)
                .waitForElementVisible('@AddressLine1Textbox',data.longWait)
                .setValue('@AddressLine1Textbox',data.Address1)
                .waitForElementVisible('@AddressLine2Textbox',data.longWait)
                .setValue('@AddressLine2Textbox',data.Address2)
                .waitForElementVisible('@CityTextbox',data.longWait)
                .setValue('@CityTextbox',data.City)
                .waitForElementVisible('@ZipTextbox',data.longWait)
                .setValue('@ZipTextbox',data.Zipcode)

                .waitForElementVisible('@StateDropdown',data.longWait)
                .click('@StateDropdown')
                .waitForElementVisible('@AlbamaOptionStateDropdown',data.longWait)
                .click('@AlbamaOptionStateDropdown')
                .waitForElementVisible('@GenderDropdown',data.longWait)
                .click('@GenderDropdown')
                .waitForElementVisible('@MaleOptionGender',data.longWait)
                .click('@MaleOptionGender')

                .waitForElementVisible('@DOBTextbox',data.longWait)
                .setValue('@DOBTextbox',data.DOB)
                .waitForElementVisible('@EmployerAssignIDTextbox',data.longWait)
                .setValue('@EmployerAssignIDTextbox',data.EmployerAssignID)
                .waitForElementVisible('@SSNTextbox',data.longWait)
                .setValue('@SSNTextbox',data.SSN)
                .waitForElementVisible('@PersonalPhoneTextbox',data.longWait)
                .setValue('@PersonalPhoneTextbox',data.PersonalPhone)
                .waitForElementVisible('@HomeEmailTextbox',data.longWait)
                .setValue('@HomeEmailTextbox',data.HomeEmailID)
                .waitForElementVisible('@CompanyMailTextbox',data.longWait)
                .setValue('@CompanyMailTextbox',data.CompanyEmailID)
                .waitForElementVisible('@OriginalHireDateTextbox',data.longWait)
                .setValue('@OriginalHireDateTextbox',data.OriginalHireDate)
                .waitForElementVisible('@JobTitleTextbox',data.longWait)
                .setValue('@JobTitleTextbox',data.JobTitle)

                .waitForElementVisible('@WorkStateDropdown',data.longWait)
                .click('@WorkStateDropdown')
                .waitForElementVisible('@AlaskaWorkStateDropdown',data.longWait)
                .click('@AlaskaWorkStateDropdown')
                .waitForElementVisible('@StatusDropdown',data.longWait)
                .click('@StatusDropdown')
                .waitForElementVisible('@FullTimeStatusDropdown',data.longWait)
                .click('@FullTimeStatusDropdown')
                .waitForElementVisible('@DivisionDropdown',data.longWait)
                .click('@DivisionDropdown')
                .waitForElementVisible('@Option1DivisionDropdown',data.longWait)
                .click('@Option1DivisionDropdown')

                .waitForElementVisible('@ClientDefinedReporting1Textbox',data.longWait)
                .setValue('@ClientDefinedReporting1Textbox',data.ClientDefinedReporting1)
                .waitForElementVisible('@ClientDefinedReporting2Textbox',data.longWait)
                .setValue('@ClientDefinedReporting2Textbox',data.ClientDefinedReporting2)
                .waitForElementVisible('@ClientDefinedReporting3Textbox',data.longWait)
                .setValue('@ClientDefinedReporting3Textbox',data.ClientDefinedReporting3)
                .waitForElementVisible('@ClientDefinedReporting4Textbox',data.longWait)
                .setValue('@ClientDefinedReporting4Textbox',data.ClientDefinedReporting4)
                .waitForElementVisible('@ClientDefinedReporting5Textbox',data.longWait)
                .setValue('@ClientDefinedReporting5Textbox',data.ClientDefinedReporting5)

                .waitForElementVisible('@MostRecentHireDateTextbox',data.longWait)
                .setValue('@MostRecentHireDateTextbox',data.MostRecentHireDate)

                .waitForElementVisible('@LocationDropdown',data.longWait)
                .click('@LocationDropdown')
                .waitForElementVisible('@Option1LocationDropdown',data.longWait)
                .click('@Option1LocationDropdown')
                .waitForElementVisible('@EmployementStatusDropdown',data.longWait)
                .click('@EmployementStatusDropdown')
                .waitForElementVisible('@ActiveOptionEmpStatusDropdown',data.longWait)
                .click('@ActiveOptionEmpStatusDropdown')
                .waitForElementVisible('@ExecuteStatusDropdown',data.longWait)
                .click('@ExecuteStatusDropdown')
                .waitForElementVisible('@ExecutiveOptionExecuteStatusDropdown',data.longWait)
                .click('@ExecutiveOptionExecuteStatusDropdown')

                .waitForElementVisible('@CurrentSalaryTextbox',data.longWait)
                .setValue('@CurrentSalaryTextbox',data.CurrentSalary)
                .waitForElementVisible('@BenefitSalaryTextbox',data.longWait)
                .setValue('@BenefitSalaryTextbox',data.BenefitSalary)

                .waitForElementVisible('@PayrollFreqDropdown',data.longWait)
                .click('@PayrollFreqDropdown')
                .waitForElementVisible('@MonthlyOptionPayrollFreqDropdown',data.longWait)
                .click('@MonthlyOptionPayrollFreqDropdown')
                .waitForElementVisible('@PaytypeDropdown',data.longWait)
                .click('@PaytypeDropdown')
                .waitForElementVisible('@HourlyOptionPaytypeDropdown',data.longWait)
                .click('@HourlyOptionPaytypeDropdown')

                .waitForElementVisible('@SaveButton',data.longWait)
                .click('@SaveButton')



















        }
    });

    this.Then(/^User able to validate the number of employees increased by 1 after adding employee$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@DashboardMenuFlyoverTab',data.longWait)
                .click('@DashboardMenuFlyoverTab')
                .waitForElementVisible('@OpenEnrollmentTab',data.longWait)
                .click('@OpenEnrollmentTab')
            //    .waitForElementVisible('@NotStartedNumber',data.longWait)

           // var value = DashboardPageHRAP.getText('@NotStartedNumber')
           //    this.demoTest = function (browser) {
           //     browser.assert.valueContains("(//*[@class='highcharts-series-group'])[last()-1]/..//*[@class='highcharts-title']", value+1);
           //   };

        }
    });

    this.When(/^User clicks on the benefit tab and open the Overview tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DashboardPageHRAP.waitForElementVisible('@BenefitsMenuFlyover',data.longWait)
                .click('@BenefitsMenuFlyover')
            BenefitsPageHRAP.waitForElementVisible('@BenefitsHeader',data.longWait)
        }
    });

    this.Then(/^User verifies the default view Benefit Overview Details$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            BenefitsPageHRAP.waitForElementVisible('@DetailsTab',data.longWait)
                .click('@DetailsTab')
                .waitForElementVisible('@MedicalTileTitle',data.longWait)
                .waitForElementVisible('@MedicalTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@MedicalCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@MedicalCarrierPlanTitle',data.longWait)

                .waitForElementVisible('@SupplementalTileTitle',data.longWait)
                .waitForElementVisible('@SupplementalTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@SupplementalCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@SupplementalCarrierPlanTitle',data.longWait)

                .waitForElementVisible('@DentalTileTitle',data.longWait)
                .waitForElementVisible('@DentalTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@DentalCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@DentalCarrierPlanTitle',data.longWait)

                .waitForElementVisible('@VisionTileTitle',data.longWait)
                .waitForElementVisible('@VisionTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@VisionCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@VisionCarrierPlanTitle',data.longWait)

                .waitForElementVisible('@AccountsTileTitle',data.longWait)
                .waitForElementVisible('@AccountsTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@AccountsCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@AccountsCarrierPlanTitle',data.longWait)

                .waitForElementVisible('@LifeTileTitle',data.longWait)
                .waitForElementVisible('@LifeTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@LifeCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@LifeCarrierPlanTitle',data.longWait)

                .waitForElementVisible('@DisabilityTileTitle',data.longWait)
                .waitForElementVisible('@DisabilityTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@DisabilityCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@DisabilityCarrierPlanTitle',data.longWait)

                .waitForElementVisible('@ProtectionTileTitle',data.longWait)
                .waitForElementVisible('@ProtectionTileImageDetailsPage',data.longWait)
                .waitForElementVisible('@ProtectionCarrierTitleDetailsPage',data.longWait)
                .waitForElementVisible('@ProtectionCarrierPlanTitle',data.longWait)
        }
    });

    this.Then(/^User verifies the default view Benefit Summary Details$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            BenefitsPageHRAP.waitForElementVisible('@MedicalTitle',data.longWait)
                .waitForElementVisible('@MedicalTileImage',data.longWait)
                .waitForElementVisible('@CarrierMedicalTitle',data.longWait)
                .waitForElementVisible('@CarrierMedicalPlanName',data.longWait)

                .waitForElementVisible('@SupplementalTitle',data.longWait)
                .waitForElementVisible('@SupplementalTileImage',data.longWait)
                .waitForElementVisible('@CarrierSupplementalTile',data.longWait)
                .waitForElementVisible('@CarrierSupplementalPlanName',data.longWait)

                .waitForElementVisible('@DentalTitle',data.longWait)
                .waitForElementVisible('@DentalTileImage',data.longWait)
                .waitForElementVisible('@CarrierDentalTile',data.longWait)
                .waitForElementVisible('@DentalSupplementalPlanName',data.longWait)

                .waitForElementVisible('@VisionTitle',data.longWait)
                .waitForElementVisible('@VisionTileImage',data.longWait)
                .waitForElementVisible('@CarrierVisionTile',data.longWait)
                .waitForElementVisible('@VisionSupplementalPlanName',data.longWait)

                .waitForElementVisible('@AccountsTitle',data.longWait)
                .waitForElementVisible('@AccountsTileImage',data.longWait)
                .waitForElementVisible('@CarrierAccountsTile',data.longWait)
                .waitForElementVisible('@AccountsSupplementalPlanName',data.longWait)

                .waitForElementVisible('@LifeTitle',data.longWait)
                .waitForElementVisible('@LifeTileImage',data.longWait)
                .waitForElementVisible('@CarrierLifeTile',data.longWait)
                .waitForElementVisible('@AccountsLifePlanName',data.longWait)

                .waitForElementVisible('@DisabilityTitle',data.longWait)
                .waitForElementVisible('@DisabilityTileImage',data.longWait)
                .waitForElementVisible('@DisabilityLifeTile',data.longWait)
                .waitForElementVisible('@AccountsDisabilityPlanName',data.longWait)

                .waitForElementVisible('@ProtectionTitle',data.longWait)
                .waitForElementVisible('@ProtectionTileImage',data.longWait)
                .waitForElementVisible('@ProtectionLifeTile',data.longWait)
                .waitForElementVisible('@AccountsProtectionPlanName',data.longWait)
        }
    });

    this.Then(/^User able to download the Download Requirement Excel_Pdf$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            BenefitsPageHRAP.waitForElementVisible('@DownloadReqTab',data.longWait)
                .click('@DownloadReqTab')
            browser.pause(data.longWait);
        }
    });

    this.When(/^User select unselect upto all files and change the view$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DocumentLibraryPageHRAP.waitForElementVisible('@TileViewTab',data.longWait)
                .waitForElementVisible('@ListViewTab',data.longWait)
        }
    });

    this.When(/^The file statuses are saved after view changing$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DocumentLibraryPageHRAP.click('@ListViewTab')
                .waitForElementVisible('@ListViewGrid',data.longWait)
                .click('@TileViewTab')
                .waitForElementVisible('@TileGridView',data.longWait)
        }
    });

    this.When(/^User opens the Document Library Page and checks the default view for HRAdmin$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF") {
            DocumentLibraryPageHRAP.waitForElementVisible('@TileViewTab',data.longWait)
                .waitForElementVisible('@ListViewTab',data.longWait)
                .waitForElementVisible('@SolutionOverviewTab',data.longWait)
                .waitForElementVisible('@DataManagementTab',data.longWait)
                .waitForElementVisible('@PayrollAndReportsTab',data.longWait)
                .waitForElementVisible('@ProcessSupportTab',data.longWait)
                .waitForElementVisible('@COBRATab',data.longWait)
                .waitForElementVisible('@AscendingTab',data.longWait)
                .waitForElementVisible('@DescendingTab',data.longWait)
        }
    });

    this.When(/^User able to download ZIP file and able to open it for HRAdmin Portal$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DocumentLibraryPageHRAP.waitForElementVisible('@AddToZip1st',data.LoginWait)
                .click('@AddToZip1st')
                .waitForElementVisible('@AddedToZip1st',data.LoginWait)
                .waitForElementVisible('@DownloadSelectedButton',data.LoginWait)
                .click('@DownloadSelectedButton');
            browser.pause(data.shortWait);
        }
    });

    this.Then(/^User reviews the enrolled employee tile$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPageHRAP.waitForElementVisible('@EmployeeSnapshotTab',data.LoginWait)
                .waitForElementVisible('@ActivityStreamTile',data.LoginWait)
        }
    });

    this.Then(/^Press the right upper corner for Enrolled Employee Tile$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QAF" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            DashboardPageHRAP.waitForElementVisible('@',data.LoginWait)
                .waitForElementVisible('@',data.LoginWait)
        }
    });










}

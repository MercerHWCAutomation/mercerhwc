Feature: HR Admin Portal Regression Test cases

  @Regression @HP_LoginFunctionality @pass
  Scenario: Checking the login flow for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    And     Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed

  @Regression @TestingEdit_Save
  Scenario: Checking the EditSave flow for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    And     Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User click on the Testing tab and the the edit tab of Profile page
    When    User make some changes and click on the save button
    Then    User lands on the review page and verify the Congratulation Message

  @Regression @TestingAdd_Save
  Scenario: Checking the AddSave flow for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    And     Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User click on the Testing tab and the the edit tab of Profile page
    When    User make some changes and click on the save button
    Then    User lands on the review page and verify the Congratulation Message

  @Regression @TestingTest_Employee
  Scenario: Checking the Testing TestEmployee flow for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User able to open the Employee Page screen
    And     User able to navigate to proxy access for MBC Portal for Promoted Clients to start event
    #Then    User initiate or complete the Life events of the MBC Employee
    #And     User clicks on Test Employee and MBC Employee page is opened

  @Regression @DashboardDefaultView @pass
  Scenario: Checking the default view of Dashboard for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When     Enter valid username and password and click Login on HRAdmin Portal Login Page
    Then    HRAdmin Portal Dashboard should be displayed

  @Regression @DashboardSwitchTabs @pass
  Scenario: Checking the switching tab of Dashboard for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    User open day to day tab and required page should open
    And     User clicks on Open Enrollment tab and required field is displayed

  @Regression @DashboardPlanYearIsInProgress @pass
  Scenario: Checking the switching tab of Dashboard for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    Only one Day to Day tab should be displayed in default view

  @Regression @DashboardDuringOrAfterOEAndBeforePY @pass
  Scenario: Checking the switching tab of Dashboard for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    User open day to day tab and required page should open
    And     User clicks on Open Enrollment tab and required field is displayed

  @Regression @DashboardViewReady @pass
  Scenario: Checking the switching tab of Dashboard for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    User open day to day tab and required page should open

    #----------------------------------------------
  @Regression @DocumentLibraryViewSwitching
  Scenario: Checking the Document Library Switching for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User able to open Document Library Page HRAdmin Portal
    And     User select unselect upto all files and change the view
    Then    The file statuses are saved after view changing


  @Regression @DocumentLibraryView
  Scenario: Checking the Document Library Switching for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User able to open Document Library Page HRAdmin Portal
    Then    User opens the Document Library Page and checks the default view for HRAdmin

  @Regression @DocumentLibraryDownloadSelected
  Scenario: Checking the Document Library Switching for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User able to open Document Library Page HRAdmin Portal
    Then    User able to download ZIP file and able to open it for HRAdmin Portal

  @Regression @EducationDefaultView @pass
  Scenario: Checking the Document Library Switching for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    User able to open Education Page for HRAdmin Portal

  @Regression @DashboardOEDAView
  Scenario: Checking the Dashboard in OE_DA View for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User opens the Open Enrollment Tab
    Then    User review the daily statistic table and all items should be properly displayed

  @Regression @DashboardOEDSView
  Scenario: Checking the Dashboard in OE_DS View for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User opens the Open Enrollment Tab
    Then    Two graphs should be presented and Daily Activity should be selected by default
    And     Download Activity Report Button should be present

  @Regression @DashboardOEDACAChartView
  Scenario: Checking the Dashboard in OE_DA_CA Chart View for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User opens the Open Enrollment Tab
    Then    Two graphs should be presented and Daily Activity should be selected by default
    And     Download Activity Report Button should be present

  @Regression @DashboardOEDStatusAddNewEmployee
  Scenario: Checking the Dashboard in OE_DS View for the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User clicks on the View Employee Tab
    And     User creates a new employee with all details and saves the Employee
    Then    User able to validate the number of employees increased by 1 after adding employee

  @Regression @BenefitOverviewDetailsDefaultView
  Scenario: Checking the default View for Benefit Overview Details the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User clicks on the benefit tab and open the Overview tab
    Then    User verifies the default view Benefit Overview Details

  @Regression @BenefitOverviewSummaryDefaultView
  Scenario: Checking the default View for Benefit Overview Summary the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User clicks on the benefit tab and open the Overview tab
    Then    User verifies the default view Benefit Summary Details

  @Regression @BenefitOverviewSummaryDefaultViewDownloadPDF
  Scenario: Checking the default View for Benefit Overview Summary the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    And     User clicks on the benefit tab and open the Overview tab
    Then    User able to download the Download Requirement Excel_Pdf

  @Regression @EmployeeSnapshotAfterPYED
  Scenario: Checking the employee snapshot After PYED the HRAdmin Portal
    Given   User opens the browser and launch the HRAdmin Portal URl
    When    Enter valid username and password and click Login on HRAdmin Portal Login Page
    And     HRAdmin Portal Dashboard should be displayed
    Then    User reviews the enrolled employee tile
    And     Press the right upper corner for Enrolled Employee Tile
























/**
 * Created by subhajit-chakraborty on 6/9/2017.
 */

module.exports = {
    sections: {
        LoginPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                inputUsernameHRAP: {selector: '#Username'},
                inputPasswordHRAP: {selector: '#Password'},
                ContinueButtonLoginHRAP: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Continue')]"},
            }
        },

        DashboardPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                MyDashboardHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'MY DASHBOARD')]"},
                DayToDayTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Day to Day')]"},
                OpenEnrollmentTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Open Enrollment')]"},
                EmployeeSnapshotTab: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Employee Snapshot')]"},
                DocumentLibraryQuicklinks:{locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..//a[contains(text(),'Document Library')]"},
                EducationQuicklinks: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Quick Links')]/..//a[contains(text(),'Education')]"},
                ViewAllEmployeeButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'View All Employees')]"},
                AddNewEmployeeButton: {locateStrategy: 'xpath',selector: "//a[contains(.,' Add New Employee')]"},
                EmployeeSnapshotTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Employee Snapshot')]"},
                ActivityStreamTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Activity Stream')]"},
                MessageCenterTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Message Center')]"},
                PayrollTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Payroll')]"},
                ReportingTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Reporting')]"},
                DashboardMenuFlyoverTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Dashboard')]"},
                EmployeesMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(.,'Employees ')]"},
                BenefitsMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Benefits')]"},
                ReportingMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Reporting')]"},
                PayrollMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Payroll')]"},
                ResourcesMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(.,'Resources ')]"},
                TestingMenuFlyover: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Testing')]"},

                //regression Add
                TestingTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Testing')]"},
                TotalEmployeesTab: {locateStrategy: 'xpath',selector: "(//h3[contains(text(),'Total Employees')])[1]"},
                OpenEnrollmentHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Open Enrollment Activity as of')]"},

                ActivityDetailsHeader: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Activity Details')]"},
                DownloadActivityReportTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Download Activity Report')]"},
                DateTab: {locateStrategy: 'xpath',selector: "//th[contains(text(),'Date')]"},
                EnrollmentTab: {locateStrategy: 'xpath',selector: "//th[contains(text(),'Enrollments')]"},
                ChangesTab: {locateStrategy: 'xpath',selector: "//th[contains(text(),'Changes')]"},
                TotalActivityTab: {locateStrategy: 'xpath',selector: "//th[contains(text(),'Total Daily Activity')]"},
                EmployeeActivityTab: {locateStrategy: 'xpath',selector: "//th[contains(text(),'Employee Activity')]"},
                HRActivityTab: {locateStrategy: 'xpath',selector: "//th[contains(text(),'HR Activity')]"},
                CallCenterActivityTab: {locateStrategy: 'xpath',selector: "//th[contains(text(),'Call Center Activity')]"},
                ActivityStatusSelectBox: {locateStrategy: 'xpath',selector: "//h2[contains(text(),'What type of activity status would you like to include?')]/../div/div/select"},
                AllActivitiesOptions: {locateStrategy: 'xpath',selector: "//h2[contains(text(),'What type of activity status would you like to include?')]/../div/div/select/option[contains(text(),'All Activities')]"},

                DailyActivityHeader: {locateStrategy: 'xpath',selector: "//h4[contains(text(),'Daily Activity')]"},
                DailyActivityTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Daily Activity')]"},
                CummulativeActivityTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Cumulative Activity')]"},
                DailyActivityDefaultSelect: {locateStrategy: 'xpath',selector: "//a[contains(.,'Daily Activity')]/../../dd[@class='active']"},
                EmployeeEnrolledSelect: {selector: "g.highcharts-legend-item:nth-child(1) text"},
                ElectionChangesSelect: {selector: "g.highcharts-legend-item:nth-child(2) text"},
                DailyActivityXAxis: {selector: "g.highcharts-series.highcharts-series-1"},
                DailyActivityYAxis: {selector: "g.highcharts-axis-labels.highcharts-yaxis-labels"},

                ViewEmployeesTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'View Employees')]"},
                NotStartedNumber: {selector: "(//*[@class='highcharts-series-group'])[last()-1]/..//*[@class='highcharts-title']"},
















            }
        },

        DocumentLibraryPageHRAP: {
            selector: 'body',
            elements: {
                //QAF

                DocumentLibraryTitle: {locateStrategy: 'xpath',selector: "//h1[contains(text(),' Document Library')]"},
                TileViewTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Tile View')]"},
                ListViewTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'List View')]"},
                SolutionOverviewTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Solution Overview')]"},
                DataManagementTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Data Management')]"},
                PayrollAndReportsTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Payroll & Reports')]"},
                SpendingAccountsTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Spending Accounts')]"},
                ProcessSupportTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Process Support')]"},
                COBRATab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'COBRA')]"},
                AscendingTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Ascending (A-Z)')]"},
                DescendingTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Descending (Z-A)')]"},

                ListViewGrid: {selector: "div[ng-hide='isTileView']"},
                TileGridView: {selector: "div[ng-show='isTileView']"},

                AddToZip1st: {locateStrategy: 'xpath',selector: "(//label[contains(text(),'Add to Zip')])[1]"},
                AddedToZip1st: {locateStrategy: 'xpath',selector: "(//label[contains(text(),'Added')])[1]"},
                DownloadSelectedButton: {locateStrategy: 'xpath',selector: "//a[contains(.,'Download Selected')]"},







            }
        },

        EducationPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                CanvasButtonHRAP: {locateStrategy: 'xpath',selector: "//div[@id='6EuBeLDeMNm.6qdK8x09Vmg.6AhjQye8YU9']/canvas"},
                LearnMoreButton: {locateStrategy: 'xpath',selector: "//div[@id='5Xi2nfHUYZy.6Yp7JZBa9LE.6H3DdmjBPZS']/canvas"},
                VideoDashboardTab: {locateStrategy: 'xpath',selector: "//div[@id='5u105TYJXcm.5ix115EISrk.6kvzrg5l5we.6JFGXrba3OK.6YVXWNF7KkG']/canvas"},
            }
        },

        EmployeePageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                EmployeeTitleHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Employees')]"},
                FirstLName: {selector: "table[role='grid'] tbody tr:nth-child(1) td:nth-child(1) a"},
                ProxyAccessLinkFirst: {locateStrategy: 'xpath',selector: "//div[contains(.,'View Employee Website')]/i/.."},
                SubmitButtonProxyPage: {selector: "input[value='Submit']"},
                EndImpersonationButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'End Impersonation')]"},
              }
        },

        ReportingPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                ReportingTitleHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Reporting')]"},
                SelectReportTypeDropdown: {selector: "select[ng-model='reportOption']"},
                AlllifeEventDropdown: {selector: "select[ng-model='reportOption'] option[label='All Life Events']"},
                CreateReportButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Create')]"},
                IncludeEventDropdown: {selector: "select[ng-model='result.value']"},
                AllEventOptionIEDropdown: {selector: "select[ng-model='result.value'] option[label='All Events']"},
                EffectiveDateStartValue: {selector: "input[ng-model='startdate.value']"},
                EffectiveDateEndValue: {selector: "input[ng-model='enddate.value']"},
                GenerateReportButton: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Generate Report')]"},
                ReportSuccessMsg: {locateStrategy: 'xpath',selector: "//div[contains(text(),'Your report was successfully created.')]"},
                ReportDownloadButton: {locateStrategy: 'xpath',selector: "//a[contains(.,'Download')]"},
                CloseButtonReportOption: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Close')]"},
                LatestCreatedReportValue: {locateStrategy: 'xpath',selector: "//tbody/tr[1]/td[4]//a[contains(text(),'All Life Events')]"},
                ErrorMsgReportDownload: {locateStrategy: 'xpath',selector: "//pre[contains(text(),'Requested action is forbidden for current user')]"},



            }
        },

        TestingPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                CloseButtonTestingTab: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Close')]"},
                FirstClientTesting: {selector: "table[role='grid'] tbody tr:nth-child(1) td:nth-child(1) a"},
                EditProfileButtonTestingTab: {selector: "a[ui-sref='hr.employeeProfile.editProfileInfo'] i"},
                EffectiveDateTextbox: {selector: "input[name='effectiveDate']"},
                SaveButtonEmpDetails: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Save')]"},
                CongratulationMsgEmpTesting: {locateStrategy: 'xpath',selector: "//span[contains(text(),'data has been successfully updated.')]"},
                ChangesReqCongMsg: {locateStrategy: 'xpath',selector: "//div[contains(text(),'Changes requested have been completed according to effective date rules.')]"},







            }
        },

        AddEmployeePage: {
            selector: 'body',
            elements: {
                //QAF
                EmployeeTitleHeader: {locateStrategy: 'xpath',selector: "//h1[contains(text(),'Employees')]"},
                AddNewEmployeeButton: {locateStrategy: 'xpath',selector: "//a[contains(.,' Add New Employee')]"},
                DemographicsInfoTitle: {locateStrategy: 'xpath',selector:"//h4[contains(text(),'Demographic Information')]"},

                FirstNameTextbox: {selector:"input[name='firstName']"},
                MiddleNameTextbox: {selector: "input[name='initials']"},
                LastNameTextbox: {selector: "input[name='lastName']"},
                AddressLine1Textbox: {selector: "input[name='addressLine1']"},
                AddressLine2Textbox: {selector: "input[name='addressLine2']"},
                CityTextbox: {selector: "input[name='hrCity']"},
                ZipTextbox: {selector: "input[name='hrZip']"},
                StateDropdown: {selector: "select[name='stateOption']"},
                AlbamaOptionStateDropdown: {selector: "select[name='stateOption'] option[label='Alabama']"},
                GenderDropdown: {selector: "select[name='genderOption']"},
                MaleOptionGender: {selector: "select[name='genderOption'] option[label='Male']"},
                DOBTextbox: {selector: "input[name='dateOfBirth']"},
                EmployerAssignIDTextbox: {locateStrategy: 'xpath',selector: "//label[contains(text(),'Employer Assigned ID')]/input"},
                SSNTextbox: {locateStrategy: 'xpath',selector: "(//label[contains(text(),'Social Security Number')]/input)[1]"},
                PersonalPhoneTextbox: {selector: "input[name='phone']"},
                HomeEmailTextbox: {selector: "input[name='homeEmail']"},
                CompanyMailTextbox: {selector: "input[name='companyEmail']"},
                OriginalHireDateTextbox: {selector: "input[name='dateHire']"},
                JobTitleTextbox: {locateStrategy: 'xpath',selector: "//label[contains(text(),'Job Title')]/input"},
                WorkStateDropdown: {selector: "select[name='workStateOption']"},
                AlaskaWorkStateDropdown: {selector: "select[name='workStateOption'] option[label='Alaska']"},
                StatusDropdown: {selector: "select[name='statusOption']"},
                FullTimeStatusDropdown: {selector: "select[name='statusOption'] option[label='Full-Time']"},
                DivisionDropdown: {selector: "select[name='divisionOption']"},
                Option1DivisionDropdown: {selector: "select[name='divisionOption'] option[label='1']"},
                ClientDefinedReporting1Textbox: {locateStrategy: 'xpath',selector: "//label[contains(.,'Carrier Defined Reporting 1')]/input"},
                ClientDefinedReporting2Textbox: {locateStrategy: 'xpath',selector: "//label[contains(.,'Carrier Defined Reporting 2')]/input"},
                ClientDefinedReporting3Textbox: {locateStrategy: 'xpath',selector: "//label[contains(.,'Carrier Defined Reporting 3')]/input"},
                ClientDefinedReporting4Textbox: {locateStrategy: 'xpath',selector: "//label[contains(.,'Carrier Defined Reporting 4')]/input"},
                ClientDefinedReporting5Textbox: {locateStrategy: 'xpath',selector: "//label[contains(.,'Carrier Defined Reporting 5')]/input"},
                MostRecentHireDateTextbox: {selector: "input[name='dateRehire']"},
                LocationDropdown: {selector: "select[name='locationOption']"},
                Option1LocationDropdown: {selector: "select[name='locationOption'] option[label='1']"},
                EmployementStatusDropdown: {selector: "select[name='eeStatusOption']"},
                ActiveOptionEmpStatusDropdown: {selector: "select[name='eeStatusOption'] option[label='Active ']"},
                ExecuteStatusDropdown: {selector: "select[name='executiveStatusOption']"},
                ExecutiveOptionExecuteStatusDropdown: {selector: "select[name='executiveStatusOption'] option[label='Executive']"},
                CurrentSalaryTextbox: {selector: "input[name='currentSalary']"},
                BenefitSalaryTextbox: {selector: "input[name='benefitSalary']"},
                PayrollFreqDropdown: {selector: "select[name='payrollFrequencyOption']"},
                MonthlyOptionPayrollFreqDropdown: {selector: "select[name='payrollFrequencyOption'] option[label='Monthly']"},
                PaytypeDropdown: {selector: "select[name='salaryOption']"},
                HourlyOptionPaytypeDropdown: {selector: "select[name='salaryOption'] option[label='Hourly']"},
                SaveButton: {locateStrategy: 'xpath',selector: "//button[contains(text(),'Save')]"},












            }
        },

        BenefitsPageHRAP: {
            selector: 'body',
            elements: {
                //QAF
                BenefitsHeader: {locateStrategy: 'xpath',selector: '//h1[contains(text(),"Benefits")]'},
                SummaryTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Summary')]"},
                DetailsTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Detail')]"},
                DownloadReqTab: {locateStrategy: 'xpath',selector: "//a[contains(.,'Download Requirements')]"},

                //summary View
                MedicalTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Medical')]"},
                MedicalTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-medical'])[1]"},
                CarrierMedicalTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Medical')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                CarrierMedicalPlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Medical')]/../../div[3]/ul/li/span[2]"},

                SupplementalTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Supplemental')]"},
                SupplementalTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-supplemental'])[1]"},
                CarrierSupplementalTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Supplemental')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                CarrierSupplementalPlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Supplemental')]/../../div[3]/ul/li/span[2]"},

                DentalTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Dental')]"},
                DentalTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-dental'])[1]"},
                CarrierDentalTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Dental')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                DentalSupplementalPlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Dental')]/../../div[3]/ul/li/span[2]"},

                VisionTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Vision')]"},
                VisionTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-vision'])[1]"},
                CarrierVisionTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Vision')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                VisionSupplementalPlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Vision')]/../../div[3]/ul/li/span[2]"},

                AccountsTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Accounts')]"},
                AccountsTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-accounts'])[1]"},
                CarrierAccountsTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Accounts')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                AccountsSupplementalPlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Accounts')]/../../div[3]/ul/li/span[2]"},

                LifeTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Life')]"},
                LifeTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-life'])[1]"},
                CarrierLifeTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Life')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                AccountsLifePlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Life')]/../../div[3]/ul/li/span[2]"},

                DisabilityTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Disability')]"},
                DisabilityTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-disability'])[1]"},
                DisabilityLifeTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Disability')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                AccountsDisabilityPlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Disability')]/../../div[3]/ul/li/span[2]"},


                ProtectionTitle: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Protection')]"},
                ProtectionTileImage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-protection'])[1]"},
                ProtectionLifeTile: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Protection')]/../../div[3]/ul/li/span[contains(text(),'Carrier:')]"},
                AccountsProtectionPlanName: {locateStrategy: 'xpath',selector: "//h3[contains(text(),'Protection')]/../../div[3]/ul/li/span[2]"},

                //Details View
                MedicalTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Medical')]"},
                MedicalTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-medical'])[2]"},
                MedicalCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Medical')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                MedicalCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Medical')]/../../../../div[2]/div/div/ul/li/span[2]"},

                SupplementalTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Supplemental')]"},
                SupplementalTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-supplemental'])[2]"},
                SupplementalCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Supplemental')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                SupplementalCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Supplemental')]/../../../../div[2]/div/div/ul/li/span[2]"},

                DentalTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Dental')]"},
                DentalTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-dental'])[2]"},
                DentalCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Dental')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                DentalCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Dental')]/../../../../div[2]/div/div/ul/li/span[2]"},

                VisionTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Vision')]"},
                VisionTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-vision'])[2]"},
                VisionCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Vision')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                VisionCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Vision')]/../../../../div[2]/div/div/ul/li/span[2]"},

                AccountsTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Accounts')]"},
                AccountsTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-accounts'])[2]"},
                AccountsCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Accounts')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                AccountsCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Accounts')]/../../../../div[2]/div/div/ul/li/span[2]"},

                LifeTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Life')]"},
                LifeTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-life'])[2]"},
                LifeCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Life')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                LifeCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Life')]/../../../../div[2]/div/div/ul/li/span[2]"},

                DisabilityTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Disability')]"},
                DisabilityTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-disability'])[2]"},
                DisabilityCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Disability')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                DisabilityCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Disability')]/../../../../div[2]/div/div/ul/li/span[2]"},

                ProtectionTileTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Protection')]"},
                ProtectionTileImageDetailsPage: {locateStrategy: 'xpath',selector: "(//span[@class='evo-icon-site-benefit-protection'])[2]"},
                ProtectionCarrierTitleDetailsPage: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Protection')]/../../../../div[2]/div/div/ul/li/span[contains(text(),'Carrier:')]"},
                ProtectionCarrierPlanTitle: {locateStrategy: 'xpath',selector: "//h4[contains(.,'Protection')]/../../../../div[2]/div/div/ul/li/span[2]"},









            }
        },
    }
};

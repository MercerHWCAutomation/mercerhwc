var seleniumServer = require('selenium-server');
var chromedriver=require('chromedriver');
var ieDriver = require('iedriver');
var firefoxDriver = require('geckodriver');
var data = require('./test_data/ConncectureDrx_TestData.js');
sauce_labs_username = "suresh-byregowda";
sauce_labs_access_key = "3ef3f035-78b6-4bd8-b676-364de0ef45fd";

require('nightwatch-cucumber')({
   // supportFiles: ['../utils/TestExecListener.js'],
    stepTimeout: 120000,
    defaultTimeoutInterval: 10000,
    nightwatchClientAsParameter: true
});

// module.exports = {
//     output_folder: 'reports',
//     custom_commands_path: '',
//     custom_assertions_path: '',
//     page_objects_path: "objects",
//     live_output: false,
//     disable_colors: false,
//     test_workers: {
//         enabled: false,
//         workers: 3
//     },
//     selenium: {
//         start_process: true,
//         server_path: seleniumServer.path,
//         host: '127.0.0.1',
//         port: 5555,
//         cli_args: {
//             "webdriver.chrome.driver": chromedriver.path,
//             'webdriver.ie.driver': 'C:/Mercer/mercer_health_afweb/node_modules/iedriver/lib/iedriver64/IEDriverServer.exe',
//             'webdriver.firefox.driver': firefoxDriver.path
//         }
//     },
//
//     test_settings: {
//         default: {
//             launch_url: "http://localhost",
//             page_objects_path: "objects",
//             selenium_host: "127.0.0.1",
//             selenium_port: 5555,
//             silent: true,
//             disable_colors: false,
//             screenshots: {
//                 enabled: true,
//                 on_failure: true,
//                 on_error: true,
//                 path: 'screenshots'
//             },
//             desiredCapabilities: {
//                 browserName: data.NameofThebrowser,
//                 // browserName: "internet explorer",
//                 javascriptEnabled: true,
//                 acceptSslCerts: true,
//             },
//             iphone: {
//                 desiredCapabilities: {
//                     "browserName": "Safari",
//                     appiumVersion: '1.6.3',
//                     platformName: "iOS",
//                     platformVersion: "9.2",
//                     deviceName: "iPhone 6s simulator"
//                 }
//             }
//         },
//         SauceLabs: {
//             launch_url: 'http://ondemand.saucelabs.com:80',
//             selenium_port: 80,
//             selenium_host: 'ondemand.saucelabs.com',
//             silent: true,
//             username: sauce_labs_username,
//             access_key: sauce_labs_access_key,
//             screenshots: {
//                 enabled: false,
//                 path: '',
//             },
//             globals: {
//                 waitForConditionTimeout: 10000,
//             },
//             desiredCapabilities: {
//                 name: 'ConnectureDrx Test',
//                 browserName: 'chrome',
//                 platform: 'Windows 7',
//                 version: '51',
//                 parentTunnel: 'erik-horrell',
//                 tunnelIdentifier: 'MercerTunnel2',
//             }
//         },
//         chrome: {
//             desiredCapabilities: {
//                 browserName: "chrome",
//                 javascriptEnabled: true,
//                 acceptSslCerts: true
//             }
//         },
//         firefox: {
//             desiredCapabilities: {
//                 browserName: "firefox",
//                 javascriptEnabled: true,
//                 acceptSslCerts: true
//             }
//         },
//         ie: {
//             desiredCapabilities: {
//                 browserName: "internet explorer",
//                 javascriptEnabled: true,
//                 acceptSslCerts: true
//             }
//         },
//         // Appium Config details
//         iphone: {
//             launch_url: "http://" + sauce_labs_username + ":" + sauce_labs_access_key +
//             "@ondemand.saucelabs.com:80/wd/hub",
//             selenium_port: 4723,
//             selenium_host: "localhost",
//             silent: true,
//             screenshots: {
//                 enabled: false,
//                 path: ""
//             },
//             desiredCapabilities: {
//                 browserName: "iphone",
//                 platformName: "iOS",
//                 deviceName: "iPhone Simulator",
//                 version: "7.1",
//                 app: "PATH TO YOUR IPHONE EMULATOR APP",
//                 javascriptEnabled: true,
//                 acceptSslCerts: true
//             }
//         },
//         ipad: {
//             launch_url: "http://127.0.0.1:4723/wd/hub",
//             selenium_port: 4723,
//             selenium_host: "localhost",
//             silent: true,
//             screenshots: {
//                 enabled: false,
//                 path: ""
//             },
//             desiredCapabilities: {
//                 browserName: "ipad",
//                 platformName: "iOS",
//                 deviceName: "iPad Simulator",
//                 version: "7.1",
//                 app: "PATH TO YOUR IPAD EMULATOR APP",
//                 javascriptEnabled: true,
//                 acceptSslCerts: true
//             }
//         },
//         android: {
//             launch_url: "http://localhost:5554/wd/hub",
//             selenium_port: 5554,
//             selenium_host: "localhost",
//             silent: true,
//             screenshots: {
//                 enabled: false,
//                 path: ""
//             },
//             desiredCapabilities: {
//                 browserName: "android",
//                 platformName: "ANDROID",
//                 deviceName: "",
//                 version: "",
//                 javascriptEnabled: true,
//                 acceptSslCerts: true
//             }
//
//             /*
//              default: {
//              launch_url: "http://ondemand.saucelabs.com:80",
//              selenium_port: 80,
//              selenium_host: "ondemand.saucelabs.com",
//              silent: true,
//              username: "kingthev",
//              access_key: "ee3bb843-1aee-40f8-8858-c25c377db05a",
//              screenshots: {
//              enabled: false,
//              path: "",
//              },
//              globals: {
//              waitForConditionTimeout: 10000,
//              },
//              desiredCapabilities: {
//              browserName: "iphone",
//              }
//              },
//
//              iphone: {
//              desiredCapabilities: {
//              browserName:  'Safari',
//              appiumVersion: '1.6.3',
//              deviceName: 'iPhone 6s Simulator',
//              deviceOrientation: 'portrait',
//              platformVersion: '10.0',
//              platformName: 'iOS'
//              }
//              },
//
//              android: {
//              desiredCapabilities: {
//              browserName: 'Browser',
//              appiumVersion: '1.5.3',
//              deviceName: 'Samsung Galaxy S4 Emulator',
//              deviceOrientation: 'portrait',
//              platformVersion: '4.4',
//              platformName: 'Android'
//              }
//              },
//
//              chrome: {
//              desiredCapabilities: {
//              browserName: "chrome",
//              platform: "OS X 10.11",
//              version: "47",
//              },
//              },
//
//              ie11: {
//              desiredCapabilities: {
//              browserName: "internet explorer",
//              platform: "Windows 10",
//              version: "11.0",
//              },
//              },*/
//
//         }
//
//     }
// }

module.exports = {
    output_folder: 'reports',
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: "objects",
	globals_path: "../GlobalTestData.js",
    live_output: false,
    disable_colors: false,
    test_workers: {
        enabled: false,
        workers: 3
    },
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: 5555,
        platform: 'Windows 10',
        cli_args: {
            "webdriver.chrome.driver" :chromedriver.path,
            'webdriver.ie.driver': '../IEDriverServer.exe',
            'webdriver.firefox.driver': firefoxDriver.path
        }
    },
    test_settings: {
        default: {
            launch_url: 'http://localhost',
            page_objects_path: 'objects',
            selenium_host: '127.0.0.1',
            selenium_port: 5555,
            silent: true,
            disable_colors: false,
            screenshots: {
                enabled: true,
                on_failure: true,
                on_error: true,
                path: 'screenshots'
            },
	        globals:{
		        appName: "Connecture_DRX"
	        },
            desiredCapabilities: {
                browserName: data.NameofThebrowser,
                // browserName: "internet explorer",
                javascriptEnabled: true,
                acceptSslCerts: true
            },
        },

        SauceLabs: {
            launch_url: 'http://ondemand.saucelabs.com:80',
            selenium_port: 80,
            selenium_host: 'ondemand.saucelabs.com',
            silent: true,
            username: sauce_labs_username,
            access_key: sauce_labs_access_key,
            screenshots: {
                enabled: false,
                path: '',
            },
            globals: {
                waitForConditionTimeout: 10000,
            },
            desiredCapabilities: {
                name:'ConnectureDrx Test',
                browserName: 'microsoftedge',
                platform: 'Windows 10',
                version: '14',
                parentTunnel:'erik-horrell',
                tunnelIdentifier:'MercerTunnel2',
            }
        },
        microsoftedge: {
            desiredCapabilities: {
                browserName: 'microsoftedge'
            }
        },
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts : true
            }
        },
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ie: {
            desiredCapabilities: {
                browserName: 'internet explorer',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        // Appium Config details
        iphone : {
            launch_url : "http://" + sauce_labs_username + ":" + sauce_labs_access_key +
            "@ondemand.saucelabs.com:80/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "iphone",
                platformName: "iOS",
                deviceName: "iPhone Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPHONE EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ipad : {
            launch_url : "http://127.0.0.1:4723/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "ipad",
                platformName: "iOS",
                deviceName: "iPad Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPAD EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        android : {
            launch_url : "http://localhost:5554/wd/hub",
            selenium_port  : 5554,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "android",
                platformName: "ANDROID",
                deviceName: "",
                version: "",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        }

    }

}

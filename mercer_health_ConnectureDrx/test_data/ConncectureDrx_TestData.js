module.exports = {

    TestingEnvironment: 'UAT',
    Connecturedrx_ExecStatus:'Y',
    // TestingEnvironment: 'PROD',

    NameofThebrowser: "chrome",
    // NameofThebrowser: "internet explorer",

    shortpause: 3000,
    averagepause: 9000,
    longpause: 30000,
    shortwaittime: 10000,
    averagewaittime: 40000,
    longwaittime: 80000,

    urlCallcenterPortal: 'https://mercermarketplace.staging.destinationrx.com/PlanCompare/CallCenter/type1/2017/Compare/Home',
    urlConsumerPortal: 'https://mercermarketplace.staging.destinationrx.com/PlanCompare/Consumer/Type1/2017/Compare/PinHome',

    usersUAT: {
        CALLCENTERUSER: {
            postusernamecallcenter: 'lorraine.trubia', postpasswordcallcenter: 'Welcome1!',
            usernamecallcenter: 'calltest', passwordcallcenter: 'calltest',
        },

        CONSUMERUSER: {
                postusernameconsumer: 'lorraine.trubia', postpasswordconsumer: 'Welcome1!',
                usernamecallcenter: 'calltest', passwordcallcenter: 'calltest',
        },
    },
    usersPROD: {
        CALLCENTERUSER: {
            postusernamecallcenter: 'lorraine.trubia', postpasswordcallcenter: 'Welcome1!',
            usernameconsumer: 'calltest', passwordconsumer: 'calltest',
        },

        CONSUMERUSER: {
            postusernameconsumer: 'lorraine.trubia', postpasswordconsumer: 'Welcome1!',
            usernameconsumer: 'calltest', passwordconsumer: 'calltest',
        },
    },

    FirstName : "calltest",  // For Searching Profile and Enrollments
    DrugName : "Lipitor",  // For Searching Drug Name
    AgentFirstName : "calltest",  // For Searching Agents Account
    AgentLastName : "calltest",   // For Searching Agents Account
    AgentUsername : "calltest",   // For Searching Agents Account
    LastName : "calltest",  // For Searching Profile and Enrollments
    // Call Center Test data
    Status : "Active", // For Searching Agents Account

    // Consumer Test data
    ZipCode : "74008",  // For Searching Profile
    County  : "Tulsa",  // Select County

 };

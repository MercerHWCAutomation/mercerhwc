module.exports = {
    sections: {
        LoginPage: {
            selector: 'body',
            elements: {
                inputusername: {selector: "#PostData_UserName"},
                inputpassword: {selector: "#PostData_Password"},
                AccessSitelink: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Access Site")]'
                },
                MercerMarketPlaceTitle : {locateStrategy: 'xpath',
                   selector: '//*[contains(text(),"Sign in with your Mercer Marketplace Retiree Exchange account")]'
                },
                EnterthePINTitle : {locateStrategy: 'xpath',
                   selector: '//*[contains(text(),"Enter the PIN you received from your Mercer Marketplace Benefits Counselor")]'
                },
                SearchProfileheading : {locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Search Profiles & Enrollments")]'
                },
                Getstartedheading : {locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Get Started")]'
                },
                inputagentusername: {selector: "#AgentLoginData_UserName"},
                inputagentpassword: {selector: "#AgentLoginData_Password"},
                Loginbtn: {selector: "#AgentLogin_LoginButton"},
                ContinueWithoutaPinbtn: {
                    locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Continue without a PIN")]'
                },
            }
        },
        SearchProfiles: {
            selector: 'body',
            elements: {
                inputFirstname: {selector: "#searchInputData_First_Name"},
                inputLastname: {selector: "#searchInputData_Last_Name"},
                searchbtn: {selector: "#MemberProfileSearchBtn"},
                AllProfileheading : {locateStrategy: 'xpath',
                   selector: '//span[contains(text(),"All Profiles")]'
                },
                NameorZipColumnheading: {selector: "#SortHeaderCol_1"},
                csvformat: {locateStrategy: 'xpath',
                    selector: '//select[@id="ExportOptions"]/option[contains(text(),".csv")]'
                },
                txtformat: {locateStrategy: 'xpath',
                    selector: '//select[@id="ExportOptions"]/option[contains(text(),".txt")]'
                },
                btnExport: {locateStrategy: 'xpath',
                    selector: '//a[@class="btn memberProfileExort"]'
                },
                FirstrowFirstColumnresult: {locateStrategy: 'xpath',
                   selector: '//table[@id="memberSearchResults"]/tbody/tr[2]/td//a[contains(text(),"calltest calltest")]'
                },

                EnrollHistorylink: {locateStrategy: 'xpath',
                   selector: '//table[@id="memberSearchResults"]/tbody/tr[2]/td[5]//a[contains(text(),"Enroll History")]'
                },
                ShopandComparelink: {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Shop and compare plans")]'
                },

                HealthInformationheading : {locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Health Information")]'
                },
                Continuebtn : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Continue >")]'
                },
                MedicareExtraPageTitle : {locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Medicare Extra Help")]'
                },
                AddDrugsPageTitle : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Add Drugs")]'
                },
                inputDrugname: {selector: "#DrugSearchBox_SearchtextBox"},
                Finddrugbtn: {selector: "#MainDrugSearchBtn"},

                SelectDrugbtn : {locateStrategy: 'xpath',
                    selector: '//div[@class="drugNameSelectBtn"]/a'
                },
                SelectyourDosageTitle : {locateStrategy: 'xpath',
                    selector: '//p[contains(text(),"Select your dosage and enter the amount")]'
                },
                AddDrugbtn: {selector: "#Drugs_AddDrug"},
                DrugListisCompletebtn : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Drug List is Complete >")]'
                },
                AddPharamcyPageTitle : {locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Add Pharmacy")]'
                },
                Skipbtn : {locateStrategy: 'xpath',
                    selector: '//a[@class="btn leftBlk skipBtn"]'
                },
                ViewandComparePlansPageTitle : {locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Compare Plans")]'
                },

                ViewPlans : {locateStrategy: 'xpath',
                    selector: '//a[@class="TabTextLink"]'
                },

                MedicareAdvPrescriptionDrugPlansTab : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Medicare Advantage Prescription Drug Plans")]'
                },

                PrescriptionDrugPlansTab : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Medicare Advantage Plans")]/../../../th[2]//a'
                },

                MedicareAdvantagePlansTab : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Medicare Advantage Plans")]'
                },
                MedicareSupplementPlansTab : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Medicare Supplement")]'
                },
                Closebtn : {locateStrategy: 'xpath',
                    selector: '//*[@class="ui-icon ui-icon-closethick"]'
                },
                ViewDetailslink : {locateStrategy: 'xpath',
                    selector: '(//a[contains(text(),"View Details")])[1]'
                },
                ViewPlanDetailslink : {locateStrategy: 'xpath',
                    selector: '(//a[contains(text(),"View Plan Details")])[1]'
                },

                PlanCovertheseDrugs: {locateStrategy: 'xpath',
                    selector: '//*[@id="Plan_covers_these_drugs"]/../td[1]/ul/li'
                },

                PreviousLink: {selector: "#backToPlanListHead"},

                AgentAccountMgmtlink : {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Agent Account Management")]'
                },

                AgentAccountMgmtPageTitle : {locateStrategy: 'xpath',
                    selector: '//h1[contains(text(),"Agent Account Management")]'
                },

                ViewAllAgentsbtn: {selector: "#viewAllAgentAccounts"},

                AgencyColumnheading : {locateStrategy: 'xpath',
                    selector: '//th[contains(text(),"Agency Name")]'
                },
                LastNameColumnheading : {locateStrategy: 'xpath',
                    selector: '//th[contains(text(),"Last Name")]'
                },
                FirstNameColumnheading : {locateStrategy: 'xpath',
                    selector: '//th[contains(text(),"First Name")]'
                },
                UserNameColumnheading : {locateStrategy: 'xpath',
                    selector: '//th[contains(text(),"Username")]'
                },
                StatusColumnheading : {locateStrategy: 'xpath',
                    selector: '//th[contains(text(),"Status")]'
                },

                inputAgencyName: {selector: "#AgentManagement_Organizations_Input"},
                inputAgentLastname: {selector: "#AgentManagement_LastName_Input"},
                inputAgentFirstname: {selector: "#AgentManagement_FirstName_Input"},
                inputAgentUsername: {selector: "#AgentManagement_Username_Input"},
                SearchAgentsAccountbtn: {selector: "#SearchAgentAccounts"},

                LastNamefromAgentResultsTable:  {locateStrategy: 'xpath',
                    selector: '//table[@id="agentResultsTable"]/tbody/tr[2]/td[2]'
                },

                FirstNamefromAgentResultsTable:  {locateStrategy: 'xpath',
                    selector: '//table[@id="agentResultsTable"]/tbody/tr[2]/td[3]'
                },

                UserNamefromAgentResultsTable:  {locateStrategy: 'xpath',
                    selector: '//table[@id="agentResultsTable"]/tbody/tr[2]/td[4]'
                },
                StatusfromAgentResultsTable:  {locateStrategy: 'xpath',
                    selector: '//table[@id="agentResultsTable"]/tbody/tr[2]/td[5]'
                },

                Signoutbtn:  {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Sign Out")]'
                },

                LoginPageafterSignOut:  {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Sign in with your Mercer Marketplace")]'
                },

                inputZipcode: {selector: "#Demographics_ZipCode"},
                countydialogTitle: {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Please select your county")]'
                },
                SelectCountyRadiogroup: {locateStrategy: 'xpath',
                    selector: '//div[@id="modalCounty"]/fieldset/ul/li'
                },
                firstRadiobtninCountydialog: {locateStrategy: 'xpath',
                    selector: '//div[@id="modalCounty"]/fieldset/ul/li[1]/input'
                },
                secondRadiobtninCountydialog: {locateStrategy: 'xpath',
                    selector: '//div[@id="modalCounty"]/fieldset/ul/li[2]/input'
                },
                SubmitbtninCountydialog: {locateStrategy: 'xpath',
                    selector: '//a[contains(text(),"Submit")]'
                },
                AddPrescriptionDrugsPageTitle: {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Add Prescription Drugs")]'
                },

                PrintthisPagebtn: {selector: "#printThisPage"},
            }
        },
    }
};

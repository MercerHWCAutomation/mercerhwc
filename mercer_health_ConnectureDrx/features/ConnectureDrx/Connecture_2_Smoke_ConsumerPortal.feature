@test1
Feature: Smoke Scenarios for ConsumerPortal

  @Connecture @ConsumerPortal
  Scenario: Navigate to Consumer Portal
    Given User Navigate to Consumer Portal Using "CONSUMERUSER" Successfully

  @Connecture @ConsumerPortal @SearchProfilefunctionality
  Scenario: Search Profile and Enrollments
    When User Enters Zipcode and Clicks on Continue button
    Then Verify that Please Select Your County Dailog is displayed
    When Select County and Clicks on Submit button
    Then Verify that Add Prescription drugs page loads successfully
    When Search a few drugs in the search box and Clicks on Find Drug button
    When User Select a drug dosage and quantity and click on Add Drug
    When User Clicks on Drug List is Complete button
    Then Verify that Add Pharmacy page loads successfully
    When User Clicks on Skip button
    Then Verify that View and Compare Plans Page loads successfully
    And  Verify Prescription Drug Plans and Medicare Advantage Prescription Plans are displayed
    And  Verify Medicare Advantage Plans and Medicare Supplement plans are displayed
    When User Clicks on View Plan Details button
    Then Verify that Plan Details loads successfully
    When User Clicks on Print This Page icon on the Header of the page
    Then Verify Print and Save for PlanDetails Page


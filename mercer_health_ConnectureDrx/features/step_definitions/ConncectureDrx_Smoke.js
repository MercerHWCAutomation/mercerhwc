var action = require('../../../Keywords');
var Objects = require(__dirname + '/../../objects/ConncectureDrx_locators.js');
//var robot = require('robot-js');
var robot123 = require('robotjs');
var data = require('../../test_data/ConncectureDrx_TestData.js');
var browser;

module.exports = function () {
	this.Given(/^User Navigate to CallCenter Portal Using "([^"]*)" Successfully$/, function (browser, userType) {
		
		var PageRepo=browser.page.ConncectureDrx_locators();
		var loginpage = PageRepo.section.LoginPage;
		var urlCallcenter;
		var execEnv = data["TestingEnvironment"];
		
		console.log('Test Environment: ' + execEnv);
		if (execEnv.toUpperCase() == "UAT") {
			urlCallcenter = data.urlCallcenterPortal;
			var userDB = data.usersUAT[userType]
		}
		else {
			urlCallcenter = data.urlCallcenterPortal;
			var userDB = data.usersPROD[userType]
		}
		action.initializePageObjects(browser, function () {
			// if(data.TestingEnvironment == "UAT" || data.TestingEnvironment == "PROD" ) {
			//     urlCallcenter = data.urlCallcenterPortal;
			//     var userDB = data.usersCallCenter[userType]
			// }
			// if(data.TestingEnvironment == "UAT" || data.TestingEnvironment == "PROD" ) {
			browser.maximizeWindow()
				.deleteCookies()
				.url(urlCallcenter);
			browser.pause(data.shortpause);
			browser.waitForElementPresent('body',30000);
			var n = urlCallcenter.includes("CallCenter");
			if(n){
				browser.saveScreenshot('./screenshots/AuthenticationRequiredPage.png');
				loginpage.waitForElementPresent('@inputusername',30000).setValue('@inputusername', userDB.postusernamecallcenter);
				loginpage.waitForElementPresent('@inputpassword',30000).setValue('@inputpassword', userDB.postpasswordcallcenter);
				loginpage.click('@AccessSitelink', function () {
					browser.timeoutsImplicitWait(data.longwaittime);
				});
				browser.pause(data.shortpause);
				action.isDisplayed("LoginPage|MercerMarketPlaceTitle",function () {
				});
				browser.saveScreenshot('./screenshots/CallCenterLoginPage.png')
				loginpage.setValue('@inputagentusername', userDB.usernamecallcenter);
				loginpage.setValue('@inputagentpassword', userDB.passwordcallcenter);
				loginpage.click('@Loginbtn', function () {
					browser.timeoutsImplicitWait(data.longwaittime);
				});
				browser.pause(data.averagepause);
				action.isDisplayed("LoginPage|SearchProfileheading",function () {
				});
			}
			// }
		});
	});
	
	this.Given(/^User Navigate to Consumer Portal Using "([^"]*)" Successfully$/, function (browser, userType) {
		var urlConsumer;
		var execEnv = data["TestingEnvironment"];
		console.log('Test Environment: ' + execEnv);
		if (execEnv.toUpperCase() == "UAT") {
			urlConsumer = data.urlConsumerPortal;
			var userDB = data.usersUAT[userType]
		}
		else {
			urlConsumer = data.urlConsumerPortal;
			var userDB = data.usersPROD[userType]
		}
		var PageRepo=browser.page.ConncectureDrx_locators();
		var loginpage = PageRepo.section.LoginPage;
		action.initializePageObjects(browser, function () {
			// if(data.TestingEnvironment == "UAT" || data.TestingEnvironment == "PROD" ) {
			//     urlConsumer = data.urlConsumerPortal;
			//     var userDB = data.usersConsumer[userType]
			// }
			// if(data.TestingEnvironment == "UAT" || data.TestingEnvironment == "PROD" ) {
			browser.maximizeWindow()
				.deleteCookies()
				.url(urlConsumer);
			browser.waitForElementPresent('body', data.longpause);
			browser.pause(data.shortpause);
			var n = urlConsumer.includes("Consumer")
			if (n) {
				loginpage.setValue('@inputusername', userDB.postusernameconsumer);
				loginpage.setValue('@inputpassword', userDB.postpasswordconsumer);
				loginpage.click('@AccessSitelink', function () {
					browser.timeoutsImplicitWait(data.longwaittime);
				});
				browser.pause(data.shortpause);
				action.isDisplayed("LoginPage|EnterthePINTitle", function () {
				});
				browser.saveScreenshot('./screenshots/ConsumerLoginPage.png')
				loginpage.click('@ContinueWithoutaPinbtn', function () {
					browser.timeoutsImplicitWait(data.longwaittime);
				});
				browser.pause(data.averagepause);
				action.isDisplayed("LoginPage|Getstartedheading", function () {
				});
			}
			// }
		});
	});
    
    
    this.Then(/^Verify that Home Page loads successfully$/, function (browser) {
     action.initializePageObjects(browser, function () {
     
     action.waitTillElementLoads("HomePage|ParticipantsTab",function () {
     
     browser.saveScreenshot('./screenshots/AfwebHomePage.png')
     browser.assert.urlContains("home")
     });
     });
    });
    
       this.Then(/^Verify AdministrationFoundation title in home page$/, function (browser) {
	       action.initializePageObjects(browser, function () {
     this.demoTest = function (browser) {
     browser.getTitle(function(title) {
     this.assert.equal(typeof title, 'string');
     this.assert.equal(title, 'Administration Foundation');
     });
     };
     });});
	
	this.Then(/^Verify Page title in Client Search page$/, function (browser) {
		action.initializePageObjects(browser, function () {
			this.demoTest = function (browser) {
			browser.getTitle(function (title) {
				this.assert.equal(typeof title, 'string');
				this.assert.equal(title, 'Client Search');
			});
			};
		});
	});
    
    this.Then(/^Verify Welcome Title in home Page$/, function (browser) {
     action.initializePageObjects(browser, function () {browser.pause(data.averagepause);
     action.isDisplayed("HomePage|WelcomeTitle",function () {
     });});
     });
	
	this.Then(/^Verify Groups and Roles Section in home page$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.getText("HomePage|RolesandPermissiontext", function (txt) {
				var res = txt.split(":");
				var lastarrayvalue = res[res.length - 1];
				var n = txt.includes("You have been assigned the following roles");
				if (n) {
					// console.log("'You have been assigned the following roles' title is displayed")
				}
				var role1 = lastarrayvalue.includes(data.Role1)
				var role2 = lastarrayvalue.includes(data.Role2)
				if (role1 && role2) {
					// console.log("Roles and Permissions are displayed in home Page")
				}
			})
		});
	});
	
	this.When(/^User Clicks on Clients Tab$/, function (browser) {
		action.initializePageObjects(browser, function () {action.performClick("HomePage|ClientsTab", function () {
			browser.timeoutsImplicitWait(data.shortwaittime);
		});});
	});
	
	this.Then(/^Verify that Clients Search Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {browser.pause(data.averagepause);
		browser.saveScreenshot('./screenshots/ClientSearchPage.png')
		browser.assert.urlContains("ClientSearch")
	});});
	
	this.When(/^User Enters Client Name and clicks on search button$/, function (browser) {
		action.initializePageObjects(browser, function () {action.setText("ClientsSearchPage|inputClientName", data.ClientName);
		action.performClick("ClientsSearchPage|Searchbutton", function () {
			browser.timeoutsImplicitWait(data.averagewaittime);
		});
	});});
	
	this.Then(/^Verify Client Search Results Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.timeoutsImplicitWait(data.shortwaittime);
			browser.saveScreenshot('./screenshots/ClientSearchResultsPage.png')
			browser.assert.urlContains("ClientSearchResults")
		});
	});
	this.Then(/^Verify Page title in Client Search Results page$/, function (browser) {
		action.initializePageObjects(browser, function () {this.demoTest = function (browser) {
			browser.getTitle(function (title) {
				this.assert.equal(typeof title, 'string');
				this.assert.equal(title, 'Client Search');
			});
		};
	});});
	
	this.When(/^User Clicks on Client Name Link$/, function (browser) {
		action.initializePageObjects(browser, function () {action.performClick("ClientsResultsPage|Clientnamelink", function () {
		});});
	});
	
	this.Then(/^Verify Client Summary Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {browser.timeoutsImplicitWait(data.shortwaittime);
		browser.saveScreenshot('./screenshots/ClientSummaryPage.png')
		browser.assert.urlContains("ClientSummary")
	});});
	
	this.Then(/^Verify Page title in Client Summary page$/, function (browser) {
		action.initializePageObjects(browser, function () {this.demoTest = function (browser) {
			browser.getTitle(function (title) {
				this.assert.equal(typeof title, 'string');
				this.assert.equal(title, 'Client Summary');
			});
		};
	});});
	this.Then(/^Verify Profile Summary Section Name in ClientSummary Page$/, function (browser) {
		browser.pause(data.shortpause);
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|ProfileSummaryTitle", function () {
		});});
	});
	this.Then(/^Verify Client Name Title in Profile Summary Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|ClientNameTitleInSummaryPage", function () {
		});});
	});
	this.Then(/^Verify Client Name Value in Profile Summary Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|ClientNameValueInSummaryPage", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Client Value is empty")
			}
		});
	});});
	this.Then(/^Verify One Code Title in Profile Summary Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|OneCodeTitleInSummaryPage", function () {
		});});
	});
	this.Then(/^Verify One Code Value in Profile Summary Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|OneCodeValueInSummaryPage", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("One Code Value is empty")
			}
		});});
	});
	this.Then(/^Verify Address Title in Profile Summary Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|AddressTitleInSummaryPage", function () {
		});
	});});
	this.Then(/^Verify Address Value in Profile Summary Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|AddressValueInSummaryPage", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Address Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify LOB Section Name in ClientSummary Page$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|LOBTitle", function () {
		});});
	});
	this.Then(/^Verify LoB Title in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|LOBColumnName", function () {
		});});
	});
	this.Then(/^Verify ClientName Title in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|ClientorControlIdColumnName", function () {
		});});
	});
	this.Then(/^Verify Application Title in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|ApplicationColumnName", function () {
		});});
	});
	this.Then(/^Verify Application Enabled Title in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|ApplicationEnabledColumnName", function () {
		});});
	});
	
	this.Then(/^Verify LOB Value in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {
	    action.getText("ClientSummaryPage|LOBSecondrowFirstColumnValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("LoB Value is empty")
			}
		});});
	});
	this.Then(/^Verify ClientIDorName Value in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|ClientIdSecondrowSecondColumnValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("ClientIDorApplication Value is empty")
			}
		});});
	});
	this.Then(/^Verify ApplicationName Value in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|ApplicationSecondrowThirdColumnValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Application Value is empty")
			}
		});});
	});
	this.Then(/^Verify ApplicationEnabled Value in LOB Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|ApplicationSecondrowFourthColumnValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Application Enabled Status Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify Feautres Section Name in ClientSummary Page$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|FeaturesTitle", function () {
		});});
	});
	this.Then(/^Verify Links in Feautres Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|AuthenticationLink", function () {
		});
		action.isDisplayed("ClientSummaryPage|SecondLink", function () {
		});});
	});
	
	this.When(/^User Clicks on ViewDetails link in Features Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.performClick("ClientSummaryPage|ViewDetailslinkInSummaryPage", function () {
			browser.timeoutsImplicitWait(data.shortwaittime);
		});});
	});
	this.Then(/^Verify Client Servicing Options Summary Page Loads Successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {browser.saveScreenshot('./screenshots/ClientServicingOptionsSummaryPage.png')
		browser.assert.urlContains("ClientServicingOptionsSummary")
	});});
	this.Then(/^Verify Authentication Rules Title ClientSummary Page$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|ParticipantAuthenticTitle", function () {
		});});
	});
	
	this.Then(/^Verify Webbsite Title in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|WebsiteTitle", function () {
		});});
	});
	this.Then(/^Verify WebSite Value in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|WebsiteValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Website Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify IVRValidation Title in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|IVRValidationTitle", function () {
		});});
	});
	this.Then(/^Verify IVRValidation Value in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|IVRValidationValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("IVR Validation Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify WebUsername Title in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|WebUsernameFormatTitle", function () {
		});});
	});
	this.Then(/^Verify WebUsername Value in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|WebUsernameFormatValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("WebUsername Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify WebPassword Title in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ClientSummaryPage|WebPasswordFormatTitle", function () {
		});});
	});
	this.Then(/^Verify WebPassword Value in Authentication Rules Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ClientSummaryPage|WebPasswordFormatValue", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("WebPassword Value is empty")
			}
		});});
	});
	
	this.When(/^User Clicks on Participants Tab$/, function (browser) {
		action.initializePageObjects(browser, function () {action.performClick("HomePage|ParticipantsTab", function () {
			browser.timeoutsImplicitWait(data.shortwaittime);
		});});
	});
	this.Then(/^Verify Participant Search Page title$/, function (browser) {
		action.initializePageObjects(browser, function () {
			this.demoTest = function (browser) {
				browser.getTitle(function (title) {
					this.assert.equal(typeof title, 'string');
					this.assert.equal(title, 'View Participant Search Page');
				});
			};
		});
	});
	this.Then(/^Verify that Participant Search Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {browser.pause(data.averagepause);
		action.isDisplayed("ParticipantSearchPage|Titleparticipantsearch", function () {
		});
		browser.saveScreenshot('./screenshots/ParticipantSearchPage.png')});
	});
	
	this.When(/^User Enters SSN in Social ID field and clicks on search button$/, function (browser) {
		action.initializePageObjects(browser, function () {action.setText("ParticipantSearchPage|inputsocialid", data.SSN);
		action.performClick("ParticipantSearchPage|btnSearchbySocialId", function () {
			browser.timeoutsImplicitWait(data.averagewaittime);
		});});
	});
	
	this.When(/^User Enters Alternate Username and clicks on search button$/, function (browser) {
		action.initializePageObjects(browser, function () {action.setText("ParticipantSearchPage|inputalternativeusername", data.AlternateUsername);
		action.performClick("ParticipantSearchPage|btnSearchbyAlternativeUsername", function () {
			browser.timeoutsImplicitWait(data.averagewaittime);
		});});
	});
	
	this.Then(/^Verify that Participant Results Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {browser.pause(data.averagepause);
		action.isDisplayed("ParticipantResultsPage|linkFirstParticipantCode", function () {
		});
		browser.saveScreenshot('./screenshots/ParticipantResultsPage.png')
		browser.assert.urlContains("ParticipantResults")
	});});
	
	this.Then(/^Verify Search Criteria Title and Alternate Username Value in Participant Results Page$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.getText("ParticipantResultsPage|Titlesearchcriteria", function (txt) {
				var n = txt.includes("Alternate Username:" & " " & data.AlternateUsername);
				if (n) {
				}
				else {
					console.log("Search Criteria Title is not displayed")
				}
			});
		});
	});
	
	this.Then(/^Verify Search Criteria Title and SSN Value in Participant Results Page$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.getText("ParticipantResultsPage|SearchTitlebySocialID", function (txt) {
				var n = txt.includes("Social ID:" & " " & data.SSN);
				if (n) {
				}
				else {
					console.log("Search Criteria Title is not displayed")
				}
			});
		});
	});
	
	this.When(/^User Clicks on Participant Code$/, function (browser) {
		action.initializePageObjects(browser, function () {
		if (data.ClientName == "QAWORK") {
			action.performClick("ParticipantResultsPage|linkParticipantCodeforQAWORK", function () {
				browser.timeoutsImplicitWait(data.averagewaittime);
			});
		}
		else if (data.ClientName == "SLSDM") {
			action.performClick("ParticipantResultsPage|linkParticipantCodeforSLSDM", function () {
				browser.timeoutsImplicitWait(data.averagewaittime);
			});
		}
		else {
			action.performClick("ParticipantResultsPage|linkParticipantCodeforanyotherclients", function () {
				browser.timeoutsImplicitWait(data.averagewaittime);
			});
		}
	});});
	this.Then(/^Verify that Participant Summary Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|TitleParticipantProfile", function () {
		});
		browser.saveScreenshot('./screenshots/ParticipantSummaryPage.png')
		browser.assert.urlContains("ParticipantSummary")
	});});
	
	this.Then(/^Verify Participant Profile Section Name in ParticipantSummary Page$/, function (browser) {
		action.initializePageObjects(browser, function () {browser.pause(data.averagepause);
		action.isDisplayed("ParticipantSummaryPage|TitleParticipantProfile", function () {
		});});
	});
	this.Then(/^Verify Name Title in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|NameTitleinParticipantProfile", function () {
		});});
	});
	this.Then(/^Verify Name Value in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|NameValueInParticipantProfile", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Name Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify Client Title in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|ClientTitleinParticipantProfile", function () {
		});});
	});
	this.Then(/^Verify Client Value in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|ClientValueInParticipantProfile", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Client Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify SSN Title in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|SSNTitleinParticipantProfile", function () {
		});});
	});
	this.Then(/^Verify SSN Value in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|SSNValueInParticipantProfile", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("SSN Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify DOB Title in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|DOBTitleinParticipantProfile", function () {
		});});
	});
	this.Then(/^Verify DOB Value in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|DOBValueInParticipantProfile", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("DOB Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify Address Title in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|AddressTitleinParticipantProfile", function () {
		});});
	});
	this.Then(/^Verify Address Value in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|AddressValueInParticipantProfile", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Address Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify Participant Web Access Section Name in ParticipantSummary Page$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|TitleParticipantWebAccess", function () {
		});});
	});
	
	this.Then(/^Verify Administration Section Name in ParticipantSummary Page$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|TitleAdministrationHistory", function () {
		});});
	});
	
	
	this.Then(/^Verify Lob OR Application Title in Participant Web Access Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|LoBTitleinParticipantWebAccess", function () {
		});});
	});
	this.Then(/^Verify Access Allowed Title in Participant Web Access Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|AccessAllowedTitleinParticipantWebAccess", function () {
		});});
	});
	
	this.Then(/^Verify DefinedBenefits Title in Participant Web Access Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|DefinedBenefitsTitleinParticipantWebAccess", function () {
		});});
	});
	this.Then(/^Verify DefinedBenefits Value in Participant Web Access Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|DefinedBenefitsValueinParticipantWebAccess", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("DefinedBenefits Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify HealthandBenefits Title in Participant Web Access Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|HealthandBenefitsTitleinParticipantWebAccess", function () {
		});});
	});
	this.Then(/^Verify HealthandBenefits Value in Participant Web Access Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|HealthandBenefitsValueinParticipantWebAccess", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Health and Benefits Value is empty")
			}
		});});
	});
	
	this.Then(/^Verify SingleSignOnValue Title in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|SingleSignOnTitleinParticipantWebAccess", function () {
		});});
	});
	this.Then(/^Verify SingleSignOnValue Value in ParticipantProfile Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.getText("ParticipantSummaryPage|SingleSignOnValueinParticipantWebAccess", function (txt) {
			if (txt !== "") {
			}
			else {
				console.log("Single Sign On Value is empty")
			}
		});});
	});
	
	
	this.Then(/^Verify Web Login Management Section Name in ParticipantSummary Page$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|TitleWebLoginManagement", function () {
		});});
	});
	this.Then(/^Verify Last Successfull login Title in WebLoginManagement Section$/, function (browser) {
		action.initializePageObjects(browser, function () {action.isDisplayed("ParticipantSummaryPage|LastsuccessfulloginTitleinLoginManagement", function () {
		});});
	});
	this.Then(/^Verify Last Successfull login Value in WebLoginManagement Section$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.getText("ParticipantSummaryPage|LastsuccessfulloginValueinLoginManagement", function (txt) {
				if (txt !== "") {
				}
				else {
					console.log("Last Successfull login Value is empty")
				}
			});
		});
	});
	
	this.When(/^User Clicks on Web Login Management link$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("WebLoginManagementPage|WebLoginManagementLink", function () {
				browser.timeoutsImplicitWait(data.shortwaittime);
			});
		});
	});
	
	this.Then(/^Verify WebLoginManagement Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.shortwaittime);
			browser.assert.urlContains("ParticipantWebLoginManagementLanding");
			browser.saveScreenshot('./screenshots/ParticipantWebLoginManagementLandingPage.png')
		});
	});
	
	this.Then(/^Verify Authentication Credentials Header in WebLoginManagementLanding Page$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.isDisplayed("WebLoginManagementPage|AuthenticationTitleinWebLoginManagementPage", function () {
			});
		});
	});
	
	this.Then(/^Verify Administrative Actions Header in WebLoginManagementLanding Page$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.isDisplayed("WebLoginManagementPage|AdministrationActionsTitleinWebLoginManagementPage", function () {
			});
		});
	});
	
	this.When(/^User Clicks on Administration Tab$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("HomePage|AdministrationTab", function () {
				browser.timeoutsImplicitWait(data.shortwaittime);
			});
		});
	});
	
	this.Then(/^Verify Application Landing Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.averagepause);
			action.isDisplayed("ApplicationLandingPage|ApplicationsTitleinAdministrationLandingPage", function () {
			});
			browser.saveScreenshot('./screenshots/ApplicationLandingPage.png')
		});
	});
	
	this.When(/^User Clicks on ViewLogs link$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ApplicationLandingPage|ViewlogsLinkinAdministrationLandingPage", function () {
				browser.timeoutsImplicitWait(data.shortwaittime);
			});
		});
	});
	
	this.Then(/^Verify ViewLogs Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.averagepause);
			action.isDisplayed("ApplicationLandingPage|LogViewerheader", function () {
			});
			browser.saveScreenshot('./screenshots/LogViewerPage.png')
		});
	});
	
	
	this.When(/^User Select From Date as oldest date form the drop down$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ViewLogsPage|SelectFromDate", function () {
				browser.timeoutsImplicitWait(data.shortpause);
			});
		});
	});
	
	this.When(/^User Select To Date as Tomorrow's Date$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ViewLogsPage|SelectToDate", function () {
				browser.timeoutsImplicitWait(data.shortpause);
			});
		});
	});
	
	this.When(/^User Select Client$/, function (browser) {
		action.initializePageObjects(browser, function () {
			if (data.ClientName == "QAWORK") {
				action.performClick("ViewLogsPage|SelectClientforQAWORK", function () {
					browser.timeoutsImplicitWait(data.shortpause);
				});
			}
			else {
				action.performClick("ViewLogsPage|SelectClientforSLSDM", function () {
					browser.timeoutsImplicitWait(data.shortpause);
				});
			}
		});
	});
	
	this.When(/^User Select Audit Messages from Filters Drop down$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ViewLogsPage|SelectFiltersforAudit", function () {
				browser.timeoutsImplicitWait(data.shortpause);
			});
		});
	});
	
	this.When(/^User Clicks on Search button$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ViewLogsPage|btnSearchforViewLogs", function () {
				browser.timeoutsImplicitWait(data.shortpause);
			});
		});
	});
	
	
	this.When(/^Verify Logs displayed for Selected Client$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.getText("ViewLogsPage|LogResults", function (txt) {
				var n = txt.includes(data.ClientName);
				if (n) {
				}
				else {
					console.log("No Search Results found")
				}
				browser.saveScreenshot('./screenshots/ViewLogsSearchResults.png')
			});
		});
	});
	
	this.When(/^User Clicks on Management Administrators link$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ApplicationLandingPage|linkManagementAdministrators", function () {
				browser.timeoutsImplicitWait(data.shortwaittime);
			});
		});
	});
	
	this.Then(/^Verify Manage Administrators Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.averagepause);
			action.isDisplayed("ManageAdministrators|TtileManagementAdministrators", function () {
			});
			browser.saveScreenshot('./screenshots/ManageAdministratorsPage.png')
		});
	});
	
	this.When(/^User Enters Administrator name in Look for field and clicks on Find now button$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.setText("ManageAdministrators|InputLookforfield", data.AdministratorName);
			action.performClick("ManageAdministrators|btnFindNow", function () {
				browser.timeoutsImplicitWait(data.shortwaittime);
			});
		});
	});
	
	this.Then(/^Verify User should be able to search Administrator$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.averagepause);
			action.isDisplayed("ManageAdministrators|AdministratorSearchResultslink", function () {
			});
			browser.saveScreenshot('./screenshots/AdministratorSearchResults.png')
		});
	});
	
	this.When(/^User Selects Administrator$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ManageAdministrators|AdministratorSearchResultslink", function () {
				browser.timeoutsImplicitWait(data.averagewaittime);
			});
		});
	});
	
	this.Then(/^Verify Administration Landing Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.timeoutsImplicitWait(data.averagepause);
			action.isDisplayed("ManageAdministrators|ClientnameinAdministratorinLandingPage", function () {
			});
			browser.saveScreenshot('./screenshots/AdministrationLandingPage.png')
		});
	});
	
	this.Then(/^Verify Groups and Roles Section in AdministratorLandingPage$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.timeoutsImplicitWait(data.averagewaittime);
			action.isDisplayed("ManageAdministrators|GroupsinAdministratorinLandingPage", function () {
				action.isDisplayed("ManageAdministrators|RolesinAdministratorinLandingPage", function () {
				});
			});
		});
	});
	
	this.When(/^User Selects View Details link in Features and Services Section$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ClientSummaryPage|ViewDetailslinkInSummaryPage", function () {
				browser.timeoutsImplicitWait(data.shortwaittime);
			});
		});
	});
	
	this.Then(/^Verify Features and Services Landing Page loads Successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.averagepause);
			action.isDisplayed("ClientSummaryPage|FeaturesandServicesheader", function () {
				browser.assert.urlContains("ClientServicingOptionsSummary")
			});
			browser.saveScreenshot('./screenshots/FeaturesandServicesLandingPage.png')
		});
	});
	
	this.Then(/^Verify Assigned Features and Services Section$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.isDisplayed("ClientSummaryPage|AssignedFeaturesandServicesheader", function () {
			});
		});
	});
	
	this.Then(/^Verify Available Features and Services Section$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.isDisplayed("ClientSummaryPage|AvailableFeaturesandServicesheader", function () {
			});
		});
	});
	
	this.When(/^User Clicks on External SSO Test link$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ExternalSSOTestPage|ExternalSSOTestLink", function () {
				browser.timeoutsImplicitWait(data.shortwaittime);
			});
		});
	});
	
	this.Then(/^Verify Participant SSO To Portal Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.timeoutsImplicitWait(data.shortwaittime);
			action.isDisplayed("ExternalSSOTestPage|ExternalTestHarnessHeader", function () {
			});
			browser.saveScreenshot('./screenshots/ParticipantSSOToPortalPage.png')
		});
	});
	
	this.When(/^User Select SSO Configuration$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ExternalSSOTestPage|SelectSSOforQAWORK", function () {
				browser.pause(data.shortpause);
			});
		});
	});
	
	this.When(/^User Clicks on Test SSO button$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ExternalSSOTestPage|TestSSObtn", function () {
				browser.pause(data.averagepause);
			});
		});
	});
	
	this.Then(/^Verify User should be able to redirect to IBC Portal successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.longpause);
			browser.window_handles(function (result) {
				var handle = result.value[1];
				browser.switchWindow(handle);
				browser.saveScreenshot('./screenshots/ExternalSSOTestRedirectedpage.png')
				browser.assert.urlContains("Home")
				browser.assert.urlContains(data.RedirectedURL)
				browser.closeWindow();
				var handle1 = result.value[0];
				browser.pause(data.longpause);
				browser.switchWindow(handle1);
			});
		});
	});
	
	this.When(/^User Clicks on Impersonate button$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("ClientSummaryPage|Impersonatebtn", function () {
				browser.pause(data.averagepause);
			});
		});
	});
	
	this.Then(/^Verify User should be able to see end impersonation button on top right corner of the page$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.pause(data.longpause);
			browser.window_handles(function (result) {
				var handle = result.value[1];
				browser.switchWindow(handle);
				// action.isDisplayed("EndImpersonationPage|EndImpersonationbtn", function () {
				// });
				browser.assert.urlContains(data.RedirectedURL)
				browser.saveScreenshot('./screenshots/EndImpersonationPage.png')
				browser.closeWindow();
				var handle1 = result.value[0];
				browser.switchWindow(handle1);
			});
		});
	});
	
	
	this.When(/^User Enters Firstname, Lastname and Clicks on Search button$/, function (browser) {
		action.initializePageObjects(browser, function () {
			browser.saveScreenshot('./screenshots/SearchProfilesPage.png')
			action.setText("SearchProfiles|inputFirstname", data.FirstName);
			browser.pause(data.shortpause);
			action.setText("SearchProfiles|inputLastname", data.LastName);
			action.performClick("SearchProfiles|searchbtn", function () {
				browser.timeoutsImplicitWait(data.averagewaittime);
			});
		});
	});
	
	this.Then(/^Verify Search Results should be displayed$/, function (browser) {
		action.initializePageObjects(browser, function () {
	    action.getText("SearchProfiles|FirstrowFirstColumnresult", function (txt) {
			if (txt.includes(data.FirstName) && txt.includes(data.LastName)) {
			}
			else {
				console.log("FirstName and Last Name is not displayed in Search Profiles Results")
			}
			browser.saveScreenshot('./screenshots/SearchResults.png')
		});
	});});
	
	this.When(/^User Clicks on Enroll History in front of any Retiree$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("SearchProfiles|EnrollHistorylink", function () {
				browser.pause(data.shortpause);
			});
		});
	});
	
	this.Then(/^Export Search Results in CSV format$/, function (browser) {
		action.initializePageObjects(browser, function () {
	    action.performClick("SearchProfiles|csvformat", function () {
			action.performClick("SearchProfiles|btnExport", function () {
				browser.pause(data.shortpause);
			});});
		});
	});
	
	this.Then(/^Export Search Results in TXT format$/, function (browser) {
		action.initializePageObjects(browser, function () {
	    action.performClick("SearchProfiles|txtformat", function () {
			action.performClick("SearchProfiles|btnExport", function () {
				browser.pause(data.shortpause);
			});});
		});
	});
	
	this.Then(/^Verify that Link to Shop and Compare Plans Will be displayed$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.isDisplayed("SearchProfiles|ShopandComparelink", function () {
			});
			browser.saveScreenshot('./screenshots/ShopandComparePlanPage.png')
		});
	});
	
	this.When(/^User Clicks on Shop and Compare plans link$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("SearchProfiles|ShopandComparelink", function () {
				browser.pause(data.shortpause);
			});
		});
	});
	this.Then(/^Verify that Health Information page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.isDisplayed("SearchProfiles|HealthInformationheading", function () {
			});
			browser.saveScreenshot('./screenshots/HealthInformationPage.png')
		});
	});
	
	this.When(/^Verify that Zipcode is Pre-Populated and Clicks on Continue button$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("SearchProfiles|Continuebtn", function () {
				browser.timeoutsImplicitWait(data.averagewaittime);
			});
		});
	});
	this.Then(/^Verify that Medicare Extra Help page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
		action.isDisplayed("SearchProfiles|MedicareExtraPageTitle", function () {
		});
		browser.saveScreenshot('./screenshots/MedicareExtraHelpPage.png')
	});});
	this.When(/^User Selects Level of qualification for Extra help and Clicks on Continue button$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("SearchProfiles|Continuebtn", function () {
				browser.timeoutsImplicitWait(data.averagewaittime);
			});
		});
	});
	
	this.When(/^Search a few drugs in the search box and Clicks on Find Drug button$/, function (browser) {
		action.initializePageObjects(browser, function () {
		action.setText("SearchProfiles|inputDrugname", data.DrugName);
		browser.pause(data.shortpause);
		action.performClick("SearchProfiles|Finddrugbtn", function () {
			browser.timeoutsImplicitWait(data.averagewaittime);
			browser.saveScreenshot('./screenshots/SelectDrug.png')
		});});
	});
	this.When(/^User Select a drug dosage and quantity and click on Add Drug$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.performClick("SearchProfiles|SelectDrugbtn", function () {
				browser.pause(data.averagepause);
				action.performClick("SearchProfiles|AddDrugbtn", function () {
					browser.pause(data.averagepause);
				});
			});
		});
	});
	this.When(/^User Clicks on Drug List is Complete button$/, function (browser) {
		action.initializePageObjects(browser, function () {
		action.performClick("SearchProfiles|DrugListisCompletebtn", function () {
			// browser.pause(data.averagepause);
			browser.timeoutsImplicitWait(data.shortwaittime);
		});
	});
});
	
	this.Then(/^Verify that Add drugs page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
		action.isDisplayed("SearchProfiles|AddDrugsPageTitle", function () {
		});
		browser.saveScreenshot('./screenshots/AddDrugPage.png')
	});
});
    this.Then(/^Verify that Add Pharmacy page loads successfully$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        action.isDisplayed("SearchProfiles|AddPharamcyPageTitle",function () {
        });
        browser.saveScreenshot('./screenshots/AddPharamcyPage.png')
    });});
    this.When(/^User Clicks on Skip button$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        action.performClick("SearchProfiles|Skipbtn",function () {
            // browser.pause(data.averagepause);
            browser.timeoutsImplicitWait(data.shortwaittime);
        });});
    });

    this.Then(/^Verify that View and Compare Plans Page loads successfully$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        action.isDisplayed("SearchProfiles|ViewandComparePlansPageTitle",function () {
        });
        browser.saveScreenshot('./screenshots/ViewandComparePlansPage.png')
    });});

    // this.Then(/^Verify Medicare Advantage Prescription Drug Plans and Prescription Drug Plans$/, function (browser) {
    //     action.isDisplayed("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab",function () {
    //         action.isDisplayed("SearchProfiles|PrescriptionDrugPlansTab",function () {
    //             action.performClick("SearchProfiles|PrescriptionDrugPlansTab",function () {
    //                 browser.pause(data.shortpause);
    //             });
    //         });
    //     });
    // });
    //
    // this.Then(/^Verify Medicare Advantage Prescription Drug Plans and Prescription Drug Plans$/, function (browser) {
    //     action.isDisplayed("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab",function () {
    //         action.isDisplayed("SearchProfiles|PrescriptionDrugPlansTab",function () {
    //             action.performClick("SearchProfiles|PrescriptionDrugPlansTab",function () {
    //                 browser.pause(data.shortpause);
    //             });
    //         });
    //     });
    // });

    this.Then(/^Verify Medicare Advantage Prescription Drug Plans and Prescription Drug Plans$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        action.readListData("SearchProfiles|ViewPlans",function(values) {
	        var str = values
	        if (str.includes("Prescription Drug Plans")) {
		        action.isDisplayed("SearchProfiles|PrescriptionDrugPlansTab", function () {
			        // console.log("Prescription Drug Plans Tab displayed")
			        action.performClick("SearchProfiles|PrescriptionDrugPlansTab", function () {
				        browser.pause(data.shortpause);
				        // console.log("Prescription Drug Plans are displayed")
				        browser.saveScreenshot('./screenshots/PrescriptionDrugPlansTab.png')
			        });
		        });
	        }
	        if (str.includes("Medicare Advantage Prescription Drug Plans")) {
		        action.isDisplayed("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab", function () {
			        // console.log("Medicare Advantage Prescription Drug Plans Tab displayed")
			        // console.log("Medicare Advantage Prescription Drug Plans are displayed")
			        browser.saveScreenshot('./screenshots/MedicareAdvantagePrescriptionDrugTab.png')
			        // action.performClick("SearchProfiles|PrescriptionDrugPlansTab",function () {
			        //     browser.pause(data.shortpause);
			        //     console.log("Medicare Advantage Prescription Drug Plans are displayed")
			        // });
		        });
	        }
        });
        });

    });

    this.Then(/^Verify Medicare Advantage Plans and Medicare Supplement plans are displayed$/, function (browser) {
	    action.initializePageObjects(browser, function () {
	    action.readListData("SearchProfiles|ViewPlans", function (values) {
		    var str = values
		    if (str.includes("Medicare Advantage Plans")) {
			    action.isDisplayed("SearchProfiles|MedicareAdvantagePlansTab", function () {
				    // console.log("Medicare Advantage Plans Tab displayed")
				    action.performClick("SearchProfiles|MedicareAdvantagePlansTab", function () {
					    browser.pause(data.shortpause);
					    // console.log("Medicare Advantage Plans are displayed")
					    browser.saveScreenshot('./screenshots/MedicareAdvantagePlansTab.png')
				    });
			    });
		    }
		    if (str.includes("Medicare Supplement")) {
			    action.isDisplayed("SearchProfiles|MedicareSupplementPlansTab", function () {
				    // console.log("Medicare Supplement Plans Tab displayed")
				    action.performClick("SearchProfiles|MedicareSupplementPlansTab", function () {
					    browser.pause(data.shortpause);
					    // browser.window_handles(function(result) {
					    //     var handle = result.value[1];
					    //     browser.switchWindow(handle);
					    //     browser.saveScreenshot('./screenshots/MedicareSupplementPlansWindow.png')
					    //     action.performClick("SearchProfiles|Closebtn",function () {
					    //         browser.pause(data.shortpause);
					    //     });
					    //     var handle1 = result.value[0];
					    //     browser.switchWindow(handle1);
					    // });
					    action.performClick("SearchProfiles|Closebtn", function () {
						    browser.pause(data.shortpause);
					    });
				    });
			    });
		    }
		
	    });
    });
    });

    this.Then(/^Verify Prescription Drug Plans and Medicare Advantage Prescription Plans are displayed$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    action.readListData("SearchProfiles|ViewPlans", function (values) {
			    var str = values
			    if (str.includes("Prescription Drug Plans")) {
				    action.isDisplayed("SearchProfiles|PrescriptionDrugPlansTab", function () {
					    // console.log("Prescription Drug Plans are displayed")
					    // action.performClick("SearchProfiles|PrescriptionDrugPlansTab",function () {
					    //     browser.pause(data.shortpause);
					    //     console.log("Prescription Drug Plans are displayed")
					    //     browser.saveScreenshot('./screenshots/PrescriptionDrugPlansTab.png')
					    // });
				    });
			    }
			    if (str.includes("Medicare Advantage Prescription Drug Plans")) {
				    action.isDisplayed("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab", function () {
					    // console.log("Medicare Advantage Prescription Drug Plans Tab displayed")
					    browser.saveScreenshot('./screenshots/MedicareAdvantagePrescriptionDrugTab.png')
					    action.performClick("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab", function () {
						    browser.pause(data.shortpause);
						    // console.log("Medicare Advantage Prescription Drug Plans are displayed")
					    });
				    });
			    }
			
		    });
	    });
    });

    this.When(/^User Clicks on View Details button$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        action.readListData("SearchProfiles|ViewPlans",function(values) {
            var str = values
            if(str.includes("Medicare Advantage Prescription Drug Plans")) {
                action.isDisplayed("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab",function() {
                    // console.log("Medicare Advantage Plans Tab displayed")
                    action.performClick("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab",function () {
                        browser.pause(data.shortpause);
                        // console.log("Medicare Advantage Plans are displayed")
                        browser.saveScreenshot('./screenshots/MedicareAdvantagePlansTab.png')
                        action.performClick("SearchProfiles|ViewDetailslink",function () {
                            // browser.pause(data.averagepause);
                            browser.timeoutsImplicitWait(data.shortwaittime);
                        });
                    });
                });
            }
        });
    });});

    this.Then(/^Verify that Plan Details loads successfully$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        browser.pause(data.averagepause);
        browser.url(function (current_url) {
            var pageurl = current_url.value
            var pageurl = pageurl.toUpperCase();
            if (pageurl.includes("PLANDETAILS")) {
                console.log("Plan Details loaded successfully")
                action.readListData("SearchProfiles|PlanCovertheseDrugs",function(values) {
                    var str = values
                    console.log(str)
                    if(str.includes(data.DrugName)) {
                        console.log("Plan Covers these drug" + "  " + data.DrugName)
                    }
                });
            }
            else if(pageurl.includes("ERROR")) {
                console.log("Plan Details not loaded")
            }
            browser.saveScreenshot('./screenshots/PlanDetails.png')
            // browser.back();
            // browser.pause(data.averagepause);
            // browser.timeoutsImplicitWait(data.shortwaittime);
        })
    });});

    this.When(/^User Clicks on SignOut link$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        action.performClick("SearchProfiles|Signoutbtn",function () {
            browser.pause(data.averagepause);
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });});

    this.Then(/^Verify User should be able to logout successfully$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        browser.pause(data.averagepause);
        action.isDisplayed("LoginPage|MercerMarketPlaceTitle",function () {
        });
        browser.saveScreenshot('./screenshots/LoginPageafterSignout.png')
    });});

    this.When(/^User Clicks on Agent Account Management link$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        action.performClick("SearchProfiles|AgentAccountMgmtlink",function () {
            browser.pause(data.averagepause);
            browser.timeoutsImplicitWait(data.shortwaittime);
        });
    });});

    this.Then(/^Verify that Agent Account Management page loads successfully$/, function (browser) {
	    action.initializePageObjects(browser, function () {
        browser.pause(data.shortpause);
        action.isDisplayed("SearchProfiles|AgentAccountMgmtPageTitle",function () {
        });
        browser.saveScreenshot('./screenshots/AgentAccountManagement.png')
    });});

    this.When(/^User Enters Agent Firstname,Lastname,Username and Clicks on Search Agent accounts button$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    if (data.AgentLastName != "") {
			    action.setText("SearchProfiles|inputAgentLastname", data.AgentLastName);
			    // browser.pause(data.shortpause);
		    }
		    if (data.AgentFirstName != "") {
			    action.setText("SearchProfiles|inputAgentFirstname", data.AgentFirstName);
			    // browser.pause(data.shortpause);
		    }
		    if (data.AgentUsername != "") {
			    action.setText("SearchProfiles|inputAgentUsername", data.AgentUsername);
		    }
		    action.performClick("SearchProfiles|SearchAgentsAccountbtn", function () {
			    browser.timeoutsImplicitWait(data.averagewaittime);
		    });
	    });
    });

    this.Then(/^Verify that Agent Firstname,Lastname,Username and Status displayed in the agent search results$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    browser.saveScreenshot('./screenshots/AgentSearchResults.png')
		    action.getText("SearchProfiles|LastNamefromAgentResultsTable", function (txt) {
			    if (txt.includes(data.AgentLastName)) {
			    }
			    else {
				    console.log("Agent Last Name is not displayed in Search Agent Results")
			    }
		    });
		    action.getText("SearchProfiles|FirstNamefromAgentResultsTable", function (txt) {
			    if (txt.includes(data.AgentFirstName)) {
			    }
			    else {
				    console.log("Agent First Name is not displayed in Search Agent Results")
			    }
		    });
		    action.getText("SearchProfiles|UserNamefromAgentResultsTable", function (txt) {
			    if (txt.includes(data.AgentUsername)) {
			    }
			    else {
				    console.log("Username is not displayed in Search Agent Results")
			    }
		    });
		    action.getText("SearchProfiles|StatusfromAgentResultsTable", function (txt) {
			    if (txt.includes(data.Status)) {
			    }
			    else {
				    console.log("Status is not displayed in Search Agent Results")
			    }
		    });
	    });
    });


    this.When(/^User Enters Zipcode and Clicks on Continue button$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    action.setText("SearchProfiles|inputZipcode", data.ZipCode);
		    browser.pause(data.shortpause);
		    action.performClick("SearchProfiles|Continuebtn", function () {
			    browser.timeoutsImplicitWait(data.averagewaittime);
		    });
		    browser.pause(data.shortpause);
	    });
    });

    this.Then(/^Verify that Please Select Your County Dailog is displayed$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    action.isDisplayed("SearchProfiles|countydialogTitle", function () {
		    });
		    browser.saveScreenshot('./screenshots/CountyDailogWindow.png')
	    });
    });

    this.Then(/^Select County and Clicks on Submit button$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    action.readListData("SearchProfiles|SelectCountyRadiogroup", function (values) {
			    var str = values
			    if (str.includes(data.County)) {
				    action.performClick("SearchProfiles|firstRadiobtninCountydialog", function () {
					    browser.pause(data.shortpause);
					    action.performClick("SearchProfiles|SubmitbtninCountydialog", function () {
						    browser.pause(data.shortpause);
					    });
				    });
			    }
		    });
	    });
    });

    this.Then(/^Verify that Add Prescription drugs page loads successfully$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    action.isDisplayed("SearchProfiles|AddPrescriptionDrugsPageTitle", function () {
		    });
		    browser.saveScreenshot('./screenshots/AddPrescriptionDrugPage.png')
	    });
    });

    this.When(/^User Clicks on View Plan Details button$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    action.readListData("SearchProfiles|ViewPlans", function (values) {
			    var str = values
			    if (str.includes("Medicare Advantage Prescription Drug Plans")) {
				    action.isDisplayed("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab", function () {
					    // console.log("Medicare Advantage Prescription Drug Plans Tab displayed")
					    action.performClick("SearchProfiles|MedicareAdvPrescriptionDrugPlansTab", function () {
						    browser.pause(data.shortpause);
						    // console.log("Medicare Advantage Prescription Plans are displayed")
						    browser.saveScreenshot('./screenshots/MedicareAdvantagePrescriptionPlans.png')
						    action.performClick("SearchProfiles|ViewPlanDetailslink", function () {
							    browser.pause(data.averagepause);
							    browser.timeoutsImplicitWait(data.shortwaittime);
						    });
					    });
				    });
			    }
		    });
	    });
    });

    this.When(/^User Clicks on Print This Page icon on the Header of the page$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    action.performClick("SearchProfiles|PrintthisPagebtn", function () {
			    browser.pause(data.averagepause);
			    browser.timeoutsImplicitWait(data.shortwaittime);
		    });
	    });
    });

    this.Then(/^Verify Print and Save for PlanDetails Page$/, function (browser) {
	    action.initializePageObjects(browser, function () {
		    if (data.NameofThebrowser === "chrome") {
			    robot123.setKeyboardDelay(data.shortwaittime);
			    robot123.keyTap("tab");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("tab");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("enter");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("tab");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("tab");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("tab");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("enter");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("enter");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("enter");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.keyTap("tab");
			    robot123.setKeyboardDelay(data.shortpause);
			    robot123.setMouseDelay(data.shortpause);
			    robot123.keyTap("enter");
			    robot123.setMouseDelay(data.shortpause);
			    robot123.setKeyboardDelay(data.shortwaittime);
			
		    }
		
	    });

    });
};



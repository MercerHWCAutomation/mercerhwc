Feature: Smoke Scenario for DB Plan

  @IBC @Smoke @DBPlan
  Scenario:  Verify DB Plan link in Wealth Menu
    Given User is logged in using "IBCUSER" to IBC application
    Then Verify DB Plan link in Wealth Menu

  @IBC @Smoke @Logout
  Scenario:  Verify Logout functionality
    When  User Clicks on Logout link in header
    Then  Verify User should be able to log out successfully
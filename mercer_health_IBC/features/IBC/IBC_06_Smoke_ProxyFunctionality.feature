Feature: Smoke Scenario for Proxy functionality

  @IBC @Smoke @Proxyfunctionality
  Scenario: Proxy functionality
    Given User is logged in using "IBCUSER" to IBC application
    Then  Verify Home Page is loaded successfully for IBC
    When User Clicks on Administration link
    Then Verify that Administration Landing Page loads successfully
    When User Clicks on Employee Search link
    Then Verify that Employee Search Page loads successfully
    When User enter SSN and click on search button
    Then Verify User should be able to see heading You are currently impersonating xxxx user in the header
    When User Clicks on EndImpersonation link in header
    Then Verify User Successfully logout from Impersonation user account
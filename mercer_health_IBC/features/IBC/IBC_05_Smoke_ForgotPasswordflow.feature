#Feature: Smoke Scenario for ForgotPassword flow
#
#  @IBC @Smoke @ForgotPasswordflow
#  Scenario: Forgot Password Flow
#    Given User Navigate to IBC Login Page using "IBCUSER"
#    When  User Enter Security Answer1
#    And   User Enter Security Answer2
#    And   User Enter New Password
#    Then  Verify Confirmation Message after saving information
#    When  Clicks on Continue button
#    Then  Verify that Home Page loads successfully
#    When  User Clicks on LoginManagementlink in Home Page
#    Then  Verify that Login Management account information page loads successfully
#    When  User Clicks on linkChangePassword
#    Then  Verify that Change Your Password page loads successfully
#    When  Change the Password
#    And   Verify User should be able to update login information
#    When  User Clicks on Logout link in header
#    Then  Verify User should be able to log out successfully
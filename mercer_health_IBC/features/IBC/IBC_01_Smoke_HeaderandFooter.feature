Feature: Smoke Scenarios for Header and Footer

  @IBC @Smoke @HomePage
  Scenario: Navigate to IBC Home Page
    Given User is logged in using "IBCUSER" to IBC application
    Then  Verify Home Page is loaded successfully for IBC

  @IBC @Smoke @Containers
  Scenario: Verify Containers in Home Page
    Then Verify Containers displayed in IBC Home Page

  @IBC @Smoke @WealthTab
  Scenario:  Verify Wealth Tab in Home Page
    Then Verify Wealth Tab displayed in IBC Home Page

  @IBC @Smoke @FormsTab
  Scenario:  Verify Forms Tab in Home Page
    Then Verify Forms Tab displayed in IBC Home Page

  @IBC @Smoke @HealthTab
  Scenario:  Verify Health Tab in Home Page
    Then Verify Health Tab displayed in IBC Home Page

  @IBC @Smoke @TotalRewardsTab
  Scenario:  Verify TotalRewards Tab in Home Page
    Then Verify TotalRewards Tab displayed in IBC Home Page

  @IBC @Smoke @ResourceCenterTab
  Scenario:  Verify ResourceCenter Tab in Home Page
    Then Verify ResourceCenter Tab displayed in IBC Home Page

  @IBC @Smoke @AdministrationTab
  Scenario:  Verify Administration Tab in Home Page
    Then Verify Administration Tab displayed in IBC Home Page

 @IBC @Smoke @LoginManagementLink
  Scenario: Update Login Information
    When  User Clicks on linkLoginManagement in Home Page
    Then  Verify that Login Management account information page loads successfully
    And   User should be able to see fields "User Name,Password,Email,SecurityQuestions,Answers,Change your User Name,Change your Password,Change your E-mail Address and Change your Security Questions" fields in Account login information page
    When  User Clicks on "Change your Security Questions" in Account Login Information Page
    When  Verify that Modify SQA Page loads successfully
    When  User Change Answer for "Security Question1" field
    And   User Clicks on "Save" button
    And   Verify User should be able to update account login information

   @IBC @Smoke @SiteMapLink
    Scenario:  Verify SiteMap link in header
    When  User Clicks on SiteMapLink in Home Page
    Then  Verify that "G2SiteMap" text is displayed in the URL

   @IBC @Smoke @HelpLink
    Scenario: Verify Help Link in header
    When  User Clicks on Help Link in Header
    Then  Verify that Help Page loads successfully

  @IBC @Smoke @TermsOfUseLink
  Scenario: Verify Terms of Use link from Footer
    When  User Clicks on Terms of Use link from footer
    Then  Verify that Terms of Use Page Opened in different Window

  @IBC @Smoke @PrivacyPolicyLink
  Scenario: Verify Privacy Policy Link in Footer
    When  User Clicks on PrivacyPolicy Link from footer
    Then  Verify that Privacy Policy Page Opened in different window

  @IBC @Smoke @ContactUsLink
  Scenario: Verify Contact Us link in Home Page
    When  User Clicks on ContactUs link in Header
    Then  Verify that Contact Us Page loads successfully

module.exports = {

    // TestingEnvironment: 'QAI',
    // TestingEnvironment: 'CITEST',
   // TestingEnvironment: 'CSO',
     TestingEnvironment: 'PROD',
   //  TestingEnvironment: 'CIDEV',
    IBC_ExecStatus:'Y',
   NameofThebrowser: "chrome",
    // NameofThebrowser: "internet explorer",

    shortpause: 2000,
    averagepause: 7000,
    longpause: 30000,
    shortwaittime: 15000,
    averagewaittime: 40000,
    longwaittime: 80000,

    urlQAI: 'https://qai.ibenefitcenter.com/QAWORK/login.tpz',
    // urlQAI: 'https://qai.ibenefitcenter.com/PBSPHL/login.tpz',
    urlCITEST: 'https://ctesti.ibenefitcenter.com/SLSDM/login.tpz',
    urlCSO: 'https://csoi.ibenefitcenter.com/SLSDM/login.tpz',
    urlPROD: 'https://ibenefitcenter2.mercerhrs.com/SLSDM/login.tpz',
    urlCIDEV: 'https://cdevi.ibenefitcenter.com/login.tpz',


    usersQAI: {
        IBCUSER: {
            username: 'test4988', password: 'test0001',
            username1: 'PBSPHL3738', password1: 'PBSPHLpass1'
        },
    },
    usersCITEST: {
        IBCUSER: {
            username: 'testqa1201', password: 'test0001' },
    },
    usersCSO: {
        IBCUSER: {
            username: 'testqa1003', password: 'test0001' },
    },
    usersPROD: {
        IBCUSER: {
            username: 'testqa1001', password: 'test0001' },
    },
    usersCIDEV: {
        IBCUSER: {
            username: 'testqa1002', password: 'Test0001' },
    },

    // QAI Environment - Test data for Resetting Password
    SSN : "4988",
    LastName : "LNAME4988",
    DOB : '10/12/1963',
    PostalCode : '02062',
    NewPassword: 'test0002',
    DefaultPassword: 'test0001',

    SSNforProxyCIDEV: "200001201",  // CIDEV Environment
    SSNforProxyQAI: "892670507",  // QAI Environment - Client - PBSPHL
    SSNforProxyCITEST: "200001201",  // CSO Environment
    SSNforProxyCSO: "200001201",  // CSO Environment
    SSNforProxyPROD: "200001201",  // PROD Environment

    DCPlan: "401",   // DC Plan
    DBPlan: "MCN",   // DB Plan


};

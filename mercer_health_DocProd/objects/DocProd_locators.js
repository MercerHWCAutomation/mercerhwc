module.exports = {
    sections: {
        HomePage: {
            selector: 'body',
            elements: {
                ParticipantsTab : {
                    locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Participants")]/../../a'
                },
                Administrationheading: {
                    locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Administration Foundation")]'
                },
                WelcomeTitle : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Welcome")]'
                },
            }
        },
        ParticipantSearchPage: {
            selector: 'body',
            elements: {
                Titleparticipantsearch : {locateStrategy: 'xpath',
                    selector: '//td[contains(text(),"Enter the search")]/../../tr[1]/td/table/tbody/tr/td/span'
                },
                inputsocialid : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Social ID")]/../../td[2]/input'
                },
                inputlastname : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Last Name")]/../../td[2]/input'
                },
                inputFirstname: {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"First Name")]/../../td[2]/input'
                },
                inputalternativeusername: {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Alternate User Name")]/../../td[2]/input'
                },
                btnSearchbySocialid : {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Social ID")]/../../td[2]/b/input'
                },

                btnSearchbySocialidPROD: {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Social ID")]/../../td[2]/input[2]'
                },

                btnSearchbyAlternativeUsername: {locateStrategy: 'xpath',
                    selector: '//*[contains(text(),"Alternate User Name")]/../../td[2]/b/input'
                },

            }
        },
        ParticipantResultsPage: {
            selector: 'body',
            elements: {
                linkFirstParticipantCode : {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp416156002_displayGrid"]/tbody/tr[2]/td[1]/a'
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[2]/td[1]/a'
                },
                linkSecondParticipantCode : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[3]/td[1]/a'
                },
                linkThirdParticipantCode : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[4]/td[1]/a'
                },
                FirstParticipantStatus : {locateStrategy: 'xpath',
                    selector: '///*[@class="geostandardgridview"]/tbody/tr[2]/td[4]'
                },
                SecondParticipantStatus : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[3]/td[4]'
                },
                ThirdParticipantStatus : {locateStrategy: 'xpath',
                    selector: '//*[@class="geostandardgridview"]/tbody/tr[4]/td[4]'
                },
                linkParticipantCode : {locateStrategy: 'xpath',
                    selector: '//td[text()="KAIS15"]/../td/a'
                },
            }
        },
        ParticipantSummaryPage: {
            selector: 'body',
            elements: {
                linkCreateDocument : {locateStrategy: 'xpath',
                    selector: '//a[text()="Create Document"]'
                },
                linkViewCommunicationHistory : {locateStrategy: 'xpath',
                    selector: '//a[text()="View Communication History"]'
                },
            }
        },
        DocProdChoosePlanPage: {
            selector: 'body',
            elements: {
                Communicationhistorytab : {locateStrategy: 'xpath',
                   // selector: '//*[@id="ctl00_TopazWebPartManager1_twp1770860988"]/div/ul/li[1]/a/span'
                    selector: '//*[contains(text(),"Communications History")]'
                },
                CreateDocumentTab: {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp1770860988"]/div/ul/li[2]/a/span'
                    selector: '//*[contains(text(),"Create a Document")]'
                },
                linkGenericDBDocuments: {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp1626083245"]/ul/ul[1]/li[1]/a'

                    selector: '//*[contains(text(),"Generic DB Documents")]'
                },
                linkGenericHBDocuments: {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp1626083245"]/ul/ul[2]/li/a'
                    selector: '//*[contains(text(),"Generic HB Documents")]'
                },

            }
        },
        ChooseDocumentPage: {
            selector: 'body',
            elements: {
                btnCreateforDB : {locateStrategy: 'xpath',
                    selector: '//td[text()="Ack"]/..//td/b/input'
                },
                btnCreateforDBPROD : {locateStrategy: 'xpath',
                    selector: '//td[text()="Ack"]/..//td/input'
                },

                btnCreateforHB : {locateStrategy: 'xpath',
                    selector: '//td[text()="ACKHB"]/..//td/b/input'
                },

                btnCreateforHBPROD : {locateStrategy: 'xpath',
                    selector: '//td[text()="ACKHB"]/..//td/input'
                },

                btnCreateforInterviewNowflow : {locateStrategy: 'xpath',
                    selector: '//td[text()="APLDN"]/..//td//input'
                },

                tabHealthForms : {locateStrategy: 'xpath',
                    selector: '//span[contains(text(),"Health Forms")]'
                },

            }
        },
        RecipientDeliveryDetailsPage: {
            selector: 'body',
            elements: {
                btnNext : {locateStrategy: 'xpath',
                    // selector: '//input[@name="ctl00$TopazWebPartManager1$twp1068291311$ctl137"]'
                    selector: '//input[@value="Next"]'
                },
                radiobtnManuallysendtoparticipant : {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp1068291311"]/div[1]/div[2]/div[1]/div/p[2]/input'
                    selector: '//p[@class="minitab_radio_button lastp"]/input'
                },
            }
        },
        ReviewDocumentPage: {
            selector: 'body',
            elements: {
                btnSubmit : {locateStrategy: 'xpath',
                    selector: '//input[@value="Submit"]'
                },
                LinkProcessingDetails : {locateStrategy: 'xpath',
                    selector: '//*[@id="ctl00_TopazWebPartManager1_twp1098303757"]/table/tbody/tr[2]/td/a/span'
                },
                btnDestroy : {locateStrategy: 'xpath',
                    selector: '//input[@value="Destroy"]'
                },
                ReviewDocStatus: {selector: '.content table tbody p'},
            }
        },
        DocProdCommunicationHistoryPage: {
            selector: 'body',
            elements: {
                labelConfirmationmsg : {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp2050199440"]/div[1]/div/p'
                    selector: '//*[@class="containercontent messagecontainer containercontent_confirm"]/div/p'
                },
                TableStatuscolumnEight : {locateStrategy: 'xpath',
                    //selector: "#ctl00_TopazWebPartManager1_twp2050199440 > div:nth-child(4) > table > tbody > tr:nth-child(1) > td:nth-child(8)"
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[1]/td[8]'
                },
                TableStatusEighthcolumn : {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp2050199440"]/div/table/tbody/tr[1]/td[8]'
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[1]/td[8]'
                },
                linkDetails : {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp2050199440"]/div/table/tbody/tr[1]/td[10]/a'
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[1]/td[10]/a'
                },
                Communicationhistorytab : {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp743949268"]/div/ul/li[1]/a/span'
                    selector: '//*[contains(text(),"Communications History")]'
                },
                Communicationhistorytabafterswitch : {locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp1770860988"]/div/ul/li[1]/a/span'
                    //*[@id="ctl00_TopazWebPartManager1_twp1770860988"]/div/ul/li[1]/a/span
                    selector: '//*[contains(text(),"Communications History")]'
                },
                linkarchive : { locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp2050199440"]/div/table/tbody/tr[1]/td[9]/a'
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[1]/td[9]/a'
                },

                linkarchivetext : { locateStrategy: 'xpath',
                    // selector: '//*[@id="ctl00_TopazWebPartManager1_twp2050199440"]/div/table/tbody/tr[1]/td[9]/a'
                    selector: '//table[@class="geostandardgridview"]/tbody/tr[1]/td[9]'
                },
                createDocument_tab : { locateStrategy: 'xpath',
                   // selector: '//*[@id="ctl00_TopazWebPartManager1_twp743949268"]/div/ul/li[2]/a/span'
                    selector: '//*[contains(text(),"Create a Document")]'
                },
            }
        },
    }
};

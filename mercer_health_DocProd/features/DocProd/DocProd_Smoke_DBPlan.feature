Feature: Smoke Scenarios for Creating Document for DB Plan

@DocProd @CreateDocument @DBPlan
    Scenario: Create Document for DB Plan
      Given User Navigate to DocProd Home Page
      Then Verify that Home Page loads successfully
      And Verify AdministrationFoundation title in home page
      And Verify Welcome Title in home Page
      When User Clicks on Participants Tab
      Then Verify that Participant Search Page loads successfully
      When User Enters SSN in Social ID field and clicks on search button
      Then Verify that Participant Results Page loads successfully
      When User Clicks on first Participant Code
      Then Verify that Participant Summary Page loads successfully
      When User Clicks on Create Document link
      Then Verify that DocProd Choose Plan Page loads successfully with DB Plan
      Then Verify Document Stage Status for DB Document
      Then User Navigate to DocProd Choose Plan Page
      Then User Navigate to Create Document Page

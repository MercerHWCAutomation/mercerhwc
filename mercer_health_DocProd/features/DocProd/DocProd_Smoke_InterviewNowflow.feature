Feature: Smoke Scenarios for InterviewNowflow

@DocProd @CreateDocument @InterviewNowflow
  Scenario: InterviewNow flow
    Given User Navigate to DocProd Home Page
    Then Verify that Home Page loads successfully
    And Verify AdministrationFoundation title in home page
    And Verify Welcome Title in home Page
    When User Clicks on Participants Tab
    Then Verify that Participant Search Page loads successfully
    When User Enters SSN in Social ID field
    Then Verify that Participant Results Page loads successfully
    When User Clicks on Participant Code
    Then Verify that Participant Summary Page loads successfully
    When User Clicks on Create Document link
    Then InterviewNow flow with HB Plan

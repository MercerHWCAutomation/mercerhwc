var action = require('../../../Keywords');
var Objects = require(__dirname + '/../../objects/DocProd_locators.js');
//var robot = require('robot-js');
var data = require('../../test_data/DocProd_TestData.js');
var robot123 = require('robotjs');
var browser;

module.exports = function () {
	
	this.Given(/^User Navigate to DocProd Home Page$/, function (browser) {
		var URL;
		action.initializePageObjects(browser, function () {
			if(data.TestingEnvironment == "QAI")
				URL = data.urlQAI;
			else if(data.TestingEnvironment == "CSO")
				URL = data.urlCSO;
			else if(data.TestingEnvironment == "CITEST")
				URL = data.urlCITEST;
			else{
				URL = data.urlPROD;
			}
			browser.maximizeWindow()
				.deleteCookies()
				.url(URL);
			browser.pause(data.shortwaittime);
			
			//extra added steps to comment
			//----------------------------------
			// browser.pause(10000);
			// robot123.typeString('xxxxxxxxx');
			// browser.pause(5000);
			// robot123.keyTap("tab");
			// browser.pause(5000);
			// robot123.typeString(xxxxxxxxx');
			// browser.pause(5000);
			// robot123.keyTap("enter");
			// browser.pause(10000);
			// console.log("after enter tap");
			// //---------------------------
			
			
		});
	});
	
	this.Then(/^Verify that Home Page loads successfully$/, function (browser) {
		action.initializePageObjects(browser, function () {
			action.isDisplayed("HomePage|Administrationheading",function () {
				browser.saveScreenshot('./screenshots/DocProdHomePage.png')
				// browser.assert.urlContains("home")
				browser.url(function (current_url) {
					var pageurl = current_url.value
					var pageurl = pageurl.toUpperCase();
					if (pageurl.includes("HOME")) {
						console.log("Home Page loaded successfully")
					}
					else if(pageurl.includes("ERROR")) {
						console.log("Error loaded for Home Page")
					}
				})
			});
        });
        
    });
    this.Then(/^Verify AdministrationFoundation title in home page$/, function (browser) {
        action.initializePageObjects(browser, function () {
	        this.demoTest = function (browser) {
		        browser.getTitle(function(title) {
			        this.assert.equal(typeof title, 'string');
			        this.assert.equal(title, 'Administration Foundation');
		        });
	        };
        });
       
    });
    this.When(/^User Clicks on Participants Tab$/, function (browser) {
       action.initializePageObjects(browser, function () {
	       action.performClick("HomePage|ParticipantsTab",function () {
		       browser.timeoutsImplicitWait(data.shortwaittime);
	       });
       });
       
    });
    this.Then(/^Verify Welcome Title in home Page$/, function (browser) {
       action.initializePageObjects(browser, function () {
	       browser.pause(data.averagepause);
	       action.isDisplayed("HomePage|WelcomeTitle",function () {
	       });
       });
       
    });
    this.Then(/^Verify that Participant Search Page loads successfully$/, function (browser) {
       action.initializePageObjects(browser, function () {
	       browser.pause(data.averagepause);
	       action.isDisplayed("ParticipantSearchPage|inputsocialid",function () {
	       });
	       browser.assert.urlContains("ParticipantSearch")
	       browser.saveScreenshot('./screenshots/ParticipantSearchPage.png')
       });
       
    });
    this.When(/^User Enters SSN in Social ID field and clicks on search button$/, function (browser) {
        action.initializePageObjects(browser, function () {
	        action.setText("ParticipantSearchPage|inputsocialid", data.SSN);
	
	        if (data.TestingEnvironment == "PROD")  {
		        action.performClick("ParticipantSearchPage|btnSearchbySocialidPROD", function () {
			        browser.timeoutsImplicitWait(data.averagewaittime);
		        });
	        }
	        else
	        {
		        action.performClick("ParticipantSearchPage|btnSearchbySocialid", function () {
			        browser.timeoutsImplicitWait(data.averagewaittime);
		        });
	        }
        });
      

    });
    this.Then(/^Verify that Participant Results Page loads successfully$/, function (browser) {
      //  browser.pause(data.longpause);
       action.initializePageObjects(browser, function () {
	       action.isDisplayed("ParticipantResultsPage|linkFirstParticipantCode",function () {
	       });
	       browser.assert.urlContains("ParticipantResults")
       });
       
    });
    this.When(/^User Clicks on first Participant Code$/, function (browser) {
       action.initializePageObjects(browser, function () {
	       action.performClick("ParticipantResultsPage|linkFirstParticipantCode",function () {
		       browser.timeoutsImplicitWait(data.averagewaittime);
	       });
       });
       
    });

    this.When(/^User Clicks on Participant Code$/, function (browser) {
       action.initializePageObjects(browser, function () {
	       action.performClick("ParticipantResultsPage|linkParticipantCode",function () {
		       browser.timeoutsImplicitWait(data.averagewaittime);
	       });
       });
       
    });

    this.Then(/^Verify that Participant Summary Page loads successfully$/, function (browser) {
       // browser.pause(data.longpause);
       action.initializePageObjects(browser, function () {
	       action.isDisplayed("ParticipantSummaryPage|linkCreateDocument",function () {
		       browser.assert.urlContains("ParticipantSummary")
	       });
       });
       

    });
    this.When(/^User Clicks on Create Document link$/, function (browser) {
      action.initializePageObjects(browser, function () {
	      action.performClick("ParticipantSummaryPage|linkCreateDocument",function () {
		      // browser.pause(data.longpause);
		      browser.timeoutsImplicitWait(data.averagewaittime);
	      });
      });
      
    });
    this.Then(/^Verify that DocProd Choose Plan Page loads successfully with DB Plan$/, function (browser) {
      action.initializePageObjects(browser, function () {
	      browser.window_handles(function(result) {
		      var handle = result.value[1];
		      browser.switchWindow(handle);
		      browser.assert.urlContains("DocProdChoosePlan")
//            if ( data.NameofThebrowser == "ie" ) {
		      action.isDisplayed("DocProdChoosePlanPage|CreateDocumentTab", function () {
			      if ( data.SelectPlanName1 == "DB") {
				      action.performClick("DocProdChoosePlanPage|linkGenericDBDocuments", function () {
					      // browser.pause(data.longpause);
					      browser.timeoutsImplicitWait(data.shortwaittime);
					      browser.assert.urlContains("DocProdChooseDocument");
					      if (data.TestingEnvironment == "PROD")  {
						      action.performClick("ChooseDocumentPage|btnCreateforDBPROD", function () {
							      // browser.pause(data.longpause);
							      browser.timeoutsImplicitWait(data.shortwaittime);
							      browser.assert.urlContains("DocProdRecipientDeliveryDetails");
							      if (data.TestingEnvironment == "PROD") {
								      action.performClick("RecipientDeliveryDetailsPage|radiobtnManuallysendtoparticipant", function () {
									      browser.pause(data.shortpause);
								      });
							      }
						      });
					      }
					      else
					      {
						      action.performClick("ChooseDocumentPage|btnCreateforDB", function () {
							      // browser.pause(data.longpause);
							      browser.timeoutsImplicitWait(data.shortwaittime);
							      browser.assert.urlContains("DocProdRecipientDeliveryDetails");
							      if (data.TestingEnvironment == "PROD") {
								      action.performClick("RecipientDeliveryDetailsPage|radiobtnManuallysendtoparticipant", function () {
									      browser.pause(data.shortpause);
								      });
							      }
						      });
					      }
					
					      action.performClick("RecipientDeliveryDetailsPage|btnNext", function () {
						      // browser.pause(data.longpause);
						      browser.timeoutsImplicitWait(data.averagewaittime);
						      browser.assert.urlContains("DocProdReviewDocument");
					      });
					      action.isDisplayed("ReviewDocumentPage|btnSubmit",function(){
						      // if (value) {
						      action.performClick("ReviewDocumentPage|btnSubmit", function () {
							      // browser.pause(data.longpause);
							      browser.timeoutsImplicitWait(data.averagewaittime);
							      browser.assert.urlContains("DocProdCommunicationHistory");
						      });
						      // }
					      })
					      action.getText("DocProdCommunicationHistoryPage|labelConfirmationmsg", function (txt) {
						      var res = txt.split("Tracking Code: ");
						      var lastarrayvalue = res[res.length - 1];
						      console.log(lastarrayvalue)
					      })
					      action.getText("DocProdCommunicationHistoryPage|TableStatuscolumnEight", function (txt) {
						      console.log(txt)
						      // var res = txt.toUpperCase();
						      if (txt == "COOLOFF" || txt == "ASSIGNED" || txt == "SUBMITTED" ) {
							      // Clicks on Details link
							      action.performClick("DocProdCommunicationHistoryPage|linkDetails", function () {
								      browser.pause(data.longpause);
								      browser.timeoutsImplicitWait(data.shortwaittime);
								      browser.assert.urlContains("DocProdReviewDocument");
							      });
						      }
					      })
					
				      });
			      }
		      });
//            }
	      });
      });
     

        // action.isDisplayed("DocProdChoosePlanPage|linkGenericDBDocuments",function () {
        //     console.log("DocProdChoosePlan Page loads successfully")
        // });
    });
    this.Then(/^Verify that DocProd Choose Plan Page loads successfully with HB Plan$/, function (browser) {
       action.initializePageObjects(browser, function () {
	       browser.window_handles(function(result) {
		       var handle = result.value[1];
		       browser.switchWindow(handle);
		       browser.assert.urlContains("DocProdChoosePlan")
		       action.isDisplayed("DocProdChoosePlanPage|CreateDocumentTab", function () {
			       if ( data.SelectPlanName2 == "HB") {
				       action.performClick("DocProdChoosePlanPage|linkGenericHBDocuments", function () {
					       // browser.pause(data.longpause);
					       browser.timeoutsImplicitWait(data.shortwaittime);
					       browser.assert.urlContains("DocProdChooseDocument");
					
					       if (data.TestingEnvironment == "PROD")  {
						       action.performClick("ChooseDocumentPage|btnCreateforHBPROD", function () {
							       // browser.pause(data.longpause);
							       browser.timeoutsImplicitWait(data.shortwaittime);
							       browser.assert.urlContains("DocProdRecipientDeliveryDetails");
							       if (data.TestingEnvironment == "PROD") {
								       action.performClick("RecipientDeliveryDetailsPage|radiobtnManuallysendtoparticipant", function () {
									       // browser.pause(data.shortpause);
									       browser.timeoutsImplicitWait(data.shortwaittime);
								       });
							       }
						       });
					       }
					       else
					       {
						       action.performClick("ChooseDocumentPage|btnCreateforHB", function () {
							       // browser.pause(data.longpause);
							       browser.timeoutsImplicitWait(data.shortwaittime);
							       browser.assert.urlContains("DocProdRecipientDeliveryDetails");
							       if (data.TestingEnvironment == "PROD") {
								       action.performClick("RecipientDeliveryDetailsPage|radiobtnManuallysendtoparticipant", function () {
									       // browser.pause(data.shortpause);
									       browser.timeoutsImplicitWait(data.shortwaittime);
								       });
							       }
						       });
					       }
					
					       action.performClick("RecipientDeliveryDetailsPage|btnNext", function () {
						       // browser.pause(data.longpause)
						       browser.timeoutsImplicitWait(data.averagewaittime);
						       browser.assert.urlContains("DocProdReviewDocument");
					       });
					
					       action.isDisplayed("ReviewDocumentPage|btnSubmit",function(){
						       // console.log(value)
						       // if (value) {
						       action.performClick("ReviewDocumentPage|btnSubmit", function () {
							       // browser.pause(data.longpause);
							       browser.timeoutsImplicitWait(data.averagewaittime);
							       browser.assert.urlContains("DocProdCommunicationHistory");
						       });
						       // }
					       })
					       action.getText("DocProdCommunicationHistoryPage|labelConfirmationmsg", function (txt) {
						       var res = txt.split("Tracking Code: ");
						       var lastarrayvalue = res[res.length - 1];
					       })
					       action.getText("DocProdCommunicationHistoryPage|TableStatuscolumnEight", function (txt) {
						       browser.pause(data.shortpause);
						       var res = txt.toUpperCase();
						       console.log(res)
						       if (txt == "COOLOFF" || txt == "ASSIGNED" || txt =="SUBMITTED" ) {
							       // Clicks on Details link
							       action.performClick("DocProdCommunicationHistoryPage|linkDetails", function () {
								       browser.pause(data.longpause);
								       browser.timeoutsImplicitWait(data.shortwaittime);
								       browser.assert.urlContains("DocProdReviewDocument");
							       });
						       }
					       })
				       });
			       }
		       });
		       //           }
	       });
       });
       

        // action.isDisplayed("DocProdChoosePlanPage|linkGenericDBDocuments",function () {
        //     console.log("DocProdChoosePlan Page loads successfully")
        // });
    });
    this.Then(/^Verify Document Stage Status for DB Document$/, function (browser) {
        // Clicks on Submit button
       action.initializePageObjects(browser, function () {
	       action.getText("ReviewDocumentPage|ReviewDocStatus", function (txt) {
		       if (txt.includes("cool off")) {
			       action.isDisplayed("ReviewDocumentPage|btnSubmit",function(){
				       // if (value ) {
				       action.performClick("ReviewDocumentPage|btnSubmit", function () {
					       // browser.pause(data.longpause);
					       action.isDisplayed("DocProdCommunicationHistoryPage|createDocument_tab",function () {
						       browser.assert.urlContains("DocProdCommunicationHistory");
					       });
					       action.performClick("DocProdCommunicationHistoryPage|createDocument_tab", function () {
						       // browser.pause(data.shortpause);
						       browser.timeoutsImplicitWait(data.shortwaittime);
						       browser.assert.urlContains("DocProdChoosePlan");
						       console.log("create document tab is clicked");
						       browser.pause(data.longpause);
					       });
				       });
				       // }
			       })
		       }
	       });
	
	       // browser.pause(data.longpause);
	       action.performClick("DocProdCommunicationHistoryPage|Communicationhistorytabafterswitch", function () {
		       action.isDisplayed("DocProdCommunicationHistoryPage|linkDetails",function () {
			       browser.assert.urlContains("DocProdCommunicationHistory");
		       });
	       });
	       action.getText("DocProdCommunicationHistoryPage|TableStatusEighthcolumn", function (txt) {
		       var res = txt.toUpperCase();
		       console.log(res)
		       if(res == "FINISHED") {
			       console.log("Verified that Document Status reached 'Finished' stage successfully")
			       browser.saveScreenshot('./screenshots/DocumentStatusForDBDocument.png')
			       if (data.TestingEnvironment == "PROD") {
				       // Clicks on Details link
				       action.performClick("DocProdCommunicationHistoryPage|linkDetails", function () {
					       browser.timeoutsImplicitWait(data.shortwaittime);
					       browser.assert.urlContains("DocProdReviewDocument");
				       });
				       action.isDisplayed("ReviewDocumentPage|btnDestroy",function () {
					       action.performClick("ReviewDocumentPage|btnDestroy", function () {
						       browser
							       .acceptAlert()
						       action.getText("DocProdCommunicationHistoryPage|TableStatusEighthcolumn", function (txt) {
							       var res = txt.toUpperCase();
							       console.log(res)
							       if(res == "CANCELLED") {
								       console.log("Verified that Document Status reached 'Cancelled' stage successfully after destroying the document")
								       browser.saveScreenshot('./screenshots/DocumentStatusAfterDestroyForDBDocument.png')
							       }
						       });
					       });
				       });
			       }
			
			       // Clicks on Archieve link
			       //  action.performClick("DocProdCommunicationHistoryPage|linkarchive", function () {
			       //       browser.pause(data.averagepause)
			       //           browser.window_handles(function(result) {
			       //           var handle2 = result.value[2];
			       //           browser.switchWindow(handle2);
			       //           browser.assert.urlContains("DocProdDocumentViewer");
			       //           console.log("DocProdDocumentViewer page is loaded successfully");
			       //           browser.saveScreenshot('./screenshots/DocProdDocumentViewerForDBDocument.png')
			       //       });
			       //  });
			       action.getText("DocProdCommunicationHistoryPage|linkarchivetext", function (txt) {
				       if (txt == "") {
					       console.log("Archive link is not displayed so currently we are unable to open the document")
				       }
				       else {
					       // Clicks on Archieve link
					       action.performClick("DocProdCommunicationHistoryPage|linkarchive", function () {
						       browser.pause(data.averagepause)
						       browser.window_handles(function(result) {
							       var handle2 = result.value[2];
							       browser.switchWindow(handle2);
							       browser.assert.urlContains("DocProdDocumentViewer");
							       console.log("DocProdDocumentViewer page is loaded successfully");
							       browser.saveScreenshot('./screenshots/DocProdDocumentViewerForDBDocument.png')
						       });
					       });
				       }
			       })
			
		       }
	       })
	       browser.closeWindow();
       });
      
     });
    this.Then(/^User Navigate to DocProd Choose Plan Page$/, function (browser) {
       action.initializePageObjects(browser, function () {
	       browser.window_handles(function(result) {
		       var handle = result.value[1];
		       browser.switchWindow(handle);
		       browser.closeWindow();
	       });
       });
      
    });
    this.Then(/^Verify Document Stage Status for HB Document$/, function (browser) {
        // Clicks on Submit button
      action.initializePageObjects(browser, function () {
	      action.getText("ReviewDocumentPage|ReviewDocStatus", function (txt) {
		      if (txt.includes("cool off")) {
			      action.isDisplayed("ReviewDocumentPage|btnSubmit",function(){
				      // if (value ) {
				      action.performClick("ReviewDocumentPage|btnSubmit", function () {
					      // browser.pause(data.longpause);
					      action.isDisplayed("DocProdCommunicationHistoryPage|createDocument_tab",function () {
						      browser.assert.urlContains("DocProdCommunicationHistory");
					      });
					      action.performClick("DocProdCommunicationHistoryPage|createDocument_tab", function () {
						      // browser.pause(data.shortpause);
						      browser.timeoutsImplicitWait(data.shortwaittime);
						      browser.assert.urlContains("DocProdChoosePlan");
						      console.log("create document tab is clicked");
						      browser.pause(data.longpause);
					      });
				      });
				      // }
			      })
		      }
	      });
	
	      // browser.pause(data.longpause);
	      action.performClick("DocProdCommunicationHistoryPage|Communicationhistorytabafterswitch", function () {
		      action.isDisplayed("DocProdCommunicationHistoryPage|linkDetails",function () {
		      });
		      browser.assert.urlContains("DocProdCommunicationHistory");
	      });
	      action.getText("DocProdCommunicationHistoryPage|TableStatusEighthcolumn", function (txt) {
		      var res = txt.toUpperCase();
		      console.log(res)
		      if(res == "FINISHED") {
			      console.log("Verified that Document Status reached 'Finished' stage successfully")
			      browser.saveScreenshot('./screenshots/DocumentStatusForHBDocument.png')
			      if (data.TestingEnvironment == "PROD") {
				      // Clicks on Details link
				      action.performClick("DocProdCommunicationHistoryPage|linkDetails", function () {
					      browser.timeoutsImplicitWait(data.shortwaittime);
					      browser.assert.urlContains("DocProdReviewDocument");
				      });
				      action.isDisplayed("ReviewDocumentPage|btnDestroy",function () {
					      action.performClick("ReviewDocumentPage|btnDestroy", function () {
						      browser
							      .acceptAlert()
						      action.getText("DocProdCommunicationHistoryPage|TableStatusEighthcolumn", function (txt) {
							      var res = txt.toUpperCase();
							      console.log(res)
							      if(res == "CANCELLED") {
								      console.log("Verified that Document Status reached 'Cancelled' stage successfully after destroying the document")
								      browser.saveScreenshot('./screenshots/DocumentStatusAfterDestroyForHBDocument.png')
							      }
						      });
					      });
				      });
			      }
			      action.getText("DocProdCommunicationHistoryPage|linkarchivetext", function (txt) {
				      if (txt == "") {
					      console.log("Archive link is not displayed so currently we are unable to open the document")
				      }
				      else {
					      // Clicks on Archieve link
					      action.performClick("DocProdCommunicationHistoryPage|linkarchive", function () {
						      browser.pause(data.averagepause)
						      browser.window_handles(function(result) {
							      var handle = result.value[2];
							      browser.switchWindow(handle);
							      browser.assert.urlContains("DocProdDocumentViewer");
							      console.log("DocProdDocumentViewer page is loaded successfully");
							      browser.saveScreenshot('./screenshots/DocProdDocumentViewerForHBDocument.png')
						      });
						      browser.closeWindow();
					      });
				      }
			      })
			      // Clicks on Archieve link
			      // action.performClick("DocProdCommunicationHistoryPage|linkarchive", function () {
			      //     browser.pause(data.averagepause)
			      //     browser.window_handles(function(result) {
			      //         var handle = result.value[2];
			      //         browser.switchWindow(handle);
			      //         browser.assert.urlContains("DocProdDocumentViewer");
			      //         console.log("DocProdDocumentViewer page is loaded successfully");
			      //         browser.saveScreenshot('./screenshots/DocProdDocumentViewerForHBDocument.png')
			      //     });
			      //     browser.closeWindow();
			      // });
		      }
	      })
	      browser.closeWindow();
      });
      
    });
    this.Then(/^User Navigate to Create Document Page$/, function (browser) {
       action.initializePageObjects(browser, function () {});
        browser.window_handles(function(result) {
            var handle = result.value[0];
            browser.switchWindow(handle);
        });
    });

    this.Then(/^InterviewNow flow with HB Plan$/, function (browser) {
       action.initializePageObjects(browser, function () {});
        browser.window_handles(function(result) {
            var handle = result.value[1];
            browser.switchWindow(handle);
            browser.assert.urlContains("DocProdChoosePlan")
            console.log("DocProd Choose Plan Page loads successfully");
//            if ( data.NameofThebrowser == "ie" ) {
                action.isDisplayed("DocProdChoosePlanPage|CreateDocumentTab", function () {
                    console.log("Create Document tab is displayed in DocPord Choose Plan Page");
                    // if ( data.InterviewNowflow == "YES" && data.SelectPlanName2 == "HB")
                    if ( data.InterviewNowflow == "YES")
                    {
                        action.performClick("DocProdChoosePlanPage|linkGenericHBDocuments", function () {
                            // browser.pause(data.longpause);
                            browser.timeoutsImplicitWait(data.averagewaittime);
                            browser.assert.urlContains("DocProdChooseDocument");
                            console.log("DocProdChooseDocument page is displayed");
                            action.performClick("ChooseDocumentPage|btnCreateforInterviewNowflow", function () {
                                // browser.pause(data.longpause);
                                browser.timeoutsImplicitWait(data.averagewaittime);
                                browser.assert.urlContains("DocProdRecipientDeliveryDetails");
                                console.log("DocProdRecipientDeliveryDetails page is displayed");
                                if (data.TestingEnvironment == "PROD") {
                                    action.performClick("RecipientDeliveryDetailsPage|radiobtnManuallysendtoparticipant", function () {
                                        browser.pause(data.shortpause);
                                    });
                                }
                            });
                            action.performClick("RecipientDeliveryDetailsPage|btnNext", function () {
                                // browser.pause(data.longpause);
                                browser.timeoutsImplicitWait(data.averagewaittime);
                                browser.assert.urlContains("DocProdReviewDocument");
                                console.log("DocProdReviewDocument page is displayed");
                            });
                            // browser.setValue("evententered")
                            robot123.typeString("evententered");
                            // var Keyboard = robot.Keyboard;
                            // var k = Keyboard();
                            // robot123.keyTap("enter");
                            // robot123.setKeyboardDelay(40000);
                            // k.click(robot.KEY_TAB);
                            // k.click(robot.KEY_TAB);
                            // k.click(robot.KEY_ENTER);
                            // browser.pause(data.averagepause);
                            // browser.setValue("planname")
                            robot123.keyTap("tab");
                            robot123.keyTap("tab");
                            robot123.keyTap("enter");
                            robot123.setKeyboardDelay(4000);
                            robot123.typeString("planname");
                            robot123.keyTap("tab");
                            robot123.keyTap("tab");
                            robot123.keyTap("enter");
                            robot123.setKeyboardDelay(4000);
                            // k.click(robot.KEY_TAB);
                            // k.click(robot.KEY_TAB);
                            // k.click(robot.KEY_ENTER);
                            // browser.pause(data.averagepause);
                            browser.setValue("10/02/2015")
                            robot123.keyTap("tab");
                            robot123.keyTap("tab");
                            robot123.keyTap("enter");
                            browser.pause(data.averagepause);
                            action.isDisplayed("ReviewDocumentPage|btnSubmit",function(){
                                // console.log(value)
                                // if (value) {
                                    action.performClick("ReviewDocumentPage|btnSubmit", function () {
                                        browser.pause(data.longpause);
                                        browser.assert.urlContains("DocProdCommunicationHistory");
                                        console.log("DocProdCommunicationHistory page is displayed");
                                    });
                                // }
                            })
                            action.getText("DocProdCommunicationHistoryPage|labelConfirmationmsg", function (txt) {
                                var res = txt.split("Tracking Code: ");
                                console.log(res)
                                var lastarrayvalue = res[res.length - 1];
                                console.log(lastarrayvalue)
                            })
                            action.getText("DocProdCommunicationHistoryPage|TableStatuscolumnEight", function (txt) {
                                var res = txt.toUpperCase();
                                if (txt == "PEER_REVIEW" ) {
                                    // Clicks on Details link
                                    console.log("Verified that Document Status reached 'Peer Review' stage successfully")
                                    browser.saveScreenshot('./screenshots/InterviewNowflowDocumentStatusForHBDocument.png')
                                    // action.performClick("DocProdCommunicationHistoryPage|linkDetails", function () {
                                    //     browser.assert.urlContains("DocProdReviewDocument");
                                    //     console.log("DocProdReviewDocument page is displayed");
                                    // });
                                }

                            })

                        });
                    }
                });
//            }
        });

        // action.isDisplayed("DocProdChoosePlanPage|linkGenericDBDocuments",function () {
        //     console.log("DocProdChoosePlan Page loads successfully")
        // });
    });

    this.When(/^User Enters SSN in Social ID field for Interviewflow and clicks on search button$/, function (browser) {
      action.initializePageObjects(browser, function () {});
        action.setText("ParticipantSearchPage|inputsocialid",data.SSNInterviewnow);
        browser.timeoutsImplicitWait(data.shortwaittime);
        action.setText("ParticipantSearchPage|inputlastname",data.LastName);
        browser.timeoutsImplicitWait(data.shortwaittime);
        action.setText("ParticipantSearchPage|inputFirstname",data.FirstName);
        browser.timeoutsImplicitWait(data.shortwaittime);
        action.setText("ParticipantSearchPage|inputClientname",data.ClientName);
        browser.timeoutsImplicitWait(data.shortwaittime);
        action.performClick("ParticipantSearchPage|btnSearchByLastname",function () {
            browser.timeoutsImplicitWait(data.averagewaittime);
        });
    });

    this.When(/^Clicks on Create Document link$/, function (browser) {
      action.initializePageObjects(browser, function () {});
        action.performClick("ParticipantSummaryPage|linkCreateDocument",function () {
            browser.pause(data.averagepause);
        });
    });

    this.When(/^User Enters SSN in Social ID field$/, function (browser) {
        action.initializePageObjects(browser, function () {});
        action.setText("ParticipantSearchPage|inputsocialid", data.SSNInterviewnow);

        if (data.TestingEnvironment == "PROD")  {
            action.performClick("ParticipantSearchPage|btnSearchbySocialidPROD", function () {
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }
        else
        {
            action.performClick("ParticipantSearchPage|btnSearchbySocialid", function () {
                browser.timeoutsImplicitWait(data.averagewaittime);
            });
        }

    });

};



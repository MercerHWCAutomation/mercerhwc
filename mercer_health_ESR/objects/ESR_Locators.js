﻿var data=require('../test_data/ESR_TestData');
var d=require('../features/step_definitions/ESR_Smoke');
module.exports = {
    sections: {

        //ESR locators
        ESRHomePage: {
            selector: 'body',
            elements: {
                pageTitle: {locateStrategy: 'xpath', selector: '//a[contains(text(),"ESR")]'},
                Client:{locateStrategy: 'xpath', selector: '//a[contains(text(),"'+ data.MI_Client +'")]'},
                pageHeadingHome: {locateStrategy: 'xpath', selector: "//header/div/div/div/ul/li/a[contains(text(),'Home')]"},

                linkClientSLS: {locateStrategy: 'xpath', selector: '//a[contains(text(),"SLSDM")]'},
                linkClientQA01CL: {locateStrategy: 'xpath', selector: '//a[contains(text(),"QA01CL")]'},
                linkClientYCCTST: {locateStrategy: 'xpath', selector: '//a[contains(text(),"YCLIFEPYR COBRA TEST")]'},
                linkClientVCTC01: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Valley Crest Companies")]'},

                linkClientESRTST: {locateStrategy: 'xpath', selector: '//a[contains(text(),"ESRTST")]'},
                linkClientChildR: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Hospital of Philadelphia")]'},

                linkClientAOLBAA: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Applied Materials")]'},
                linkClientACISTL: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Arch Coal, Inc.")]'},

                titleSLSDMPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"SLSDM")]'},
                titleQAACMEPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"QAACME")]'},

                titleESRTSTPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"ESRTST")]'},
                titleChildRPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"CHILDR")]'},

                titleAOLBAAPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"AOLBAA")]'},
                titleACISTLPage: {locateStrategy: 'xpath', selector: '//h1[contains(text(),"ACISTL")]'},
	            DataList:{locateStrategy: 'xpath', selector: "//td/a/span[contains(text(),'QA01CL_Reconciliation_Monthly_OCT_2014')]"},
               // tabReports: {locateStrategy: 'xpath', selector: '//dl/dd/a[text()='Reports'][@class='active']'},
                tabReports: {locateStrategy: 'xpath', selector: '(//dl[@class="link-list-horz col-24 inpage-navbar"])[1]//a[contains(text(),"Reports")]'},
              //  tabDataFiles: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Data Files")]/..)[1]'},
                tabDataFiles: {locateStrategy: 'xpath', selector: '(//dl[@class="link-list-horz col-24 inpage-navbar"])[1]//a[contains(text(),"Data Files")]'},
                //Feedback tab from Reports Tab
                tabFeedbackFiles1: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Feedback Files")])[4]'},
                //Feedback tab from Data Files Tab
                tabFeedbackFiles2: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Feedback Files")])[3]'},
                //Feedback tab generic
                tabFeedbackFiles: {locateStrategy: 'xpath', selector: '(//dl[@class="link-list-horz col-24 inpage-navbar"])[1]//a[contains(text(),"Feedback Files")]'},
                tabReportingYears: {locateStrategy: 'xpath', selector: "//div[@data-active-view='true']/div/div/dl/dd/a[text()='Reporting Years']"},
                tabClientDetails: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Client Details")])[2]'},
                //Datafiles: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Data Files")])[2]'},
                Reports: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Reports")]'},
                AnnualReports:{locateStrategy: 'xpath', selector: '//a[contains(text(),"Annual Reports")]'},
                GenerateFlatFile: {locateStrategy: 'xpath', selector: '(//button[@title="Generate Flat File"])[1]'},
                confirmationMessage: {locateStrategy: 'xpath', selector: '(//h5[contains(text(),"Confirmation")])[2]'},

                titleDataFiles: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Data Files")]'},
                titleReports: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Reports")]'},
                titleFeedbackFiles: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Feedback Files")]'},
                titleReportingYears: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"Reporting Years")])[3]'},
                titleClientDetails: {locateStrategy: 'xpath', selector: '//h2[contains(text(),"Client Details")]'},

	            ReportingYear:{locateStrategy:'xpath',selector:"//a[text()='"+data.ReportingYear+"']"},
	            
                linkUploadFile: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Upload File")]'},
                linkUploadBWFile: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Upload BW File")]'},

                titlePopupUploadFile: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Upload File")]'},
                buttonUploadFile: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Upload File")]'},
                buttonFileUpload: { selector: '#buttonUpload'},
                buttonCancelFile: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[3]'},
                buttonBrowseFile: {locateStrategy: 'xpath', selector: '//input[@id="upload-file-input"]'},
				
                statusOfUpload: {locateStrategy: 'xpath', selector: '(//*[@class="tbl-200-col"])[3]'},
                fileNameUploaded: {locateStrategy: 'xpath', selector: '(//*[@class="tbl-200-col"])[1]'},

                titleUploadBWFilePopUp: {selector: '#ui-dialog-title-5'},
                dropdownSelectYear: {selector: '#bw-monthly-report-year'},
                dropdownSelectMonth: {selector: '#bw-monthly-report-month'},
                
                dropdownOption2015SelectYear: {locateStrategy: 'xpath', selector: '(//option[@value="2015"])[3]'},
                dropdownOptionJanSelectMonth: {locateStrategy: 'xpath', selector: '(//option[contains(text(),"Jan")])[2]'},
                buttonRadioNoUploadBWFilePopUp: {selector: '#BW_Correction_No'},
                buttonRadioYesUploadBWFilePopUp: {selector: '#BW_Correction_Yes'},
                buttonOKUploadBWFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[3]'},
                buttonCancelUploadBWFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[5]'},

                buttonMonthlyReport: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Create Monthly Report")]'},
                buttonAnnualReport: {locateStrategy: 'xpath', selector: '//button[contains(text(),"Create Annual Report")]'},

                titleMonthlyReportPopUp: {selector: '#ui-dialog-title-2'},
	            
                Rerun:{selector:"#chkRerun"},
                // IRS
                dropdownMonthlyReportSelectIRSYear: {selector: '#irs-monthly-report-year'},
	            dropdownMonthlyReportOptionIRS2015: {locateStrategy: 'xpath', selector: '(//*[@id="irs-monthly-report-year"]/..//option[@value="2015"])'},
	            dropdownMonthlyReportOptionIRS2016: {locateStrategy: 'xpath', selector: '(//*[@id="irs-monthly-report-year"]/..//option[@value="2016"])'},
	            dropdownMonthlyReportOptionIRS2017: {locateStrategy: 'xpath', selector: '(//*[@id="irs-monthly-report-year"]/..//option[@value="2017"])'},
	            dropdownMonthlyReportOptionIRS2018: {locateStrategy: 'xpath', selector: '(//*[@id="irs-monthly-report-year"]/..//option[@value="2018"])'},
	
	            dropdownMonthlyReportOption2016: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option[@value="2016"])[2]'},
              
              //Year
                dropdownMonthlyReportSelectYear: {selector: '#monthly-report-year'},
	            dropdownMonthlyReportOptionYear2014: {locateStrategy: 'xpath', selector: '(//select[@id="monthly-report-year"]/..//option[@value="2014"])'},
                dropdownMonthlyReportOptionYear2015: {locateStrategy: 'xpath', selector: '(//select[@id="monthly-report-year"]/..//option[@value="2015"])'},
	            dropdownMonthlyReportOptionYear2016: {locateStrategy: 'xpath', selector: '(//select[@id="monthly-report-year"]/..//option[@value="2016"])'},
	            dropdownMonthlyReportOptionYear2017: {locateStrategy: 'xpath', selector: '(//select[@id="monthly-report-year"]/..//option[@value="2017"])'},
	            dropdownMonthlyReportOptionYear2018: {locateStrategy: 'xpath', selector: '(//select[@id="monthly-report-year"]/..//option[@value="2018"])'},
		
	//Month
	
	            dropdownMonthlyReportSelectMonth: {selector: '#monthly-report-month'},
                dropdownMonthlyReportOptionJan: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Jan")]'},
	            dropdownMonthlyReportOptionFeb: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Feb")]'},
	            dropdownMonthlyReportOptionMar: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Mar")]'},
	            dropdownMonthlyReportOptionApr: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Apr")]'},
	            dropdownMonthlyReportOptionMay: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"May")]'},
	            dropdownMonthlyReportOptionJun: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Jun")]'},
	            dropdownMonthlyReportOptionJul: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Jul")]'},
	            dropdownMonthlyReportOptionAug: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Aug")]'},
	            dropdownMonthlyReportOptionSep: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Sep")]'},
	            dropdownMonthlyReportOptionOct: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Oct")]'},
	            dropdownMonthlyReportOptionNov: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Nov")]'},
	            dropdownMonthlyReportOptionDec: {locateStrategy: 'xpath', selector: '//select[@id="monthly-report-month"]/..//option[contains(text(),"Dec")]'},
	
	
	            buttonOKMonthlyReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[1]'},
                buttonCancelMonthlyReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[1]'},
                messageSuccess: {locateStrategy: 'xpath', selector: '(//div[@data-bind="visible: showConfirm()"][@style=""]/span[contains(text(),"You have successfully created")])[1]'},
               
                messageSuccessMonthlyReport: {locateStrategy: 'xpath', selector: '//div[@id="applicationHost"]/div/section/div[1]/div[1]/div[@class="box-bdy"]/div/div/div[@class="message-ctn success-ctn"][@data-bind="visible: showConfirm()"][1]/span[contains(text(),"You have successfully created new monthly reports for '+data.clientname+'")]'    }, ///span[contains(text(),"You have successfully created new monthly reports for '+data.clientname+'")]
	            messageSuccessAnnualReport: {locateStrategy: 'xpath', selector: '//div[@id="applicationHost"]/div/section/div[1]/div[1]/div[@class="box-bdy"]/div/div/div[@class="message-ctn success-ctn"][@data-bind="visible: showAnnualConfirm()"][1]/span[contains(text(),"You have successfully created new Annual report for '+data.clientname+'")]'}, ///span[contains(text(),"You have successfully created new Annual report for '+data.clientname+'")]
                messageErrorReport: {locateStrategy: 'xpath', selector: "//body//div[3]//div[@class='message-ctn error-ctn']/span[contains(text(),'Could not create report.There is already an Analytics task currently running. Please generate report after some time.')]"},
	
	            messageSuccessMonthlyReportNP: {locateStrategy: 'xpath', selector: "//div[@id='applicationHost']/div/section/div[1]/div[1]/div[@class='box-bdy']/div/div/div[@class='message-ctn success-ctn'][@data-bind='visible: showConfirm()'][@style='display: none;'][1]/span[text()='']"    }, ///span[contains(text(),"You have successfully created new monthly reports for '+data.clientname+'")]
	            messageSuccessAnnualReportNP: {locateStrategy: 'xpath', selector: "//div[@id='applicationHost']/div/section/div[1]/div[1]/div[@class='box-bdy']/div/div/div[@class='message-ctn success-ctn'][@data-bind='visible: showAnnualConfirm()'][@style='display: none;'][1]/span[text()='']"}, ///span[contains(text(),"You have successfully created new Annual report for '+data.clientname+'")]
	
	
	
	            titleAnnualReportPopUp: {selector: '#ui-dialog-title-3'},
                dropdownAnnualReportSelectIRSYear: {selector: '#irs-annual-report-year'},
                dropdownAnnualReportOption2016: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option)[2]'},
	
	            dropdownAnnualReportOptionYear2014: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option[@value="2014"])'},
	            dropdownAnnualReportOptionYear2015: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option[@value="2015"])'},
	            dropdownAnnualReportOptionYear2016: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option[@value="2016"])'},
	            dropdownAnnualReportOptionYear2017: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option[@value="2017"])'},
	            dropdownAnnualReportOptionYear2018: {locateStrategy: 'xpath', selector: '(//*[@id="irs-annual-report-year"]/..//option[@value="2018"])'},
	
	
	
	            buttonOKAnnualReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[2]'},
                buttonCancelAnnualReportPopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[2]'},

                reportReconciliation: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_Reconciliation_Monthly_JAN_2015")])[1]'},
                reportONG: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_ONG_Monthly_JAN_2015")])[1]'},
                reportNHR: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_NHR_Monthly_JAN_2015")])[1]'},
                reportAnnual: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"ANNUAL ESR REPORT_SLSDM_2016")])[1]'},
                feedbackFileMonthly: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_FF_ME")])[1]'},
                feedbackFileAnnual: {locateStrategy: 'xpath', selector: '(//*[contains(text(),"SLSDM_FF_ME_ANNUAL")])[1]'},

                linkCreateFile: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Create File")]'},
                titlePopupCreateFile: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Create Feedback File")]'},
                dropdownCreateFeedbackFileType: {selector: '#file-type'},
                dropdownCreateFeedbackFileOptionMonthly: {locateStrategy: 'xpath', selector: '//select[@id="file-type"]/..//option[contains(text(),"Monthly")]'},
                dropdownCreateFeedbackFileOptionAnnual: {locateStrategy: 'xpath', selector: '//select[@id="file-type"]/..//option[contains(text(),"Annual")]'},
                inputDateCreateFeedbackFile: {locateStrategy: 'xpath', selector: '//input[@id="js-create-feeback-file-datepicker"]'},
                buttonOKCreateFeedbackFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"OK")])[4]'},
                buttonCancelCreateFeedbackFilePopUp: {locateStrategy: 'xpath', selector: '(//button[contains(text(),"Cancel")])[7]'},

                tabAnnualFiles: {locateStrategy: 'xpath', selector: '//a[contains(text(),"Annual Files")]'},
				AnnualReports_tab:{locateStrategy:'xpath',selector:"//ul[contains(@class,'ui-widget-header')]/li/a[contains(text(),'Annual Reports')]"},
                datePicker: {locateStrategy: 'xpath', selector: '(//span[@class="ui-datepicker-month"])[1]/..'},
                datePickerMonth: {locateStrategy: 'xpath', selector: '(//span[@class="ui-datepicker-month"])[1]'},
                datePickerYear: {locateStrategy: 'xpath', selector: '(//span[@class="ui-datepicker-year"])[1]'},
                datePickerSelect31: {locateStrategy: 'xpath', selector: '(//a[contains(text(),"31")])[1]'},
                datePickerSelect01: {locateStrategy: 'xpath', selector: '(//a[@class="ui-state-default"])[1]'},
                buttonPreviousDatePicker: {locateStrategy: 'xpath', selector: '//span[contains(text(),"Prev")]'},

	            AddMoreMember_button:{locateStrategy:'xpath',selector:"//form[@id='reporting-year-form']/fieldset/ol[8]//a[contains(text(),'Add more members')]"},
	            Member3_Add:{locateStrategy:'xpath',selector:"//tbody[@id='tblMemberGroups']/tr[6]/td[2]/input[@id='txtMemName']"},
	            Member3Name_Add:{locateStrategy:'xpath', selector:"//tbody[@id='tblMemberGroups']/tr[6]/td[3]/input[@id='txtMemIdentifier']"},
                UploadStatus:{locateStrategy:'xpath', selector:"(//td[contains(text(),'UPLOADED')])[1]"},
	            Err_msg:{selector:"#lblmemgroup"},
	            Next_button:{selector:"button[title='Next']"},
                ReportingYear2016:{locateStrategy:'xpath', selector:"//table/tbody/tr[4]/td[2]/a"},
                Next1: {locateStrategy:'xpath', selector:"//form[@id='reporting-year-form']/fieldset[2]/ol/li/button[3]"},
                Next2:{locateStrategy:'xpath', selector:"//form[@id='pop-categories-form']/fieldset[1]/ol/li/button[3]"},
                Next3:{locateStrategy:'xpath', selector:"//form[@id='ongoing-lookback-form']/fieldset[3]/ol/li/button[3]"},
               // Next4:{locateStrategy:'xpath', selector:"(//button[@class='ui-btn col-2 green medium js-form-submit-btn'])[3]"},
               // Next5:{locateStrategy:'xpath', selector:"(//button[@class='ui-btn col-2 green medium js-form-submit-btn'])[3]"},
                Next6: {locateStrategy:'xpath', selector:"//form[@id='new-employee-lookback-form']/fieldset[3]/ol/li/button[3]"},
                Next9: {locateStrategy:'xpath', selector:"//form[@id='affdordability-conversions-form']/fieldset[2]/ol/li/button[3]"},
                Next12: {locateStrategy:'xpath', selector:"//form[@id='eligibility-feedback-file-form']/fieldset[3]/ol/li/button[3]"},

                Save: {locateStrategy:'xpath', selector:"//form[@id='eligibility-feedback-file-form']/fieldset[3]//button[contains(text(),'Save')]"},
                Confirm: {locateStrategy:'xpath', selector:"//span[contains(.,'You have successfully updated 2016 Reporting Year for YCCTST')]"},
                SelectYear : {locateStrategy:'xpath', selector:"//select[@id='bw-monthly-report-year']"},
                SelectYear2017: {locateStrategy:'xpath', selector:"//select[@id='bw-monthly-report-year']/option[3]"},
                SelectYear2016: {locateStrategy:'xpath', selector:"//select[@id='bw-monthly-report-year']/option[4]"},
                SelectMonth : {locateStrategy:'xpath', selector:"//select[@id='bw-monthly-report-month']"},
                SelectMonthDec : {locateStrategy:'xpath', selector:"//select[@id='bw-monthly-report-month']/option[13]"},
                CorrectionFileYes: {locateStrategy:'xpath', selector:"//input[@id='BW_Correction_Yes']"},
                UploadBWOk: {locateStrategy:'xpath', selector:"//li[@class='ui-form-action upload-bw-file']/button[2]"}









	            
            }
        },

    }
};




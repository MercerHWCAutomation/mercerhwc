module.exports = {

    TestingEnvironment: 'SANDBOX',
    ESR_ExecStatus:'Y',
    //TestingEnvironment: 'SANDBOX',
    //TestingEnvironment: 'STRESS',
    //TestingEnvironment: 'CITEST',
    //TestingEnvironment: 'PROD',

   NameofThebrowser: "chrome",
//	NameofThebrowser: "internet explorer",
     clientname:"",
    miniWait: 2000,
    shortWait: 5000,
    longWait: 20000,

    url:{
        QA:   'http://usfkl13as460v/ESR/#/',
        SANDBOX:  'http://usfkl13as460v/ESRUI/#/',
        STRESS:  'http://esr.mrshmc.com/#/home',
        CITEST: 'http://usfkl14as210v/ESR/#/',
        PROD: 'http://usfkl14as215v/ESR/#/'
    },
    Client: 'QA01CL',
	//MI_Client:'SLSDM',
	MI_Client:'YCLIFEPYR COBRA TEST',
	ReportingYear:2017,
	ValidateSpace:'tt sp',
	ValidateSpecialChar:'!@#$#$%^&*())*%#@!~`/*--++22155888899',
	Name: "UHC Minimum Essential Coverage",


    QAO1CL:{
        IRS1:2015,
        Year1:2014,
        Month1:'Oct',
	
	    IRS2: 2016,
	    Year2:2014,
	    Month2: 'Dec',
	
	    IRS3: 2016,
	    Year3:2015,
	    Month3: 'May',
	
	    IRS4: 2016,
	    Year4:2015,
	    Month4: 'Oct',
	
	    IRS5: 2016,
	    Year5:2015,
	    Month5: 'Dec',
        
        //Annual report for 2015
	
	    IRS6: 2017,
	    Year6:2016,
	    Month6: 'Oct',
	
	    IRS7: 2017,
	    Year7:2016,
	    Month7: 'Dec'
	
	    //Annual report for 2016
	
    }
};



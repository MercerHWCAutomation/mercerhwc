Feature: Database Validations in ESR Application

  @Sc101
Scenario: snapshot_id
  Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
  #When     The user navigates to specified "ESRTST" Page
  Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
 #Then     "ESRHomePage|titleMonthlyReportPopUp" page or message should be displayed
  Then  User checks rerun for all scenarios
  And      The User selects the IRS year "2015", year "2014" and month "Oct"
  Then     Confirmation message should be displayed successfully for Monthly report
  Then   User executes a new query
  Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
 # When     The user navigates to specified "ESRTST" Page
  Given    User Clicks on "ESRHomePage|buttonMonthlyReport" Link or Button
  And      The User selects the IRS year "2015", year "2014" and month "Oct"
  Then     Confirmation message should be displayed successfully for Monthly report
  Then   User executes a new query





 #  @Db1
 # Scenario: database validation
  # Then   User goes to database and executes the query





  @Sc96
  Scenario: To Upload inbound file for ESRTST client
   Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
   #When     The user navigates to specified "ESRTST" Page
   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
   Then User uploads the file for Jan
    Then User verifies for upload status
   Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
   #When     The user navigates to specified "ESRTST" Page
   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
   Then User uploads the file for Feb
    Then User verifies for upload status
   Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
   #When     The user navigates to specified "ESRTST" Page
   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
   Then User uploads the file for March
    Then User verifies for upload status
   Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
   #When     The user navigates to specified "ESRTST" Page
   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
   Then User uploads the file for April
    Then User verifies for upload status
   Given User Validates for Medical Plan Name





      @Sc72
      Scenario: YCCTST validation in IE
        Given    Once User is logged in to ESR application
        When     The user navigates to specified "YCLIFEPYR COBRA TEST" Page
        Then User Clicks on "ESRHomePage|tabReportingYears" Link or Button
        Then User Clicks on "ESRHomePage|ReportingYear2016" Link or Button
        Then User Clicks on "ESRHomePage|Next1" Link or Button
        Then User Clicks on "ESRHomePage|Next2" Link or Button
        Then User Clicks on "ESRHomePage|Next3" Link or Button
        Then User Clicks on "ESRHomePage|Next3" Link or Button
        Then User Clicks on "ESRHomePage|Next3" Link or Button
        Then User Clicks on "ESRHomePage|Next6" Link or Button
        Then User Clicks on "ESRHomePage|Next6" Link or Button
        Then User Clicks on "ESRHomePage|Next6" Link or Button
        Then User Clicks on "ESRHomePage|Next9" Link or Button
        Then User Clicks on "ESRHomePage|Next9" Link or Button
        Then User Clicks on "ESRHomePage|Next9" Link or Button
        Then User Clicks on "ESRHomePage|Next12" Link or Button
        Then User Clicks on "ESRHomePage|Next12" Link or Button
        Then User Clicks on "ESRHomePage|Save" Link or Button
       #Then "ESRHomePage|Confirm" page or message should be displayed


 @Sc88
 Scenario: Comparison of extracts for ESRTST
  Then User executes query for ssn 174704636
  Then User executes for esr_person_emplt
  Then User executes for esr_emp_service hours
  Then User executes for esr_emp_med_plan
  Then User executes for esr_person_member
  Then User executes for ESR_EMPLT_WORK_ST
  Then User executes for ESR_LOA
   Then User executes for ESR_PAY_RATE
Then User executes for ESR_EMPLOYMENTLOASTAGINGDATA
   Then User executes for ESR_EMP_SH_STAGINGDATA
   Then User executes for ESR_FOREIGN_SERV
   Then User executes for ESR_RATE_OF_PAY
   Then User executes for ESR_PERSON_W2_WAGE
   Then User executes for ESR_TRACK_INITIAL_HOURS
   Then User executes for ESR_PERSON_WAIT_PERIOD
   Then User executes for ESR_ACTIVE_COBRA_COVERAGE
   Then User executes for ESR_DATA_FEED
   Then User executes for ESR_JOBS
   Given    Once User is logged in to ESR application
   When     The user navigates to specified "QA01CL" Page
   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
   Then User uploads the inbound file for ESRTST
   Given    Once User is logged in to ESR application
   When     The user navigates to specified "QA01CL" Page
   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
   Then User uploads the inbound file for W2_ESRTST
   Then User executes query for ssn 174704636
   Then User executes for esr_person_emplt
   Then User executes for esr_emp_service hours
   Then User executes for esr_emp_med_plan
   Then User executes for esr_person_member
   Then User executes for ESR_EMPLT_WORK_ST
   Then User executes for ESR_LOA
   Then User executes for ESR_PAY_RATE
   Then User executes for ESR_EMPLOYMENTLOASTAGINGDATA
   Then User executes for ESR_EMP_SH_STAGINGDATA
   Then User executes for ESR_FOREIGN_SERV
   Then User executes for ESR_RATE_OF_PAY
   Then User executes for ESR_PERSON_W2_WAGE
   Then User executes for ESR_TRACK_INITIAL_HOURS
   Then User executes for ESR_PERSON_WAIT_PERIOD
   Then User executes for ESR_ACTIVE_COBRA_COVERAGE
   Then User executes for ESR_DATA_FEED
   Then User executes for ESR_JOBS

   @Sc88
   Scenario: Comparison of extracts for childr
     Then User executes query for childr ssn 174704636
     Then User executes for childr esr_person_emplt
     Then User executes for childr esr_loa
     Then User executes for childr ESR_EMP_SERVICE_HOURS
     Then User executes for childr ESR_EMP_MEDICAL_PLAN
     Then User executes for childr ESR_DATA_FEED
     Then User executes for childr ESR_PERSON_MEMBER
     Then User executes for childr ESR_EMPLT_WORK_ST
     Then User executes for childr ESR_LOA where person_id = 35778
     Then User executes for childr ESR_FPL
     Then User executes for childr ESR_PAY_RATE where person_id = 35778
     Then User executes for childr ESR_EMPLOYMENTLOASTAGINGDATA where SSN = 174704636
     Then User executes for childr ESR_JOBS order by 1 desc
     Then User executes for childr ESR_EMP_SH_STAGINGDATA where SSN = 174704636
     Then User executes for childr ESR_FOREIGN_SERV where person_id = 35778
     Then User executes for childr ESR_RATE_OF_PAY where person_id = 35778
     Then User executes for childr ESR_PERSON_W2_WAGE where person_id = 35778
     Then User executes for childr ESR_TRACK_INITIAL_HOURS where person_id = 35778
     Then User executes for childr ESR_PERSON_WAIT_PERIOD where person_id = 48799
     Then User executes for childr ESR_ACTIVE_COBRA_COVERAGE where person_id = 35778
     Given    Once User is logged in to ESR application
     When     The user navigates to specified "Children's Hospital of Philadelphia" Page
     Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
     Then User uploads the inbound file for CHILDR
     Given    Once User is logged in to ESR application
     When     The user navigates to specified "Children's Hospital of Philadelphia" Page
     Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
     Then User uploads the inbound file for W2_CHILDR
     Then User executes query for childr ssn 174704636
     Then User executes for childr esr_person_emplt
     Then User executes for childr esr_loa
     Then User executes for childr ESR_EMP_SERVICE_HOURS
     Then User executes for childr ESR_EMP_MEDICAL_PLAN
     Then User executes for childr ESR_DATA_FEED
     Then User executes for childr ESR_PERSON_MEMBER
     Then User executes for childr ESR_EMPLT_WORK_ST
     Then User executes for childr ESR_LOA where person_id = 35778
     Then User executes for childr ESR_FPL
     Then User executes for childr ESR_PAY_RATE where person_id = 35778
     Then User executes for childr ESR_EMPLOYMENTLOASTAGINGDATA where SSN = 174704636
     Then User executes for childr ESR_JOBS order by 1 desc
     Then User executes for childr ESR_EMP_SH_STAGINGDATA where SSN = 174704636
     Then User executes for childr ESR_FOREIGN_SERV where person_id = 35778
     Then User executes for childr ESR_RATE_OF_PAY where person_id = 35778
     Then User executes for childr ESR_PERSON_W2_WAGE where person_id = 35778
     Then User executes for childr ESR_TRACK_INITIAL_HOURS where person_id = 35778
     Then User executes for childr ESR_PERSON_WAIT_PERIOD where person_id = 48799
     Then User executes for childr ESR_ACTIVE_COBRA_COVERAGE where person_id = 35778

     @Sc99
   Scenario: To Validate column END_DTTM is not NULL for first row.
 #   Given    Once User is logged in to ESR application
 #      Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
 #   #When     The user navigates to specified "ESRTST" Page
 #   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
 #   Then User uploads the file for May
 #      Then User verifies for upload status
 #   Given    Once User is logged in to ESR application
 #      Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
 #  # When     The user navigates to specified "ESRTST" Page
 #   Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
 #   Then User uploads the file for May_2
 #      Then User verifies for upload status
     Then User execute query for esr data feed

     @Db6
     Scenario:To Validate column serv_end_dt should not be blank for first row
       Given    Once User is logged in to ESR application
       Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
       #When     The user navigates to specified "ESRTST" Page
       Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
       #Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
       And      The User selects the IRS year "2016" for monthly report
       Then     Confirmation message should be displayed successfully for Annual report
      # Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed
       Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
      # Then     User downloads annual report for the client "ESRTST" for "2016"
      # Then     User moves to the specifed folder "ANNUAL ESR REPORT_ESRTST_2016"
       When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button
       Given User executes query for serv_end_dt

       @Db7
       Scenario: To	Validate record in ESR_Annual_Report  should be available with latest job id from ESR_JOBS
         Given   User executes query for ESR_Annual_Report
         Then User executes ESR_Jobs query


@Sc14
Scenario: To Validate current_status column has value ‘TERM’
  Given      Once User is logged in to ESR application
  Given    User Clicks on "ESRHomePage|linkClientChildR" Link or Button
  #When     The user navigates to specified "Children's Hospital of Philadelphia" Page
  Given    User Clicks on "ESRHomePage|linkUploadBWFile" Link or Button
  Then User Clicks on "ESRHomePage|SelectYear" Link or Button
  Then User Clicks on "ESRHomePage|SelectYear2016" Link or Button
  Then User Clicks on "ESRHomePage|SelectMonth" Link or Button
  Then User Clicks on "ESRHomePage|SelectMonthDec" Link or Button
  Then User Clicks on "ESRHomePage|CorrectionFileYes" Link or Button
  Then User Clicks on "ESRHomePage|UploadBWOk" Link or Button
  Given   User executes query for esr_person_emplt

  @Sc36
  Scenario: To Validate MED_PLAN_END_DATE column has date ‘12/31/2016’
    Given      Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientChildR" Link or Button
    #When     The user navigates to specified "Children's Hospital of Philadelphia" Page
    Given    User Clicks on "ESRHomePage|linkUploadBWFile" Link or Button
    Then User Clicks on "ESRHomePage|SelectYear" Link or Button
    Then User Clicks on "ESRHomePage|SelectYear2016" Link or Button
    Then User Clicks on "ESRHomePage|SelectMonth" Link or Button
    Then User Clicks on "ESRHomePage|SelectMonthDec" Link or Button
    Then User Clicks on "ESRHomePage|CorrectionFileYes" Link or Button
    Then User Clicks on "ESRHomePage|UploadBWOk" Link or Button
    Given   User executes query for MED_PLAN_END_DATE


    @Sc52

    Scenario: To Validate serv_end_date column is not blank for first row
      Given    Once User is logged in to ESR application
      Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
      #When     The user navigates to specified "ESRTST" Page
      Given    User Clicks on "ESRHomePage|buttonAnnualReport" Link or Button
     # Then     "ESRHomePage|titleAnnualReportPopUp" page or message should be displayed
      And      The User selects the IRS year "2016" for monthly report
      Then     Confirmation message should be displayed successfully for Annual report
     # Then     "ESRHomePage|messageSuccessAnnualReport" page or message should be displayed
      Given    User Clicks on "ESRHomePage|AnnualReports_tab" Link or Button
      #Then     User downloads annual report for the client "ESRTST" for "2016"
     # Then     User moves to the specifed folder "ANNUAL ESR REPORT_ESRTST_2016"
      When     User Clicks on "ESRHomePage|pageHeadingHome" Link or Button
      Given   User executes query for serv_end_dt for esrtst client


  @Sc4
  Scenario: To Validate column END_DTTM is not NULL for first row.
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
    #When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
    Then User uploads the file for Nov
    Then User verifies for upload status
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
    #When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
    Then User uploads the file for Dec
    Then User verifies for upload status
    Given    Once User is logged in to ESR application
    Given    User Clicks on "ESRHomePage|linkClientESRTST" Link or Button
    #When     The user navigates to specified "ESRTST" Page
    Given    User Clicks on "ESRHomePage|linkUploadFile" Link or Button
    Then User uploads the file for Dec corr
    Then User verifies for upload status
    Then User checks if serv_end_dt is Nov 30


@Sc90
Scenario: To Validate results are displayed for CHILDR client
  Given      Once User is logged in to ESR application
  Given    User Clicks on "ESRHomePage|linkClientChildR" Link or Button
 # When     The user navigates to specified "Children's Hospital of Philadelphia" Page
  Given    User Clicks on "ESRHomePage|linkUploadBWFile" Link or Button
  Then User Clicks on "ESRHomePage|SelectYear" Link or Button
  Then User Clicks on "ESRHomePage|SelectYear2017" Link or Button
  Then User Clicks on "ESRHomePage|SelectMonth" Link or Button
  Then User Clicks on "ESRHomePage|SelectMonthDec" Link or Button
  Then User Clicks on "ESRHomePage|CorrectionFileYes" Link or Button
  Then User Clicks on "ESRHomePage|UploadBWOk" Link or Button
  Given   User updates company enrollment data
  Given      Once User is logged in to ESR application
  Given    User Clicks on "ESRHomePage|linkClientChildR" Link or Button
 # When     The user navigates to specified "Children's Hospital of Philadelphia" Page
  Given    User Clicks on "ESRHomePage|linkUploadBWFile" Link or Button
  Then User Clicks on "ESRHomePage|SelectYear" Link or Button
  Then User Clicks on "ESRHomePage|SelectYear2017" Link or Button
  Then User Clicks on "ESRHomePage|SelectMonth" Link or Button
  Then User Clicks on "ESRHomePage|SelectMonthDec" Link or Button
  Then User Clicks on "ESRHomePage|CorrectionFileYes" Link or Button
  Then User Clicks on "ESRHomePage|UploadBWOk" Link or Button
  Then Validate column END_DTTM is not NULL for first row
  Then Validate results are displayed and Result should not be NULL















































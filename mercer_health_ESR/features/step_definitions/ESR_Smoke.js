﻿var ESRutility=require('./ESR_Reusables');
var action = require('./../../../Keywords');
var data = require('../../test_data/ESR_TestData.js');
var robot123 = require('robotjs');
var Objects=require(__dirname + '/../../objects/ESR_Locators.js');
var oracledb = require('oracledb');
var url = require('url');
var http = require('http');
var moment=require('moment');
var httpPort = 7000;
var jobid1,jobid2;
var dbConfig = require('./dbconfig.js');
var cl;
//var excel_compare = require("./samp");
const osHomedir = require('os-homedir');
var path = osHomedir();
var glob = require("glob")
var fs=require('fs');
var Excel = require('exceljs');
var homepath = path.replace(new RegExp('\\' + path.sep, 'g'), '/');
var LocationExcelNew1,LocationExcelNew2;
//var homepath = 'C:/Users/Hemant-Sagar/Downloads';
//var homepath2 = 'C:\\Users\\Hemant-Sagar\\Downloads\\Post';

module.exports = function () {
    this.Given(/^Once User is logged in to ESR application$/, function (browser) {
        var URL;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        URL = data.url[execEnv];

        var locators = browser.page.ESR_Locators();
        page = locators.section;
        ESRHomePage = page.ESRHomePage;

        browser.maximizeWindow()
            .deleteCookies(function () {
                browser.pause(data.longWait)
                    .url(URL)
                    .waitForElementPresent('body', data.longWait, function () {
                        action.initializePageObjects(browser, function () {
                            action.reloadPage(function () {
                                ESRHomePage.waitForElementVisible('@pageTitle', data.longWait);
                                ESRHomePage.waitForElementVisible('@pageHeadingHome', data.longWait);
                                ESRHomePage.waitForElementVisible('@linkClientSLS', data.longWait);
                            });
                        });
                    });


            })

    });
    this.When(/^User Clicks on "([^"]*)" Link or Button$/, function (browser, locator) {
        action.initializePageObjects(browser, function () {
            action.isDisplayed(locator, function () {
                action.performClick(locator, function () {
                    browser.pause(data.shortWait);
                });
            });

        });
    });

    this.Then(/^User should see "([^"]*)" message is displayed$/, function (browser, locator) {
        action.initializePageObjects(browser, function () {
            action.elementDisplayedStatus('ESRHomePage|messageErrorReport', function (status) {
                if (status) {
                    console.log("Try again later");

                }
                else {
                    action.waitTillElementLoads('ESRHomePage|titleMonthlyReportPopUp', function () {
                        //browser.pause(12000);
                        action.isDisplayed('ESRHomePage|titleMonthlyReportPopUp', function () {

                        })
                    })
                }


                //	browser.timeoutsImplicitWait(data.shortWait);
            });
        });
    });
    this.Then(/^User checks rerun for all scenarios$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.isDisplayed('ESRHomePage|Rerun', function () {
                action.performClick('ESRHomePage|Rerun', function () {
                });
            });
        });
    });
    this.Then(/^Confirmation message should be displayed successfully for Annual report$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.elementDisplayedStatus('ESRHomePage|messageErrorReport', function (status) {
                if (status) {
                    console.log("Try again later");

                }
                else {
                    action.waitTillElementLoads('ESRHomePage|messageSuccessAnnualReport', function () {
                        browser.pause(12000);
                        action.isDisplayed('ESRHomePage|messageSuccessAnnualReport', function () {

                        })
                    })
                }


                //	browser.timeoutsImplicitWait(data.shortWait);
            });
        });
    });
    this.Then(/^Confirmation message should be displayed successfully for Monthly report$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.elementDisplayedStatus('ESRHomePage|messageErrorReport', function (status) {
                if (status) {
                    console.log("Try again later");

                }
                else {
                    action.waitTillElementLoads('ESRHomePage|messageSuccessMonthlyReport', function () {
                        //browser.pause(20000);
                        action.isDisplayed('ESRHomePage|messageSuccessMonthlyReport', function () {

                        })
                    })
                }
                /*action.elementDisplayedStatus('ESRHomePage|messageErrorReport',function (status) {
                    if(status)
                    {
                        console.log("Try again later");

                    }
                    else
                    {
                        action.waitTillConfirmationLoads('ESRHomePage|messageSuccessMonthlyReportNP', function () {
                            browser.pause(50000);
                            action.isDisplayed('ESRHomePage|messageSuccessMonthlyReport', function () {

                            })
                        })
                    }*/


                //	browser.timeoutsImplicitWait(data.shortWait);
            });
        });
    });
    this.Given(/^User uploads the inbound file$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if (execEnv.toUpperCase() == "QA") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }
        else if (execEnv.toUpperCase() == "SANDBOX") {
            action.UploadFile("C:/Users/sumithra-prabakaran/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "STRESS") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "CITEST") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "PROD") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        /*    action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi",function (result) {
         console.log("File Uploaded"+result);
         action.performClick('ESRHomePage|buttonFileUpload',function () {
         //browser.pause(data.miniWait);
         browser.timeoutsImplicitWait(data.shortWait);
         });
         });       */

        browser.timeoutsImplicitWait(data.shortWait);

    });
    this.Given(/^User uploads the inbound file having increase in column length for Medical plan name$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if (execEnv.toUpperCase() == "QA") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }
        else if (execEnv.toUpperCase() == "SANDBOX") {
            browser.setValue('input[type="file"]', require('path').resolve('C:\\Users\\Hemant-Sagar\\Desktop\\QA01CL_Medical Plan name 50.csv'))
            action.performClick('ESRHomePage|buttonFileUpload', function () {
                browser.pause(data.longWait);
                browser.timeoutsImplicitWait(data.shortWait);
            });


            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "STRESS") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "CITEST") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "PROD") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        /*    action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi",function (result) {
         console.log("File Uploaded"+result);
         action.performClick('ESRHomePage|buttonFileUpload',function () {
         //browser.pause(data.miniWait);
         browser.timeoutsImplicitWait(data.shortWait);
         });
         });       */

        browser.timeoutsImplicitWait(data.shortWait);

    });
    this.Given(/^User uploads the inbound file having LCMP Plan start and end dates equal$/, function (browser) {

        var execEnv = data["TestingEnvironment"];

        if (execEnv.toUpperCase() == "QA") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }
        else if (execEnv.toUpperCase() == "SANDBOX") {
            browser.setValue('input[type="file"]', require('path').resolve('C:\\Users\\Hemant-Sagar\\Desktop\\QA01CL_LCMP_Same dates.csv'))
            action.performClick('ESRHomePage|buttonFileUpload', function () {
                browser.pause(data.longWait);
                browser.timeoutsImplicitWait(data.shortWait);
            });


            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "STRESS") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "CITEST") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        else if (execEnv.toUpperCase() == "PROD") {
            action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/ESRTST_Sandbox", function (result) {
                console.log("File Uploaded" + result);
                action.performClick('ESRHomePage|buttonFileUpload', function () {
                    browser.pause(data.longWait);
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });

            browser.timeoutsImplicitWait(data.longWait);

        }

        /*    action.UploadFile("C:/Users/venkat-ramanan-p/Desktop/US_Ben_Smoke_1/ESR/SLSDM_inbound_QAi",function (result) {
         console.log("File Uploaded"+result);
         action.performClick('ESRHomePage|buttonFileUpload',function () {
         //browser.pause(data.miniWait);
         browser.timeoutsImplicitWait(data.shortWait);
         });
         });       */

        browser.timeoutsImplicitWait(data.shortWait);

    });

    this.Given(/^User uploads the BW file$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick('ESRHomePage|dropdownSelectYear', function () {
                action.performClick('ESRHomePage|dropdownOption2015SelectYear', function () {
                    action.performClick('ESRHomePage|dropdownSelectMonth', function () {
                        action.performClick('ESRHomePage|dropdownOptionJanSelectMonth', function () {
                            action.performClick('ESRHomePage|buttonRadioYesUploadBWFilePopUp', function () {
                                browser.timeoutsImplicitWait(data.shortWait);
                                action.performClick('ESRHomePage|buttonOKUploadBWFilePopUp', function () {
                                    /*action.waitTillElementNotDisplayed('ESRHomePage|buttonCancelUploadBWFilePopUp',function () {
                                     browser.timeoutsImplicitWait(data.shortWait);
                                 
                                     });  */
                                    return true;
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    this.Given(/^User generates the monthly report$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick('ESRHomePage|dropdownMonthlyReportSelectIRSYear', function () {
                browser.timeoutsImplicitWait(data.shortWait);
                action.performClick('ESRHomePage|dropdownMonthlyReportOptionIRS2016', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                    action.performClick('ESRHomePage|dropdownMonthlyReportSelectYear', function () {
                        browser.timeoutsImplicitWait(data.shortWait);
                        action.performClick('ESRHomePage|dropdownMonthlyReportOptionYear2015', function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            action.performClick('ESRHomePage|dropdownMonthlyReportSelectMonth', function () {
                                browser.timeoutsImplicitWait(data.shortWait);
                                action.performClick('ESRHomePage|dropdownMonthlyReportOptionJan', function () {
                                    browser.timeoutsImplicitWait(data.shortWait);
                                    action.performClick('ESRHomePage|buttonOKMonthlyReportPopUp', function () {
                                        browser.timeoutsImplicitWait(data.shortWait);
                                        action.waitTillElementLoads('ESRHomePage|messageSuccessMonthlyReport', function () {
                                            browser.timeoutsImplicitWait(data.shortWait);
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    this.Given(/^User generates the annual report$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick('ESRHomePage|dropdownAnnualReportSelectIRSYear', function () {
                browser.timeoutsImplicitWait(data.shortWait);
                action.performClick('ESRHomePage|dropdownAnnualReportOption2016', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                    action.performClick('ESRHomePage|buttonOKAnnualReportPopUp', function () {
                        browser.timeoutsImplicitWait(data.shortWait);

                    });
                });
            });
        });
    });

    this.Given(/^User generates the monthly feedback file$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick('ESRHomePage|dropdownCreateFeedbackFileType', function () {
                browser.timeoutsImplicitWait(data.shortWait);
                action.performClick('ESRHomePage|dropdownCreateFeedbackFileOptionMonthly', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                    // --------- Insert a function to navigate to 31/10/2015
                    action.performClick('ESRHomePage|inputDateCreateFeedbackFile', function () {
                        // action.navigateTillDisplayed('ESRHomePage|datePicker','ESRHomePage|buttonPreviousDatePicker',function () {
//                        browser.pause(3000);
                        browser.timeoutsImplicitWait(data.shortWait);
                        action.performClick('ESRHomePage|datePickerSelect01', function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            action.performClick('ESRHomePage|buttonOKCreateFeedbackFilePopUp', function () {
                                browser.timeoutsImplicitWait(data.shortWait);
                                browser.pause(data.shortWait);
                            });
                        });
                        //   });
                    });
                });
            });
        });
    });

    this.Given(/^User generates the annual feedback file$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick('ESRHomePage|dropdownCreateFeedbackFileType', function () {
                browser.timeoutsImplicitWait(data.miniWait);
                action.performClick('ESRHomePage|dropdownCreateFeedbackFileOptionAnnual', function () {
                    browser.timeoutsImplicitWait(data.miniWait);
                    //   action.setText('ESRHomePage|inputDateCreateFeedbackFile',"01-Jan-2015", function () {
                    action.performClick('ESRHomePage|inputDateCreateFeedbackFile', function () {
                        action.performClick('ESRHomePage|datePickerSelect01', function () {
                            browser.timeoutsImplicitWait(data.shortWait);
                            action.performClick('ESRHomePage|buttonOKCreateFeedbackFilePopUp', function () {
                                browser.timeoutsImplicitWait(data.shortWait);
                                browser.pause(data.shortWait);
                            });
                        });
                    });
                });
            });
        });
    });

    this.Given(/^User navigates to Data_Files Tab$/, function (browser) {
        action.initializePageObjects(browser, function () {
            browser.pause(data.miniWait);
            action.performClick('ESRHomePage|tabDataFiles', function () {
                browser.pause(data.shortWait);
                //browser.timeoutsImplicitWait(data.shortWait);
                action.isDisplayed('ESRHomePage|titleDataFiles', function () {
                    browser.timeoutsImplicitWait(data.miniWait);

                });
            });
        });

    });
    this.Given(/^User navigates to Reports Tab$/, function (browser) {
        action.initializePageObjects(browser, function () {
            browser.pause(data.miniWait);
            action.isDisplayed('ESRHomePage|tabReports', function () {
                action.performClick('ESRHomePage|tabReports', function () {
                    browser.pause(data.shortWait);
                    //browser.timeoutsImplicitWait(data.shortWait);

                });
            });

        });

    });

    this.Given(/^User navigates to Feedback_Files Tab$/, function (browser) {
        action.initializePageObjects(browser, function () {
            browser.pause(data.shortWait);
            action.performClick('ESRHomePage|tabFeedbackFiles', function () {
                browser.pause(data.shortWait);
                action.isDisplayed('ESRHomePage|titleFeedbackFiles', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                });
            });
        });
    });

    this.Given(/^The user navigates to Client Page$/, function (browser) {
        action.initializePageObjects(browser, function () {
            var execEnv = data["TestingEnvironment"];

            if (execEnv.toUpperCase() == "QA") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "SANDBOX") {
                action.performClick('ESRHomePage|linkClientESRTST', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "STRESS") {
                action.performClick('ESRHomePage|linkClientAOLBAA', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "CITEST") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "PROD") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
        });
    });
    this.Given(/^The user navigates to specified "([^"]*)" Page$/, function (browser, ClientData) {

        action.initializePageObjects(browser, function () {
            var cl = "";
            if (ClientData == 'Valley Crest Companies') {
                cl = "VCTC01"
            }
            else if (ClientData == 'YCLIFEPYR COBRA TEST') {
                cl = "YCCTST";
            }
            else {
                cl = ClientData;
            }
            data.clientname = cl;
            var execEnv = data["TestingEnvironment"];
            if (execEnv.toUpperCase() == "QA") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "SANDBOX") {
                browser.useXpath().waitForElementPresent('//a[contains(text(),"' + ClientData + '")]', data.longWait).click('//a[contains(text(),"' + ClientData + '")]', function () {
                    browser.timeoutsImplicitWait(data.shortWait);

                }).useCss().waitForElementPresent('body', data.longWait);
            }
            else if (execEnv.toUpperCase() == "STRESS") {
                action.performClick('ESRHomePage|linkClientAOLBAA', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "CITEST") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "PROD") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
        });
    });
    this.When(/^The User selects the IRS year "([^"]*)", year "([^"]*)" and month "([^"]*)"$/, function (browser, IRS, year, month) {
        ESRutility.MonthlyReportGeneration(browser, IRS, year, month, function () {


        });

    });
    this.When(/^The User selects the IRS year "([^"]*)" for monthly report$/, function (browser, year) {
        ESRutility.AnnualReportGeneration(browser, year, function () {


        });

    });

    this.Given(/^The corresponding page title is displayed$/, function (browser) {
        action.initializePageObjects(browser, function () {
            var execEnv = data["TestingEnvironment"];

            if (execEnv.toUpperCase() == "QA") {
                action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "SANDBOX") {
                action.isDisplayed('ESRHomePage|titleESRTSTPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "STRESS") {
                action.isDisplayed('ESRHomePage|titleAOLBAAPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "CITEST") {
                action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "PROD") {
                action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
        });
    });

    this.Given(/^The user navigates to the appropriate Client Page$/, function (browser) {
        action.initializePageObjects(browser, function () {
            var execEnv = data["TestingEnvironment"];

            if (execEnv.toUpperCase() == "QA") {
                action.performClick('ESRHomePage|linkClientQAACME', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "SANDBOX") {
                action.performClick('ESRHomePage|linkClientChildR', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "STRESS") {
                action.performClick('ESRHomePage|linkClientACISTL', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "CITEST") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "PROD") {
                action.performClick('ESRHomePage|linkClientSLS', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
        });
    });

    this.Given(/^The page title is displayed$/, function (browser) {
        action.initializePageObjects(browser, function () {
            var execEnv = data["TestingEnvironment"];

            if (execEnv.toUpperCase() == "QA") {
                action.isDisplayed('ESRHomePage|titleQAACMEPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "SANDBOX") {
                action.isDisplayed('ESRHomePage|titleChildRPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "STRESS") {
                action.isDisplayed('ESRHomePage|titleACISTLPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "CITEST") {
                action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
            else if (execEnv.toUpperCase() == "PROD") {
                action.isDisplayed('ESRHomePage|titleSLSDMPage', function () {
                    browser.timeoutsImplicitWait(data.shortWait);
                })
            }
        });
    });

    this.When(/^User Navigates to Reporting years tab$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.isDisplayed('ESRHomePage|tabReportingYears', function () {
                action.performClick('ESRHomePage|tabReportingYears', function () {

                })
            })
        });
    });
    this.Then(/^User should be on Reporting years page$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.isDisplayed('ESRHomePage|titleReportingYears', function () {

            })
        });
    });
    this.When(/^User selects the particular year on Reporting years page$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.isDisplayed('ESRHomePage|ReportingYear', function () {
                action.performClick('ESRHomePage|ReportingYear2016', function () {

                })

            })
        });
    });
    this.Then(/^User should be able to see Member and and their identifying details$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.isDisplayed('ESRHomePage|AddMoreMember_button', function () {

            })
        });
    });
    this.When(/^User adds a new member$/, function (browser) {
       // browser.timeoutsImplicitWait(20000);
        browser.pause(20000);

        browser.useXpath().click('//form[@id=\'reporting-year-form\']/fieldset/ol[8]//a[contains(text(),\'Add more members\')]');
        //ESRHomePage.waitForElementVisible('@AddMoreMember_button',data.shortWait)
        //    .click('@AddMoreMember_button');
        ESRHomePage.waitForElementVisible('@Member3_Add',data.longWait)
            .setValue('@Member3_Add',data.ValidateSpace);
        ESRHomePage.waitForElementVisible('@Member3Name_Add',data.longWait)
            .setValue('@Member3Name_Add',data.ValidateSpace);


      //  action.initializePageObjects(browser, function () {
      //      action.isDisplayed('ESRHomePage|AddMoreMember_button', function () {
      //          action.performClick('ESRHomePage|AddMoreMember_button', function () {

      //              action.isDisplayed('ESRHomePage|Member3_Add', function () {
      //              })
      //          })
      //      })
      //  });
    });


    // });
    this.When(/^Enter space on the Member Identifier field$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.setText('ESRHomePage|Member3_Add', data.ValidateSpace);
            action.setText('ESRHomePage|Member3Name_Add', data.ValidateSpace);
        });
    });

    this.Then(/^User should be thrown an error$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.isDisplayed('ESRHomePage|Next_button', function () {
                action.performClick('ESRHomePage|Next_button', function () {
                    action.isDisplayed('ESRHomePage|Next_button', function () {
                        action.verifyText('ESRHomePage|Err_msg', 'Please remove space(s) from member identifier.')
                    })

                })
            })
        });
    });

    this.When(/^User edits the same field with all special characters$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.setText('ESRHomePage|Member3_Add', data.ValidateSpecialChar);
            action.setText('ESRHomePage|Member3Name_Add', data.ValidateSpecialChar);
            action.isDisplayed('ESRHomePage|Next_button', function () {

            })


        });
    });
    this.Then(/^User should be thrown an error for special char$/, function (browser) {
        action.initializePageObjects(browser, function () {
            action.performClick('ESRHomePage|Next_button', function () {
                action.isDisplayed('ESRHomePage|Next_button', function () {
                    action.verifyText('ESRHomePage|Err_msg', 'Please remove special character(s).')
                })

            })

        });

    });
    this.Then(/^User downloads "([^"]*)" for the client "([^"]*)" for "([^"]*)"$/, function (browser, report, client, date) {
        //action.isDisplayed('ESRHomePage|tabReports',function () {
        //action.performClick('ESRHomePage|tabReports', function () {
        action.initializePageObjects(browser, function () {
            action.elementDisplayedStatus('ESRHomePage|messageErrorReport', function (status) {

                if (status) {
                    console.log("Try again later");

                }
                else {
                    browser.waitForElementPresent('body', data.longWait);
                    browser.elements('xpath', "//td/a/span[contains(text(),'" + client + "_" + report + "_" + date + "')]", function (webElementsArray) {
                        count = webElementsArray.value.length;
                        //console.log("outside similar excel count: "+count);
                        webElementsArray.value.forEach(function (webEle) {

                            //console.log("looping"+count);
                            if (count == webElementsArray.value.length || count == (webElementsArray.value.length - 1)) {
                                browser.elementIdClick(webEle.ELEMENT, function () {

                                    if (data.NameofThebrowser == "internet explorer") {
                                        robot123.setKeyboardDelay(data.shortWait);
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");
                                        robot123.setKeyboardDelay(data.shortWait);
                                        robot123.keyTap("enter");
                                        robot123.setKeyboardDelay(data.shortWait);
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");

                                        robot123.keyTap("enter");
                                        robot123.setKeyboardDelay(data.shortWait);
                                    }

                                });

                                browser.pause(7000)
                            }
                            count--;

                        });
                    });

                }
            });
        });

    });
    this.Then(/^User downloads annual report for the client "([^"]*)" for "([^"]*)"$/, function (browser, client, year) {
        action.initializePageObjects(browser, function () {
            action.elementDisplayedStatus('ESRHomePage|messageErrorReport', function (status) {

                if (status) {
                    console.log("Try again later");

                }
                else {
                    browser.waitForElementPresent('body', data.longWait); //ANNUAL ESR REPORT_QA01CL_2016
                    browser.elements('xpath', "//td/a/span[contains(text(),'ANNUAL ESR REPORT_" + client + "_" + year + "')]", function (webElementsArray) {
                        count = webElementsArray.value.length;
                        //console.log(count+ "is the count");
                        //console.log("outside similar excel count: "+count);
                        webElementsArray.value.forEach(function (webEle) {

                            //console.log("looping"+count);
                            if (count == webElementsArray.value.length || count == (webElementsArray.value.length - 1)) {
                                browser.elementIdClick(webEle.ELEMENT, function () {
                                    console.log("Clicked Excel");
                                    //browser.pause(7000);

                                    if (data.NameofThebrowser == "internet explorer") {
                                        robot123.setKeyboardDelay(data.shortWait);
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");
                                        robot123.setKeyboardDelay(data.shortWait);
                                        robot123.keyTap("enter");
                                        robot123.setKeyboardDelay(data.shortWait);
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");
                                        robot123.keyTap("tab");

                                        robot123.keyTap("enter");
                                        robot123.setKeyboardDelay(data.shortWait);
                                    }

                                });

                                //console.log("inside"+count);
                                //console.log(webEle.ELEMENT);
                                browser.pause(7000)
                            }
                            count--;

                        });
                    });

                }
            });
        });

    });

    this.Given(/^User compares "([^"]*)" for the client "([^"]*)" for "([^"]*)"$/, function (browser) {

    })
    this.Then(/^User lists the differences for "([^"]*)"$/, function (browser, rep) {
        action.initializePageObjects(browser, function () {
            var file1_loc, file2_loc;
            var options;

            glob(homepath + "/Downloads/ESR/" + rep + "/*.xlsx", function (er, files) {
                console.log(files);
                file1_loc = files[0];
                file2_loc = files[1];
                options = {

                    file1: file1_loc, // file1 is the main excel to compair with
                    file2: file2_loc, // file2 is the file for compair


                };
                action.CompareExcelData(options, rep);

            });
        });
    });
    this.Then(/^User moves to the specifed folder "([^"]*)"$/, function (browser, rep) {
        action.initializePageObjects(browser, function () {
            var location_excel1, location_excel2;
            var excel_name1, excel_name2;
            var mkdirp = require('mkdirp');
            var fname = rep + new Date().getMonth() + new Date().getFullYear() // + "_" + new Date().getTime();
           // var fname = rep + new Date().getMonth() + new Date().getFullYear();

            //changed----------------------------
           mkdirp(homepath + "/Downloads/ESR" + fname, function (err) {
          //  mkdirp(homepath + "/ESR" + fname, function (err) {
           //console.log("homepath is:"+homepath)

                if (err) {
                    console.error("err is " + err);
                }
                else {
                    //console.log('Dir created!');
                    glob(homepath + "/Downloads/*.xlsx", function (er, files) {
                   // glob(homepath + "/ESR/*.xlsx", function (er, files) {

                        location_excel1 = files[0];
                        location_excel2 = files[1];
                        console.log(location_excel1);
                        console.log(location_excel2);
                        //var excel_name1,excel_name2;
                        excel_name1 = location_excel1.split('/');
                        excel_name2 = location_excel2.split('/');
                        //console.log(excel_name1[4]);
                        //console.log(excel_name2[4]);
                        //----------------------

                        var NewPath1= excel_name1[4].split('_');
                        console.log(NewPath1[0]+"_"+NewPath1[1]+"_"+NewPath1[2]+"_"+NewPath1[3]+"_"+NewPath1[4]+".xlsx");
                        LocationExcelNew1 = NewPath1[0]+"_"+NewPath1[1]+"_"+NewPath1[2]+"_"+NewPath1[3]+"_"+NewPath1[4]+".xlsx";

                        var NewPath2= excel_name2[4].split('_');
                        console.log(NewPath2[0]+"_"+NewPath2[1]+"_"+NewPath2[2]+"_"+NewPath2[3]+"_"+NewPath2[4]+".xlsx");
                        LocationExcelNew2 = NewPath2[0]+"_"+NewPath2[1]+"_"+NewPath2[2]+"_"+NewPath2[3]+"_"+NewPath2[4]+".xlsx";

                        //-------------------
                        console.log("Files are :" + location_excel1 + " and " + location_excel2);
                        moved(location_excel1,location_excel2)


                    });
                }

            //changed----------------------------------



                function moved(location_excel1, location_excel2) {
         //--OR          // move(location_excel1, homepath + "/Downloads/ESR/" + fname + "/" + excel_name1[4], function () {
                        move(location_excel1, homepath + "/Downloads/ESR/Pre/" +LocationExcelNew1, function () {
                   // move(location_excel1, homepath + "/ESR/Pre/" + fname + "/" + excel_name1[4], function () {
                   	console.log("Moved the files successfully!!"+homepath + "/Downloads/ESR/Pre/" +LocationExcelNew1)
                      //  move(location_excel1, homepath + "/Downloads/ESR/" + fname + "/" + excel_name1[4], function () {
         //--OR               move(location_excel2, homepath + "/Downloads/ESR/" + fname + "/" + excel_name2[4], function () {
                            move(location_excel2, homepath + "/Downloads/ESR/Post/"+ LocationExcelNew2, function () {

                            console.log("Moved the files successfully!!" + homepath + "/Downloads/ESR/Post/" +LocationExcelNew2)
                        });
                    });

                }

                function move(oldPath, newPath, callback) {

                    fs.rename(oldPath, newPath, function (err) {
                        if (err) {
                            if (err.code === 'EXDEV') {
                                copy();
                            } else {
                                callback(err);
                            }
                            return;
                        }
                        callback();
                    });

                    function copy() {
                        var readStream = fs.createReadStream(oldPath);
                        var writeStream = fs.createWriteStream(newPath);

                        readStream.on('error', callback);
                        writeStream.on('error', callback);

                        readStream.on('close', function () {
                            fs.unlink(oldPath, callback);
                        });

                        readStream.pipe(writeStream);
                    }
                }
            });
        });
    });


    this.Given(/^User lists the differences for "([^"]*)" annual report for Sheet "([^"]*)"$/, function (browser, rep, SheetNo) {
        action.initializePageObjects(browser, function () {
            action.CompareExcelAnnualData(rep, SheetNo, function () {

            })
        });
    });

    this.Then(/^User verifies for upload status$/, function (browser) {
        action.initializePageObjects(browser, function () {

            action.isDisplayed('ESRHomePage|UploadStatus', function () {

            });

        });


    });


    this.Then(/^User uploads the file for Jan$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/Jan-16.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });


    this.Then(/^User uploads the file for Feb$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/Feb16.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });


    this.Then(/^User uploads the file for March$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/Mar-16.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the file for April$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/Apr-16.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the file for May$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/May_1.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the file for May_2$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/May_2.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the file for Nov$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/Nov-16.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the file for Dec$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/Dec-16.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the file for Dec corr$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/Dec-16_Corr.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the inbound file for CHILDR$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/CHILDR.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the inbound file for W2_CHILDR$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/W2_CHILDR.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });




    this.Given(/^User goes to database and executes the query$/, function () {

        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    'select * from owner_esr_qa.esr_jobs order by 1 desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });

    this.Given(/^User Validates for Medical Plan Name$/, function () {

        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    'select MED_PLAN_NAME from owner_esr_qa.esr_emp_medical_plan where person_id =  1626413', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);

                        //var Metadata = result.metaData;
                        //var Rows = result.rows;

                        //console.log(Metadata);
                        //console.log(Rows);
                        var Metadata = JSON.stringify(result.metaData);      //convert the result in string format
                        var Rows = JSON.stringify(result.rows);

                        //console.log(Metadata.split("name"));              //to check the string function is working in split
                        //console.log(Rows.split(","));

                        var Array1 = Metadata.split("name");
                        var Array2 = Rows.split(",");

                        // console.log(Array1[3]);
                        console.log(Array2[0]);

                        if (Array2[0] = "UHC Minimum Essential Coverage") {
                            console.log("Verified");
                        }

                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });


    this.Then(/^User executes a new query$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    "select snapshot_id  from owner_esr_qa.ESR_PERSON_MEAS_PD_SNAPSHOT where CLIENT_CD = 'ESRTST' order by snapshot_id desc", {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        doRelease(connection);
                    });
            });


        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });


    this.Then(/^User executes query for ssn 174704636$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_PERSON where SSN = 174704636', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });


    this.Then(/^User executes for esr_person_emplt$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_PERSON_EMPLT where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }
    });

    this.Then(/^User executes for esr_emp_service hours$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_EMP_SERVICE_HOURS where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for esr_emp_med_plan$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_EMP_MEDICAL_PLAN where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });



    this.Then(/^User executes for esr_person_member$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_PERSON_MEMBER where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });


    this.Then(/^User executes for ESR_EMPLT_WORK_ST$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_EMPLT_WORK_ST where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });


    this.Then(/^User executes for ESR_LOA$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_LOA where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_PAY_RATE$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_PAY_RATE where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_EMPLOYMENTLOASTAGINGDATA$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_EMPLOYMENTLOASTAGINGDATA where SSN = 174704636', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_EMP_SH_STAGINGDATA$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_EMP_SH_STAGINGDATA where SSN = 174704636', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_FOREIGN_SERV$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_FOREIGN_SERV where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });


    this.Then(/^User executes for ESR_RATE_OF_PAY$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_RATE_OF_PAY where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_PERSON_W2_WAGE$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_PERSON_W2_WAGE where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_TRACK_INITIAL_HOURS$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_TRACK_INITIAL_HOURS where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });


    this.Then(/^User executes for ESR_PERSON_WAIT_PERIOD$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_PERSON_WAIT_PERIOD where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_ACTIVE_COBRA_COVERAGE$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_ACTIVE_COBRA_COVERAGE where person_id = 1625789', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_DATA_FEED$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_DATA_FEED order by 1 desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for ESR_JOBS$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM OWNER_ESR_QA.ESR_JOBS order by 1 desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });


    this.Then(/^User uploads the inbound file for ESRTST$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/ESRTST.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });

    this.Then(/^User uploads the inbound file for W2_ESRTST$/, function (browser) {

        browser.setValue('input[type="file"]', require('path').resolve('C:/Users/Hemant-Sagar/Desktop/W2_ESRTST.csv'));
        action.performClick('ESRHomePage|buttonFileUpload', function () {
            browser.pause(data.longWait);
            browser.timeoutsImplicitWait(data.shortWait);

        });
    });
    this.Then(/^User execute query for esr data feed$/, function () {

        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'select END_DTTM  from owner_esr_qa.esr_data_feed order by data_feed_id desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        var ws=

                        //console.log(url);
                        doRelease(connection);
                    });
            });


        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });


    this.Then(/^User executes query for serv_end_dt$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'select serv_end_dt, data_feed_id  from owner_esr_qa.esr_foreign_serv where person_Id = 1599110 order by data_feed_id asc', {},
//
                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        var a = JSON.stringify(result.metaData);      //convert the result in string format
                        var b = JSON.stringify(result.rows);
                        var Array1 = a.split("name");
                        var Array2 = b.split(",");
                        console.log(Array2[0]);
                        if (Array2[0] != " ") {
                           console.log("Verified");
                       }
                       else{
                            console.log("ERROR!");
                        }

                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes query for ESR_Annual_Report$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    'select job_id from owner_esr_qa.ESR_Annual_Report  where person_Id = 1599109 and ctrl_grP_org_id = 1921 order by job_id desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        //console.log(result.metaData);
                       // console.log(result.rows);
                        var a=JSON.stringify(result.rows);
                        jobid1=a.split(",");
                        console.log(jobid1);
                        doRelease(connection);
                    });
            });


        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });

    this.Then(/^User executes ESR_Jobs query$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    "select  job_id from owner_esr_qa.ESR_jobs where CLIENT_CD = 'ESRTST'and JOB_TYPE = 'ANNUAL' order by job_id desc", {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                       // console.log(result.metaData);
                        //console.log(result.rows);
                        var b=JSON.stringify(result.rows);
                        jobid2=b.split(",");
                        console.log(jobid2);
                        if(jobid1[0]==jobid2[0]){
                            console.log("job ids are equal");
                        }
                        else{
                                console.log("job ids are not equal");
                            }

                        doRelease(connection);

                    });
            });


        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });



    this.Then(/^User executes query for esr_person_emplt$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    "select current_status from esr_person_emplt where person_id = 31249", {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                       // console.log(result.metaData);
                      //  console.log(result.rows);
                        var a = JSON.stringify(result.metaData);      //convert the result in string format
                        var b = JSON.stringify(result.rows);
                        var Array1 = a.split("name");
                        var Array2 = b.split(",");
                        console.log(Array2[0]);
                        if (Array2[0] = "TERM") {
                            console.log("Verified");
                        }
                        else{
                            console.log("ERROR!");
                        }
                        doRelease(connection);
                    });
            });


        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });


    this.Then(/^User executes query for MED_PLAN_END_DATE$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    "select med_plan_end_dt from esr_emp_medical_plan where person_id = 35087 order by last_update desc",{},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                         console.log(result.metaData);
                          console.log(result.rows);

                  //      var a = JSON.stringify(result.metaData);      //convert the result in string format
                        var b = JSON.stringify(result.rows);

                        var c = b.split(",");
                        function convertToDateFormat(c) {
                            if ( c!="" ) { // Datestr="03/08/2016"
                                var datedata = c.split("/");
                                var formatedDateString=datedata[2]+'-' + datedata[1] + '-' + datedata[0] + 'T00:00:00.000Z';
                                return formatedDateString;
                            }
                        }
                        console.log(convertToDateFormat("12/31/2016"));



                  //     console.log(Array2[0]);
                  //      if (Array2[0] = "2016-12-31T18:30:00.000Z") {
                  //          console.log("Verified");
                  //      }
                  //      else{
                  //          console.log("ERROR!");
                  //      }
                        doRelease(connection);
                    });
            });


        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });


    this.Then(/^User executes query for serv_end_dt for esrtst client$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    "select serv_end_dt,data_feed_id from owner_esr_qa.ESR_foreign_serv  where person_Id = 1599110 order by data_feed_id asc",{},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }

                              var a = JSON.stringify(result.metaData);      //convert the result in string format
                              var b = JSON.stringify(result.rows);
                              var Array1 = a.split("name");
                              var Array2 = b.split(",");

                              console.log(Array2[0]);
                              if (Array2[0] != " ") {
                                  console.log("Verified");
                              }
                              else{
                                  console.log("ERROR!");
                              }
                        doRelease(connection);
                    });
            });


        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });


    this.Given(/^User checks if serv_end_dt is Nov 30$/, function () {

        oracledb.getConnection(
            {
                user: dbConfig.user,
                password: dbConfig.password,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    'select serv_end_dt from owner_esr_qa.ESR_foreign_serv  where person_Id = 1599105 order by serv_end_dt asc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        var b = JSON.stringify(result.rows);

                        var c = b.split(",");
                        function convertToDateFormat(c) {
                            if ( c!="" ) { // Datestr="03/08/2016"
                                var datedata = c.split("/");
                                var formatedDateString=datedata[2]+'-' + datedata[1] + '-' + datedata[0] + 'T00:00:00.000Z';
                                return formatedDateString;
                            }
                        }
                        console.log(convertToDateFormat("11/30/2014"));

                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });

    this.Given(/^User updates company enrollment data$/, function () {

        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    "Update company_employment_data set sal_hrly_code = 'S' where SSN = 179626707 and BEGIN_EFF_DATE = '4-DEC-2017'", {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });

    this.Given(/^Validate column END_DTTM is not NULL for first row$/, function () {

        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    'select end_dttm,data_feed_id from esr_data_feed order by data_feed_id desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                       // console.log(result.metaData);
                        console.log(result.rows);
                       var a=JSON.stringify(result.rows);
                       var Array1=a.split(",");
                       console.log(Array1[0]);
                       if(Array1[0] !=" "){
                           console.log("verified");
                       }
                       else{
                           console.log("not verified");

                       }
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });

    this.Given(/^Validate results are displayed and Result should not be NULL$/, function () {

        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute
                    'Select data_feed_id from esr_person_medical_enrollment order by data_feed_id desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                       // console.log(result.metaData);
                        console.log(result.rows);
                        var a=JSON.stringify(result.rows);
                        var Array1=a.split(",");
                        console.log(Array1[0]);
                        if(Array1[0] !=" "){
                            console.log("verified");
                        }
                        else{
                            console.log("not verified");

                        }
                       doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }


    });

    this.Then(/^User executes query for childr ssn 174704636$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_PERSON where SSN = 174704636', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr esr_person_emplt$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_PERSON_EMPLT where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr esr_loa$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_LOA where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_EMP_SERVICE_HOURS$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_EMP_SERVICE_HOURS where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_EMP_MEDICAL_PLAN$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_EMP_MEDICAL_PLAN where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_DATA_FEED$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_DATA_FEED order by 1 desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_PERSON_MEMBER$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_PERSON_MEMBER where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_EMPLT_WORK_ST$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_EMPLT_WORK_ST where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_LOA where person_id = 35778$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_LOA where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_FPL$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_FPL', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_PAY_RATE where person_id = 35778$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_PAY_RATE where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_EMPLOYMENTLOASTAGINGDATA where SSN = 174704636$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_EMPLOYMENTLOASTAGINGDATA where SSN = 174704636', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_JOBS order by 1 desc$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_JOBS order by 1 desc', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_EMP_SH_STAGINGDATA where SSN = 174704636$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_EMP_SH_STAGINGDATA where SSN = 174704636', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_FOREIGN_SERV where person_id = 35778$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_FOREIGN_SERV where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_RATE_OF_PAY where person_id = 35778$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_RATE_OF_PAY where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_PERSON_W2_WAGE where person_id = 35778$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_PERSON_W2_WAGE where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_TRACK_INITIAL_HOURS where person_id = 35778$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_TRACK_INITIAL_HOURS where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_PERSON_WAIT_PERIOD where person_id = 48799$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_PERSON_WAIT_PERIOD where person_id = 48799', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });

    this.Then(/^User executes for childr ESR_ACTIVE_COBRA_COVERAGE where person_id = 35778$/, function () {
        oracledb.getConnection(
            {
                user: dbConfig.user1,
                password: dbConfig.password1,
                connectString: dbConfig.connectString
            },
            function (err, connection) {
                if (err) {
                    console.error(err.message);
                    return;
                }
                connection.execute(
                    // The statement to execute

                    'SELECT * FROM ESR_ACTIVE_COBRA_COVERAGE where person_id = 35778', {},

                    {
                        maxRows: 1

                    },
                    function (err, result) {
                        if (err) {
                            console.error(err.message);
                            doRelease(connection);
                            return;
                        }
                        console.log(result.metaData);
                        console.log(result.rows);
                        //console.log(url);
                        doRelease(connection);
                    });
            });

        function doRelease(connection) {
            connection.close(
                function (err) {
                    if (err) {
                        console.error(err.message);
                    }
                });
        }

    });
    this.Then(/^User moves to the Pre folder "([^"]*)"$/, function (browser, rep) {
        action.initializePageObjects(browser, function () {
            var location_excel1;
            var excel_name1;
            var mkdirp = require('mkdirp');
            console.log("Homepath: "+homepath);
            //  var fname = rep + new Date().getMonth() + new Date().getFullYear() + "_" + new Date().getTime();
            var fname = rep + new Date().getMonth() + new Date().getFullYear();
            // mkdirp(homepath + "/Downloads/ESR/" + fname, function (err) {
            mkdirp(homepath + "/Downloads/Pre/" , function (err) {
                if (err) {
                    console.error("err is " + err);
                }
                else {
                    //console.log('Dir created!');
                    glob(homepath + "/Downloads/Pre/*.xlsx", function (er, files) {

                        location_excel1 = files[0];
                        console.log("Location Excel 1"+location_excel1)
                        //location_excel2 = files[1];
                        //var excel_name1,excel_name2;
                        excel_name1 = location_excel1.split('/');
                        console.log("Excel Name 1"+excel_name1)
                        //  excel_name2 = location_excel2.split('/');
                        console.log(excel_name1[4]);
                        // console.log(excel_name2[4]);
                        console.log("Files are :" + location_excel1);
                        moved(location_excel1);
                    });
                }

                function moved(location_excel1) {
                    //  move(location_excel1, homepath + "/Downloads/ESR/" + fname + "/" + excel_name1[4], function () {
                    move(location_excel1, homepath + "/Downloads/Pre/*.xlsx" + fname + "/" + excel_name1[4], function () {
                        //	console.log("Moved the files successfully!!"+location_excel1)
                        // move(location_excel2, homepath + "/Downloads/ESR/" + fname + "/" + excel_name2[4], function () {
                        // move(location_excel2, homepath + "/Downloads/post" + fname + "/" + excel_name2[4], function () {
                        //console.log("Moved the files successfully!!" + location_excel2)
                    });
                    //});

                }

                function move(oldPath, newPath, callback) {

                    fs.rename(oldPath, newPath, function (err) {
                        if (err) {
                            if (err.code === 'EXDEV') {
                                copy();
                            } else {
                                callback(err);
                            }
                            return;
                        }
                        callback();
                    });

                    function copy() {
                        var readStream = fs.createReadStream(oldPath);
                        var writeStream = fs.createWriteStream(newPath);

                        readStream.on('error', callback);
                        writeStream.on('error', callback);

                        readStream.on('close', function () {
                            fs.unlink(oldPath, callback);
                        });

                        readStream.pipe(writeStream);
                    }
                }
            });
        });
    });

    this.Then(/^User moves to the Post folder "([^"]*)"$/, function (browser, rep) {
        action.initializePageObjects(browser, function () {
            var location_excel2;
            var excel_name2;
            var mkdirp = require('mkdirp');
            //  var fname = rep + new Date().getMonth() + new Date().getFullYear() + "_" + new Date().getTime();
            var fname = rep + new Date().getMonth() + new Date().getFullYear();
            // mkdirp(homepath + "/Downloads/ESR/" + fname, function (err) {
            mkdirp(homepath + "/Downloads/Post/" , function (err) {
                if (err) {
                    console.error("err is " + err);
                }
                else {
                    //console.log('Dir created!');
                    glob(homepath + "/Downloads/Post/*.xlsx", function (er, files) {

                        // location_excel1 = files[0];
                        location_excel2 = files[1];
                        //var excel_name1,excel_name2;
                        // excel_name1 = location_excel1.split('/');
                        excel_name2 = location_excel2.split('/');
                        //console.log(excel_name1[4]);
                        console.log(excel_name2[4]);
                        console.log("Files are :" + location_excel2);
                        moved(location_excel2);
                    });
                }

                function moved(location_excel2) {
                    //  move(location_excel1, homepath + "/Downloads/ESR/" + fname + "/" + excel_name1[4], function () {
                    // move(location_excel1, homepath + "/Downloads/Pre" + fname + "/" + excel_name1[4], function () {
                    //	console.log("Moved the files successfully!!"+location_excel1)
                    // move(location_excel2, homepath + "/Downloads/ESR/" + fname + "/" + excel_name2[4], function () {
                    move(location_excel2, homepath + "/Downloads/Post" + fname + "/" + excel_name2[4], function () {
                        console.log("Moved the files successfully!!" + location_excel2)
                    });
                    //});

                }

                function move(oldPath, newPath, callback) {

                    fs.rename(oldPath, newPath, function (err) {
                        if (err) {
                            if (err.code === 'EXDEV') {
                                copy();
                            } else {
                                callback(err);
                            }
                            return;
                        }
                        callback();
                    });

                    function copy() {
                        var readStream = fs.createReadStream(oldPath);
                        var writeStream = fs.createWriteStream(newPath);

                        readStream.on('error', callback);
                        writeStream.on('error', callback);

                        readStream.on('close', function () {
                            fs.unlink(oldPath, callback);
                        });

                        readStream.pipe(writeStream);
                    }
                }
            });
        });
    });

};






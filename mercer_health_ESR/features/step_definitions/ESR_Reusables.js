var action = require('./../../../Keywords');
var data=require('./../../test_data/ESR_TestData');
	
var MonthlyReportGeneration=	function (browser,IRS_year,Year,Month,callback) {
		
		action.initializePageObjects(browser, function () {
			action.performClick('ESRHomePage|dropdownMonthlyReportSelectIRSYear', function () {
				browser.timeoutsImplicitWait(data.shortWait);
				action.performClick('ESRHomePage|dropdownMonthlyReportOptionIRS'+IRS_year, function () {
					browser.timeoutsImplicitWait(data.shortWait);
					action.performClick('ESRHomePage|dropdownMonthlyReportSelectYear', function () {
						browser.timeoutsImplicitWait(data.shortWait);
						action.performClick('ESRHomePage|dropdownMonthlyReportOptionYear'+Year, function () {
							browser.timeoutsImplicitWait(data.shortWait);
							action.performClick('ESRHomePage|dropdownMonthlyReportSelectMonth', function () {
								browser.timeoutsImplicitWait(data.shortWait);
								action.performClick('ESRHomePage|dropdownMonthlyReportOption'+Month, function () {
									browser.timeoutsImplicitWait(data.shortWait);
									//action.performClick('ESRHomePage|Rerun',function () {
										action.performClick('ESRHomePage|buttonOKMonthlyReportPopUp', function () {
											browser.timeoutsImplicitWait(data.shortWait);
											action.elementDisplayedStatus('ESRHomePage|messageErrorReport',function (status) {
												if(status)
												{
													console.log("Try again later");
													
													callback();
												}
												else
												{
													browser.pause(data.shortWait);
													callback();
												}
											})
											
											
										});
									//});
									
								});
							});
						});
					});
				});
			});
		});
		
		
	};
var AnnualReportGeneration=	function (browser,Year,callback) {
	
	action.initializePageObjects(browser, function () {
		action.performClick('ESRHomePage|dropdownAnnualReportSelectIRSYear', function () {
			browser.timeoutsImplicitWait(data.shortWait);
			action.performClick('ESRHomePage|dropdownAnnualReportOptionYear'+Year, function () {
				browser.timeoutsImplicitWait(data.shortWait);
				
				action.performClick('ESRHomePage|buttonOKAnnualReportPopUp', function () {
					browser.timeoutsImplicitWait(data.shortWait);
					action.elementDisplayedStatus('ESRHomePage|messageErrorReport',function (status) {
						if(status)
						{
							console.log("Try again later");
							
							callback();
						}
						else
						{
							browser.pause(data.shortWait);
							callback();
						}
					})
					
					
				});
				
			});
		});
	});
	
	
};
module.exports.MonthlyReportGeneration=MonthlyReportGeneration;
module.exports.AnnualReportGeneration=AnnualReportGeneration;



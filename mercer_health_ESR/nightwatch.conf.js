var seleniumServer = require('selenium-server');
var chromedriver=require('chromedriver');
var ieDriver = require('iedriver');
var firefoxDriver = require('geckodriver');
var data = require('./test_data/ESR_TestData');
const osHomedir = require('os-homedir');
var path = osHomedir();
var homepath = path.replace(new RegExp('\\' + path.sep, 'g'), '/');
sauce_labs_username = "Sumithra_P";
sauce_labs_access_key = "1c828d5b-a2a6-4bd9-8f94-44f4dd1151a6";

require('nightwatch-cucumber')({
    //supportFiles: ['../utils/TestExecListener.js'],
    stepTimeout: 2900000,
    defaultTimeoutInterval: 40000,
    nightwatchClientAsParameter: true
});

module.exports = {
   
    output_folder: 'reports',
    custom_commands_path: '',
    custom_assertions_path: '',
    page_objects_path: "objects",
	globals_path: "../GlobalTestData.js",
    live_output: false,
    disable_colors: false,
    test_workers: {
        enabled: false,
        workers: 3
    },
    selenium: {
        start_process: true,
        server_path: seleniumServer.path,
        host: '127.0.0.1',
        port: 5555,
        platform: 'Windows 10',
        cli_args: {
            "webdriver.chrome.driver" :chromedriver.path,
            'webdriver.ie.driver': '../IEDriverServer.exe',
            'webdriver.gecko.driver': firefoxDriver.path
        }
    },
    test_settings: {
        default: {
            launch_url: 'http://localhost',
            page_objects_path: 'objects',
            selenium_host: '127.0.0.1',
            selenium_port: 5555,
            silent: true,
            disable_colors: false,
            screenshots: {
                enabled: true,
                on_failure: true,
                on_error: true,
                path: 'screenshots'
            },
            
            desiredCapabilities: {
                browserName: data.NameofThebrowser,
                // browserName: "internet explorer",
                javascriptEnabled: true,
                acceptSslCerts: true,
	            ChromeOptions:{
		            args: ["start-fullscreen"],
		            prefs:{
			            downloads:
				            {
					            prompt_for_download:false,
					            directory_upgrade:true,
					            default_directory:homepath +"/Downloads/ESR/"
				            }
		            }
	            }
            },
        },

        SauceLabs: {
            launch_url: 'http://ondemand.saucelabs.com:80',
            selenium_port: 80,
            selenium_host: 'ondemand.saucelabs.com',
            silent: true,
            username: sauce_labs_username,
            access_key: sauce_labs_access_key,
            screenshots: {
                enabled: false,
                path: '',
            },
            globals: {
                waitForConditionTimeout: 10000,
                
            },
            desiredCapabilities: {
                name:'ESR Test',
                browserName: 'internet explorer',
               platform: 'Windows 7',
                version: '11',
                parentTunnel:'erik-horrell',
                tunnelIdentifier:'MercerTunnel2',
            }
        },
        microsoftedge: {
            desiredCapabilities: {
                browserName: 'microsoftedge'
            }
        },
        chrome: {
            desiredCapabilities: {
                browserName: 'chrome',
                javascriptEnabled: true,
                acceptSslCerts : true,
                ChromeOptions:{
                    args: ["start-fullscreen"],
                    prefs:{
                        downloads:
                            {
                                prompt_for_download:false,
                                directory_upgrade:true,
                                default_directory:homepath +"/Downloads/ESR/"
                            }
                    }
                },
                
            }
        },
        firefox: {
            desiredCapabilities: {
                browserName: 'firefox',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ie: {
            desiredCapabilities: {
                browserName: 'internet explorer',
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        // Appium Config details
        iphone : {
            launch_url : "http://" + sauce_labs_username + ":" + sauce_labs_access_key +
            "@ondemand.saucelabs.com:80/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "iphone",
                platformName: "iOS",
                deviceName: "iPhone Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPHONE EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        ipad : {
            launch_url : "http://127.0.0.1:4723/wd/hub",
            selenium_port  : 4723,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "ipad",
                platformName: "iOS",
                deviceName: "iPad Simulator",
                version: "7.1",
                app: "PATH TO YOUR IPAD EMULATOR APP",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        },
        android : {
            launch_url : "http://localhost:5554/wd/hub",
            selenium_port  : 5554,
            selenium_host  : "localhost",
            silent: true,
            screenshots : {
                enabled : false,
                path : ""
            },
            desiredCapabilities: {
                browserName: "android",
                platformName: "ANDROID",
                deviceName: "",
                version: "",
                javascriptEnabled: true,
                acceptSslCerts: true
            }
        }

    }

}


